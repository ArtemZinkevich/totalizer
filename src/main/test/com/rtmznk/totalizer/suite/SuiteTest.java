package com.rtmznk.totalizer.suite;

import com.rtmznk.totalizer.database.ConnectionManagerTest;
import com.rtmznk.totalizer.database.ConnectionPoolTest;
import com.rtmznk.totalizer.utility.LocaleUtilityTest;
import com.rtmznk.totalizer.utility.PasswordUtilityTest;
import com.rtmznk.totalizer.utility.RequestDataValidatorTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ConnectionManagerTest.class, ConnectionPoolTest.class,
        LocaleUtilityTest.class, PasswordUtilityTest.class, RequestDataValidatorTest.class})
public class SuiteTest {
}
