package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;


public class RequestDataValidatorTest {
    private static RequestDataValidator validator;
    private static Map<String, String> registrationData;
    private static Map<String, String> depositData;
    private static Totalizer totalizerMock;
    private static Coupon couponMock;
    private static User userMock;

    @BeforeClass
    public static void setUpData() {
        validator = RequestDataValidator.getInstance();
        registrationData = new HashMap<>();
        depositData = new HashMap<>();
        registrationData.put("name", "validName");
        registrationData.put("lastName", "validLastName");
        registrationData.put("password", "1111Aa");
        registrationData.put("password2", "1111Aa");
        registrationData.put("mail", "normal@mail.com");
        depositData.put("money", "1000");
        depositData.put("number", "1111111111111111");
        depositData.put("month", "3");
        depositData.put("year", "12");
        depositData.put("cvv", "234");
        depositData.put("person", "person");
        totalizerMock = createTotalizerMock();
        couponMock = createCouponMock();
        userMock = mock(User.class);
        when(userMock.getMoney()).thenReturn(BigDecimal.valueOf(100));
    }

    private static Totalizer createTotalizerMock() {
        Totalizer totalizer = mock(Totalizer.class);
        when(totalizer.getName()).thenReturn("NormalName");
        when(totalizer.getJackpot()).thenReturn(BigDecimal.TEN);
        Event eventMock = mock(Event.class);
        when(eventMock.getType()).thenReturn(Event.Type.RESULT);
        when(eventMock.getId()).thenReturn(1L);
        when(eventMock.getScore1()).thenReturn(1);
        when(eventMock.getScore2()).thenReturn(1);
        when(eventMock.getSport()).thenReturn("sport");
        when(eventMock.getTeam1()).thenReturn("team1");
        when(eventMock.getTeam1()).thenReturn("team2");
        when(eventMock.getDate()).thenReturn(LocalDateTime.now().plusDays(2));
        List<Event> eventListMock = (List<Event>) mock(List.class);
        Iterator<Event> iterator = mock(Iterator.class);
        when(iterator.next()).thenReturn(eventMock);
        when(eventListMock.iterator()).thenReturn(iterator);
        when(totalizer.getEvents()).thenReturn(eventListMock);
        return totalizer;
    }

    private static Coupon createCouponMock() {
        Coupon coupon = mock(Coupon.class);
        when(coupon.getAmount()).thenReturn(BigDecimal.TEN);
        when(coupon.getCredit()).thenReturn(BigDecimal.ONE);
        Bet betMock = mock(Bet.class);
        when(betMock.getEventId()).thenReturn(1L);
        when(betMock.getEventType()).thenReturn(Event.Type.RESULT);
        when(betMock.getResult()).thenReturn(Bet.Result.DRAW);
        List<Bet> betListMock = (List<Bet>) mock(List.class);
        Iterator<Bet> iterator = mock(Iterator.class);
        when(iterator.next()).thenReturn(betMock);
        when(betListMock.iterator()).thenReturn(iterator);
        when(coupon.getBets()).thenReturn(betListMock);
        return coupon;
    }

    @Test
    public void receiveRegistrationErrorsTest() throws Exception {
        Assert.assertTrue("Validator identify valid fields as invalid",
                validator.receiveRegistrationErrors(registrationData).isEmpty());
    }

    @Test
    public void receiveDepositErrorsTest() throws Exception {
        Map<String, String> validationErrors = validator.receiveDepositErrors(depositData, BigDecimal.ZERO);
        Assert.assertTrue("Validator identify valid fields as invalid", validationErrors.isEmpty());
    }

    @Test
    public void validateIdTest() throws Exception {
        boolean expectedTrue = validator.validateId("1");
        boolean expectedFalse = validator.validateId("string");
        Assert.assertTrue(expectedTrue && !expectedFalse);
    }

    @Test
    public void validateTotolizerParametersTest() throws Exception {
        Map<String, String> errors = validator.validateTotolizerParameters(totalizerMock);
        Assert.assertTrue(errors.isEmpty());
    }

    @Test
    public void validateCouponTest() throws Exception {
        Map<String, String> errors = validator.validateCoupon(couponMock, totalizerMock, userMock);
        Assert.assertTrue(errors.isEmpty());
    }

}