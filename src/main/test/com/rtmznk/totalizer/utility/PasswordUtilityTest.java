package com.rtmznk.totalizer.utility;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class PasswordUtilityTest {
    private String passwordToTest;

    public PasswordUtilityTest(String passwordToTest) {
        this.passwordToTest = passwordToTest;
    }

    @Parameterized.Parameters
    public static Collection<String> setUpPasswordParameters() {
        return Arrays.asList("password1", "password2", "anotherPassword1", "1q2w3e", "anything here");
    }

    @Test
    public void hashPasswordTest() throws Exception {
        String actual1 = PasswordUtility.hashPassword(passwordToTest);
        String actual2 = PasswordUtility.hashPassword(passwordToTest);
        assertEquals("Hash of equal password string distinguished", actual1, actual2);
    }

}