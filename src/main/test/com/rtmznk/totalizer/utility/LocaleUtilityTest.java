package com.rtmznk.totalizer.utility;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;


public class LocaleUtilityTest {
    private static final String RUSSIAN = "ru";
    private static final String ENGLISH = "en";
    private static final String UNKNOWN = "unknown";
    private Locale en;
    private Locale ru;
    private LocaleUtility utility;

    @Before
    public void setUp() throws Exception {
        en = Locale.ENGLISH;
        ru = new Locale(RUSSIAN);
        utility = LocaleUtility.getInstance();
    }

    @After
    public void tearDown() throws Exception {
        en = null;
        ru = null;
    }

    @Test
    public void getLocaleByStringTest() throws Exception {
        Locale actualEn = utility.getLocaleByString(ENGLISH);
        Locale actualRu = utility.getLocaleByString(RUSSIAN);
        Locale actualDefault = utility.getLocaleByString(UNKNOWN);
        boolean expected = actualEn.equals(en) && actualRu.equals(ru) && actualDefault.equals(ru);
        assertTrue(expected);
    }

}