package com.rtmznk.totalizer.database;

import org.junit.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;


public class ConnectionPoolTest {
    private static ConnectionManager connectionManager;
    private static ConnectionPool connectionPool;
    private List<Connection> takenConnections;

    @BeforeClass
    public static void setUp() throws Exception {
        connectionManager = ConnectionManager.getInstance();
        connectionPool = ConnectionPool.getInstance();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        connectionManager = null;
        connectionPool.destroyConnectionPool();
        connectionPool = null;
    }

    @Before
    public void initTakenConnections() {
        takenConnections = new ArrayList<>();
    }

    @After
    public void returnAllTakenBackToPool() {
        takenConnections.forEach(connectionPool::returnConnection);
        takenConnections = null;
    }

    @Test
    public void takeConnectionTest() throws Exception {
        Connection connection = connectionPool.takeConnection();
        takenConnections.add(connection);
        boolean expected = connection != null && connection instanceof ProxyConnection;
        Assert.assertTrue(expected);
    }

    @Test
    public void returnConnectionTest() throws Exception {
        int poolSize = connectionManager.getPoolSize();
        Connection first = null;
        for (int i = 0; i < poolSize; i++) {
            if (i == 0) {
                first = connectionPool.takeConnection();
            } else {
                takenConnections.add(connectionPool.takeConnection());
            }

        }
        connectionPool.returnConnection(first);
        Connection expected = connectionPool.takeConnection();
        takenConnections.add(expected);
        Assert.assertEquals(first, expected);
    }

}