package com.rtmznk.totalizer.database;

import org.junit.*;

import java.sql.Connection;

import static org.junit.Assert.*;


public class ConnectionManagerTest {
    private ConnectionManager connectionManager;

    @Before
    public void setUp() throws Exception {
        connectionManager = ConnectionManager.getInstance();
    }

    @Test
    public void receiveConnectionTest() throws Exception {
        Connection connection = connectionManager.receiveConnection();
        Assert.assertTrue(connection instanceof ProxyConnection);
    }

}