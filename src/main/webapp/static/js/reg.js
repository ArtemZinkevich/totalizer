$(function () {
    var FILL_FIELD_ENG = "*This field must be filled in";
    var FILL_FIELD_RU = "*Это поле должно быть заполнено";
    var NAME_FIELD_ENG = "*This field must contain only characters";
    var NAME_FIELD_RU = "*Это поле может содержать только буквы";
    var BAD_MAIL_ENG = "*Incorrect e-mail(@ and '.' is obligatory)";
    var BAD_MAIL_RU = "*Некорректный е-мэйл(@ и '.' обязательны)";
    var IN_USE_MAIL_ENG = "*e-mail you entered allready in use";
    var IN_USE_MAIL_RU = "*введенный е-мэйл уже используется";
    var INVALID_PASS_EN = "*At least 6 characters (1 Latin letter of each case and 1 digit)";
    var INVALID_PASS_RU = "*Минимум 6 сиволов(по 1 латинской букве каждого регистра и 1 цифре)";
    var PWD_NOT_EQUAL_EN = "*Passwords are different";
    var PWD_NOT_EQUAL_RU = "*Не совпадают значения паролей";

    $('#login-form-link').click(function (e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function (e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

    var isEnLang = function () {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    };

    var validateName = function () {
        var result = true;
        var value = $("#firstName").val();
        var errorName = $("#err-name");
        errorName.text("");
        if (!/[a-zA-Zа-яА-ЯЁё]+/.test(value)) {
            if (isEnLang()) {
                errorName.text(NAME_FIELD_ENG);
            } else {
                errorName.text(NAME_FIELD_RU);
            }
            result = false;
        }
        return result;
    };

    var validateLastName = function () {
        var result = true;
        var value = $("#lastName").val();
        var errorLastName = $("#err-lname");
        errorLastName.text("");
        if (!/[a-zA-Zа-яА-ЯЁё]+/.test(value)) {
            if (isEnLang()) {
                errorLastName.text(NAME_FIELD_ENG);
            } else {
                errorLastName.text(NAME_FIELD_RU);
            }
            result = false;
        }
        return result;
    };

    function doAjax() {
        var body = {};
        body.mail = $("#mail").val();
        var errorMail = $("#err-mail");
        $.post("mail-check", body, function (res) {
            if (res === "true") {
                if (isEnLang()) {
                    errorMail.text(IN_USE_MAIL_ENG);
                }
                else {
                    errorMail.text(IN_USE_MAIL_RU);
                }
                $("#register-submit").attr("disabled", true);
            } else {
                errorMail.text("");
                $("#register-submit").attr("disabled", false);
            }
        });
    }


    var validateMail = function () {
        var result = true;
        var mail = $("#mail").val();
        var errorMail = $("#err-mail");
        errorMail.text("");
        if (!mail) {
            if (isEnLang()) {
                errorMail.text(FILL_FIELD_ENG);
            }
            else {
                errorMail.text(FILL_FIELD_RU);
            }
            result = false;
        } else if (mail.indexOf("@") < 1 || mail.indexOf(".") < 1) {
            if (isEnLang()) {
                errorMail.text(BAD_MAIL_ENG);
            }
            else {
                errorMail.text(BAD_MAIL_RU);
            }
            result = false;
        } else {
            doAjax();
        }

        return result;
    };


    var validatePassword = function () {
        var result = true;
        var pass = $("#password").val();
        var errorPass = $("#err-password");
        errorPass.text("");
        if (!pass) {
            if (isEnLang()) {
                errorPass.text(FILL_FIELD_ENG);
            }
            else {
                errorPass.text(FILL_FIELD_RU);
            }
            result = false;
        } else if (pass && pass.search(/^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})$/)) {
            if (isEnLang()) {
                errorPass.text(INVALID_PASS_EN);
            }
            else {
                errorPass.text(INVALID_PASS_RU);
            }
            result = false;
        }
        return result;
    };

    var validate2Password = function () {
        var result = true;
        var pass1 = $("#password").val();
        var pass2 = $("#confirm-password").val();
        var errorPass = $("#err-password2");
        errorPass.text("");
        if (pass1 !== pass2) {
            if (isEnLang()) {
                errorPass.text(PWD_NOT_EQUAL_EN);
            }
            else {
                errorPass.text(PWD_NOT_EQUAL_RU);
            }
            result = false;
        }   // должны совпадать
        return result;
    };

    var clearErrName = function () {
        var errorName = $("#err-name");
        errorName.text("");
    };
    var clearErrLastName = function () {
        var errorName = $("#err-lname");
        errorName.text("");
    };
    var clearErrMail = function () {
        var errorMail = $("#err-mail");
        errorMail.text("");
    };
    var clearErrPass = function () {
        var errorPass = $("#err-password");
        errorPass.text("");
    };
    var clearErrPass2 = function () {
        var errorPass = $("#err-password2");
        errorPass.text("");
    };


    $("#firstName").focusout(validateName);
    $("#lastName").focusout(validateLastName);
    $("#mail").focusout(validateMail);
    $("#password").focusout(validatePassword);
    $("#confirm-password").focusout(validate2Password);
    $("#firstName").on('input',clearErrName);
    $("#lastName").on('input',clearErrLastName);
    $("#mail").on('input',clearErrMail);
    $("#password").on('input',clearErrPass);
    $("#confirm-password").on('input',clearErrPass2);

    var validateForm = function () {
        return validateName() && validateLastName() && validateMail() && validatePassword() && validate2Password();
    };
    $("#register-form").unbind('submit').bind('submit',validateForm);
});

