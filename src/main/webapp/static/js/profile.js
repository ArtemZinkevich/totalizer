$(function () {
    $("#change-pass-page").click(function (e) {
        e.preventDefault();
        getPasswordPage();
    });
    humanizeDate();
    function getPasswordPage() {
        startLoading();
        $("#external").load("profile/pass-page #content", {},
            function (response, status, xhr) {
                showModal(status, response)
                bindPassFormValidation();
                endLoading(1700);
            });
    }

    function humanizeDate() {
        var dates = $("span.date");
        $(dates).each(function (index) {
            var date = new Date(parseInt($(this).text()));
            $(this).text(date);
        });
    }

    function showModal(status, response) {
        if (status == "error") {
            modalWindowAddContent(response);
            $('#myModal').modal();
        }
    }

    function modalWindowAddContent(string) {
        var html = $('<div />').append(string).find('#content');
        $(".modal-body").html(html);
    }


    function bindPassFormValidation() {
        var form = $("#pass-form");
        form.unbind('submit');
        var VALIDATION_MESSAGE;
        if (isEnLang()) {
            VALIDATION_MESSAGE = "Please, fill in this field"
        } else {
            VALIDATION_MESSAGE = "Необходимо заполнить это поле"
        }
        form.validate({
            rules: {
                // simple rule, converted to {required:true}
                oldPass: "required",
                newPass: "required"
            },
            messages: {
                oldPass: VALIDATION_MESSAGE,
                newPass: VALIDATION_MESSAGE
            },
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                startLoading();
                $("#external").load("profile/change-password #content", {
                    oldPass: $("#old-pass").val(),
                    newPass: $("#new-pass").val()
                }, function (response, status, xhr) {
                    showModal(status, response);
                    bindPassFormValidation();
                    endLoading(1700);
                });
            }
        });
    }

    function startLoading() {
        var over = '<div id="overlay">' +
            '<div id="loading">' +
            '</div></div>';
        $(over).appendTo('#overlay-container');
    }

    function endLoading(timeout) {
        setTimeout(function () {
            $('#overlay').remove();
        }, timeout);
    }

    function isEnLang() {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    }
});


