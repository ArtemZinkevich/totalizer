$(function () {
    function post(path, params, method) {
        method = method || "post"; // Set method to post by default if not specified.

        var form = document.createElement("form");
        form.setAttribute("method", method);
        form.setAttribute("action", path);

        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);

                form.appendChild(hiddenField);
            }
        }

        document.body.appendChild(form);
        form.submit();
    }

    $('#logout').click(function (e) {
        e.preventDefault();
        post("logout");
    });
    $('#profile').click(function (e) {
        e.preventDefault();
        post("go-profile-update");
    });
    $('#game').click(function (e) {
        e.preventDefault();
        post("game");
    });
    $('#en-lang').click(function (e) {
        e.preventDefault();
        var pageLink = $("#page").val();
        post("i18n", {lang: "en", page: pageLink});
    });
    $('#ru-lang').click(function (e) {
        e.preventDefault();
        var pageLink = $("#page").val();
        post("i18n", {lang: "ru", page: pageLink});
    });
});


