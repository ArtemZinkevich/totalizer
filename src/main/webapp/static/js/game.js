$(function () {
    bindValidation();
    humanizeDate();
    var LOAD_TIME = 1000;

    function humanizeDate() {
        var dates = $("span.date");
        $(dates).each(function (index) {
            var date = new Date(parseInt($(this).text()));
            $(this).text(date);
        });
    }
    function createBetValidationRules() {
        var rules = {};
        var requiredRule = {required: true};
        var scoreRule = {
            required: true,
            digits: true,
            maxlength: 3
        };
        rules.betSum = {
            required: true,
            pattern: "^\\d{1,6}(\\.\\d{1,2})?$"
        };
        rules.betCredit = {
            credit: true,
            min: 1
        };
        var bet = $(".bet");
        $(bet).each(function (index) {
            var betTypeVal = $(this).find("#eventType").val();
            if (betTypeVal == "score") {
                var score1 = $(this).find(".scoreT1").attr("name");
                rules[score1] = scoreRule;
                var score2 = $(this).find(".scoreT2").attr("name");
                rules[score2] = scoreRule;
            }
        });
        console.log(rules);
        return rules;
    }

    function obtainCouponJSON() {
        var coupon = {};
        coupon.bets = [];
        coupon.amount = $("#bet-sum").val();
        coupon.credit = $("#bet-credit").val();
        var betArray = $(".bet");
        $(betArray).each(function (index) {
            var bet = {};
            var betTypeVal = $(this).find("#eventType").val();
            bet.eventId = $(this).find("#eventId").val();
            bet.eventType = $(this).find("#eventType").val();
            if (betTypeVal == "score") {
                var score1 = $(this).find(".scoreT1").val();
                if (score1) {
                    bet.score1 = score1;
                }
                var score2 = $(this).find(".scoreT2").val();
                if (score2) {
                    bet.score2 = score2;
                }
            } else if (betTypeVal == "total") {
                var total = $(this).find("input:checked").val();
                if (total) {
                    bet.total = total;
                }
            } else if (betTypeVal == "result") {
                var result = $(this).find("input:checked").val();
                if (result) {
                    bet.result = result;
                }
            }

            coupon.bets.push(bet);
        });
        console.log(coupon);
        return coupon;
    }

    function createBetValidationMessages() {
        var SCORE_MESSAGE;
        var BET_MESSAGE;
        var CREDIT_MESSAGE;
        var RADIO_MESSAGE;
        if (isEnLang()) {
            SCORE_MESSAGE = "score 1-3 digits";
            BET_MESSAGE = "Enter bet sum: number more than 1 and less 1000000";
            CREDIT_MESSAGE = "Less than 50% from bet sum but more than 1$";
            RADIO_MESSAGE = "Must be selected";
        } else {
            SCORE_MESSAGE = "счет(от 1 до 3 цифр)";
            BET_MESSAGE = "Введите сумму ставки: Число больше 1 и меньше 1000000";
            CREDIT_MESSAGE = "Меньше 50% от суммы ставки но больше 1 $";
            RADIO_MESSAGE = "Должно быть выбрано";
        }
        var messages = {};
        messages.betSum = BET_MESSAGE;
        messages.betCredit = CREDIT_MESSAGE;
        var bet = $(".bet");
        $(bet).each(function (index) {
            var betTypeVal = $(this).find("#eventType").val();
            if (betTypeVal == "score") {
                var score1 = $(this).find(".scoreT1").attr("name");
                messages[score1] = SCORE_MESSAGE;
                var score2 = $(this).find(".scoreT2").attr("name");
                messages[score2] = SCORE_MESSAGE;
            } else {
                var radioFirst = $(this).find("input:radio")[0];
                var nameVal = $(radioFirst).attr("name");
                messages[nameVal] = RADIO_MESSAGE;
            }
        });
        console.log(messages);
        return messages;
    }

    function isEnLang() {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    }

    function bindValidation() {
        var form = $("#bet-form");
        form.unbind('submit');
        jQuery.validator.addMethod("credit", function (value, element) {
            return this.optional(element) || $("#bet-sum").val() / 2 > $("#bet-credit").val();
        });
        form.validate({
            rules: createBetValidationRules(),
            messages: createBetValidationMessages(),
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                startLoading();
                var stringifiedJSON = JSON.stringify(obtainCouponJSON());
                console.log(stringifiedJSON);
                $("#external").load("profile/user/make-bet #content", {json: stringifiedJSON}
                    , function (response, status, xhr) {
                        showModal(status, response);
                        $('html, body').animate({scrollTop: 0}, 'fast');
                        bindValidation();
                        endLoading(LOAD_TIME);
                    });
            }
        });
    }

    function showModal(status, response) {
        if (status == "error") {
            modalWindowAddContent(response);
            $('#myModal').modal();
        }
    }

    function modalWindowAddContent(string) {
        var html = $('<div />').append(string).find('#content');
        $(".modal-body").html(html);
    }

    function startLoading() {
        var over = '<div id="overlay">' +
            '<div id="loading">' +
            '</div></div>';
        $(over).appendTo('#overlay-container');
    }

    function endLoading(timeout) {
        setTimeout(function () {
            $('#overlay').remove();
        }, timeout);
    }
});
