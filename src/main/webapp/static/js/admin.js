$(function () {
    var russianLang = {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    };
    var LOAD_TIME = 1700;
    $("#user-list").click(function (e) {
        e.preventDefault();
        getUserListPage();
    });
    $("#create-toto").click(function (e) {
        e.preventDefault();
        getTotolizerCreationPage();
    });
    $("#edit-toto").click(function (e) {
        e.preventDefault();
        getTotolizerEditPage();
    });
    $("#finish-toto").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/admin/finish-totalizer #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                endLoading(LOAD_TIME);
            });
    });
    $("#cancel-toto").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/admin/cancel-totalizer #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                endLoading(LOAD_TIME);
            });
    });
    $("#start-toto").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/admin/start-totalizer #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                endLoading(LOAD_TIME);
            });
    });

    function getUserListPage() {
        startLoading();
        $("#external").load("profile/admin/user-list #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                humanizeDate();
                bindDataTable();
                bindBanForm();
                endLoading(LOAD_TIME);
            });
    }

    function getTotolizerCreationPage() {
        startLoading();
        $("#external").load("profile/admin/create-toto-page #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                bindTotoFormSelectors();
                bindTotolizerCreateForm();
                endLoading(LOAD_TIME);
            });
    }

    function getTotolizerEditPage() {
        startLoading();
        $("#external").load("profile/admin/edit-toto-page #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                bindTotoFormSelectors();
                bindTotolizerEditForm();
                endLoading(LOAD_TIME);
            });
    }

    function bindTotoFormSelectors() {
        var eventArray = $(".event");
        $(eventArray).each(function (index) {
            var event = $(this);
            var totalDiv = $(this).find(".total");
            var select = $(this).find("select");
            if ($(select).val() != 'total') {
                $(totalDiv).hide();
            }
            $(select).change(function () {
                if ($(select).val() == 'total') {
                    $(totalDiv).show();
                } else {
                    $(totalDiv).hide();
                    var field = $(event).find(".score");
                    $(field).val(null);
                }
            });
        });
    }

    function obtainTotalizerFormJSON() {
        var totalizer = {};
        totalizer.events = [];
        totalizer.name = $("#toto-name").val();
        totalizer.jackpot = $("#jackpot").val();
        var eventArray = $(".event");
        $(eventArray).each(function (index) {
            var event = {};
            event.date = $(this).find(".date").val();
            event.type = $(this).find(".type").val();
            event.sport = $(this).find(".sport").val();
            event.team1 = $(this).find(".team1").val();
            event.team2 = $(this).find(".team2").val();
            var total = $(this).find(".score").val();
            if (typeof total === "undefined") {
                event.total = null;
            } else {
                event.total = total;
            }
            totalizer.events.push(event);
        });
        return totalizer;
    }

    function createTotalizerFormValidationRules() {
        var rules = {};

        var requiredRule = {required: true};
        rules.name = requiredRule;
        rules.jackpot = {
            required: true,
            pattern: "^\\d{1,7}$",
            digits: true,
            max: 1000000000
        };
        var eventArray = $(".event");
        $(eventArray).each(function (index) {
            var date = $(this).find(".date").attr("name");
            rules[date] = requiredRule;
            var type = $(this).find(".type").attr("name");
            rules[type] = requiredRule;
            var sport = $(this).find(".sport").attr("name");
            rules[sport] = requiredRule;
            var t1 = $(this).find(".team1").attr("name");
            rules[t1] = requiredRule;
            var t2 = $(this).find(".team2").attr("name");
            rules[t2] = requiredRule;
            var total = $(this).find(".score").attr("name");
            rules[total] = requiredRule;
        });
        console.log(rules);
        return rules;
    }

    function createTotalizerFormValidationMessages() {
        var VALIDATION_MESSAGE_REQUIRED;
        var JACKPOT_MESSAGE;
        var TOTAL_MESSAGE;
        if (isEnLang()) {
            VALIDATION_MESSAGE_REQUIRED = "Please, fill in this field";
            JACKPOT_MESSAGE = "Enter integer from 1 to 1000000000 without spaces";
            TOTAL_MESSAGE = "Must look like '(some digits).5'";
        } else {
            VALIDATION_MESSAGE_REQUIRED = "Необходимо заполнить это поле";
            JACKPOT_MESSAGE = "Введите целое число от 1 до 1000000000 без пробелов";
            TOTAL_MESSAGE = "Должно соответствовать шаблону '(цифры).5'";
        }
        var messages = {};
        messages.name = VALIDATION_MESSAGE_REQUIRED;
        messages.jackpot = JACKPOT_MESSAGE;
        var eventArray = $(".event");
        $(eventArray).each(function (index) {
            var date = $(this).find(".date").attr("name");
            messages[date] = VALIDATION_MESSAGE_REQUIRED;
            var type = $(this).find(".type").attr("name");
            messages[type] = VALIDATION_MESSAGE_REQUIRED;
            var sport = $(this).find(".sport").attr("name");
            messages[sport] = VALIDATION_MESSAGE_REQUIRED;
            var t1 = $(this).find(".team1").attr("name");
            messages[t1] = VALIDATION_MESSAGE_REQUIRED;
            var t2 = $(this).find(".team2").attr("name");
            messages[t2] = VALIDATION_MESSAGE_REQUIRED;
            var total = $(this).find(".score").attr("name");
            messages[total] = TOTAL_MESSAGE;
        });
        console.log(messages);
        return messages;
    }

    function bindTotolizerCreateForm() {
        var form = $("#toto-form");
        form.unbind('submit');
        form.validate({
            rules: createTotalizerFormValidationRules(),
            messages: createTotalizerFormValidationMessages(),
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                startLoading();
                var stringifiedJSON = JSON.stringify(obtainTotalizerFormJSON());
                console.log(stringifiedJSON);
                $("#external").load("profile/admin/create-totalizer #content", {json: stringifiedJSON}
                    , function (response, status, xhr) {
                        showModal(status, response);
                        bindTotoFormSelectors();
                        bindTotolizerCreateForm();
                        endLoading(LOAD_TIME);
                    });
            }
        });

    }

    function bindTotolizerEditForm() {
        var form = $("#toto-form");
        form.unbind('submit');
        form.validate({
            rules: createTotalizerFormValidationRules(),
            messages: createTotalizerFormValidationMessages(),
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                startLoading();
                var stringifiedJSON = JSON.stringify(obtainTotalizerFormJSON());
                console.log(stringifiedJSON);
                $("#external").load("profile/admin/update-totalizer #content", {json: stringifiedJSON}
                    , function (response, status, xhr) {
                        showModal(status, response);
                        bindTotoFormSelectors();
                        bindTotolizerEditForm();
                        endLoading(LOAD_TIME);
                    });
            }
        });

    }

    function bindDataTable() {
        var lang;
        if (!isEnLang()) {
            lang = russianLang;
        }
        $('#userlist').dataTable({
            "language": lang
        });
    }

    function bindBanForm() {
        var form = $("#ban-form");
        form.unbind('submit');
        var VALIDATION_MESSAGE;
        if (isEnLang()) {
            VALIDATION_MESSAGE = "Please, fill in this field"
        } else {
            VALIDATION_MESSAGE = "Необходимо заполнить это поле"
        }
        form.validate({
            rules: {
                // simple rule, converted to {required:true}
                userId: "required"
            },
            messages: {
                userId: VALIDATION_MESSAGE
            },
            errorElement: "div",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            },
            submitHandler: function (form) {
                startLoading();
                $("#external").load("profile/admin/ban-unban #content", {
                    id: $("#userId").val()
                }, function (response, status, xhr) {
                    showModal(status, response);
                    bindDataTable();
                    bindBanForm();
                    endLoading(1700);
                });
            }
        });
    }

    function startLoading() {
        var over = '<div id="overlay">' +
            '<div id="loading">' +
            '</div></div>';
        $(over).appendTo('#overlay-container');
    }

    function endLoading(timeout) {
        setTimeout(function () {
            $('#overlay').remove();
        }, timeout);
    }

    function isEnLang() {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    }

    function humanizeDate() {
        var dates = $("span.date");
        $(dates).each(function (index) {
            var date = new Date(parseInt($(this).text()));
            $(this).text(date);
        });
    }

    function showModal(status, response) {
        if (status == "error") {
            modalWindowAddContent(response);
            $('#myModal').modal();
        }
    }

    function modalWindowAddContent(string) {
        var html = $('<div />').append(string).find('#content');
        $(".modal-body").html(html);
    }

});