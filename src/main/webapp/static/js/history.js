$(function () {
    var russianLang = {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    };
    humanizeDate();
    bindHistoryDataTable();

    function bindHistoryDataTable() {
        var lang;
        if (!isEnLang()) {
            lang = russianLang;
        }
        $('#remittances').dataTable({
            "language": lang
        });
    }

    function isEnLang() {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    }

    function humanizeDate() {
        var dates = $("span.date");
        $(dates).each(function (index) {
            var date = new Date(parseInt($(this).text()));
            $(this).text(date);
        });
    }
});

