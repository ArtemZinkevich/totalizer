$(function () {
    var LOAD_TIME = 1000;
    var depositErrors = {
        SUM_ERR_RU: "*Сумма от 1$ до 9999.99$ с шагом 0.01$",
        SUM_ERR_EN: "*Sum from 1$ to 9999.99$ with step 0.01$",
        NUM_ERR_RU: "*Неверный формат номера карты",
        NUM_ERR_EN: "*Invalid card number format",
        CVV_ERR_RU: "*3 цифры с обратной стороны карты",
        CVV_ERR_EN: "*3 digits from card backside",
        DATE_ERR_RU: "*Неверная дата окончания срока действия карты",
        DATE_ERR_EN: "*Invalid card expire date",
        REQUIRED_EN: "*Field required",
        REQUIRED_RU: "*Обязательное поле",
        getSumErr: function (lang) {
            if (lang) {
                return this.SUM_ERR_EN;
            }
            else {
                return this.SUM_ERR_RU;
            }
        },
        getNumErr: function (lang) {
            if (lang) {
                return this.NUM_ERR_EN;
            }
            else {
                return this.NUM_ERR_RU;
            }
        },
        getDateErr: function (lang) {
            if (lang) {
                return this.DATE_ERR_EN;
            }
            else {
                return this.DATE_ERR_RU;
            }
        },
        getReqErr: function (lang) {
            if (lang) {
                return this.REQUIRED_EN;
            }
            else {
                return this.REQUIRED_RU;
            }
        },
        getCvvErr: function (lang) {
            if (lang) {
                return this.CVV_ERR_EN;
            }
            else {
                return this.CVV_ERR_RU;
            }
        }
    };
    var russianLang = {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    };
    $("#deposit").click(function (e) {
        e.preventDefault();
        getDepositPage();
    });
    $("#cancel-bet").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/user/cancel-bet #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                endLoading(LOAD_TIME);
            });
    });
    $("#repay-loan").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/user/repay-loan #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                endLoading(LOAD_TIME);
            });
    });
    $("#remittance-list").click(function (e) {
        e.preventDefault();
        startLoading();
        $("#external").load("profile/user/remittances #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                humanizeDate();
                bindRemittanceDataTable();
                colorifyTable();
                endLoading(LOAD_TIME);
            });
    });

    function getDepositPage() {
        startLoading();
        $("#external").load("profile/user/deposit-page #content", {},
            function (response, status, xhr) {
                showModal(status, response);
                bindDepositFormValidation();
                endLoading(LOAD_TIME);
            });
    }

    function showModal(status, response) {
        if (status == "error") {
            modalWindowAddContent(response);
            $('#myModal').modal();
        }
    }

    function modalWindowAddContent(string) {
        var html = $('<div />').append(string).find('#content');
        $(".modal-body").html(html);
    }


    function bindDepositFormValidation() {
        var form = $("#deposit-form");
        form.unbind('submit');
        var ruleSet_default = {
            required: true,
            number: true
        };
        var sumSet = {
            max: 10000,
            min: 1,
            rangelength: [1, 7]
        };
        $.extend(sumSet, ruleSet_default);
        var cardnumSet = {
            rangelength: [4, 4],
            digits: true
        };
        $.extend(cardnumSet, ruleSet_default);
        form.validate({
            rules: {
                money: sumSet,
                n1: cardnumSet,
                n2: cardnumSet,
                n3: cardnumSet,
                n4: cardnumSet,
                month: {
                    required: true,
                    digits: true,
                    range: [1, 12]
                },
                year: {
                    required: true,
                    digits: true,
                    range: [10, 50]
                },
                cvv: {
                    required: true,
                    digits: true,
                    rangelength: [3, 3]
                },
                person: {required: true}
            },
            messages: {
                money: depositErrors.getSumErr(isEnLang()),
                n1: depositErrors.getNumErr(isEnLang()),
                n2: depositErrors.getNumErr(isEnLang()),
                n3: depositErrors.getNumErr(isEnLang()),
                n4: depositErrors.getNumErr(isEnLang()),
                month: depositErrors.getDateErr(isEnLang()),
                year: depositErrors.getDateErr(isEnLang()),
                cvv: depositErrors.getCvvErr(isEnLang()),
                person: depositErrors.getReqErr(isEnLang())
            }, groups: {
                cardnumber: "n1 n2 n3 n4",
                date: "year month"
            },
            errorPlacement: function (error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function (form) {
                var f = $("#deposit-form");
                var array = f.serializeArray();
                var obj = objectifyForm(array);
                var carNum = obj.n1.concat(obj.n2);
                carNum = carNum.concat(obj.n3);
                carNum = carNum.concat(obj.n4);
                delete obj.n1;
                delete obj.n2;
                delete obj.n3;
                delete obj.n4;
                obj.number = carNum;
                console.log(obj);
                startLoading();
                $("#external").load("profile/user/make-deposit #content", obj, function (response, status, xhr) {
                    showModal(status, response);
                    bindDepositFormValidation();
                });
                endLoading(LOAD_TIME);
            }
        });
    }

    function humanizeDate() {
        var dates = $("span.date");
        $(dates).each(function (index) {
            var date = new Date(parseInt($(this).text()));
            $(this).text(date);
        });
    }

    function colorifyTable() {
        var red = $(".red");
        $(red).each(function (index) {
            $(this).css("background-color", "#db5555");
        });
        var green = $(".green");
        $(green).each(function (index) {
            $(this).css("background-color", "#487f30");
        });
    }

    function bindRemittanceDataTable() {
        var lang;
        if (!isEnLang()) {
            lang = russianLang;
        }
        $('#remittances').dataTable({
            "language": lang
        });
    }

    function objectifyForm(formArray) {//serialize data function

        var returnArray = {};
        for (var i = 0; i < formArray.length; i++) {
            returnArray[formArray[i]['name']] = formArray[i]['value'];
        }
        return returnArray;
    }

    function startLoading() {
        var over = '<div id="overlay">' +
            '<div id="loading">' +
            '</div></div>';
        $(over).appendTo('#overlay-container');
    }

    function endLoading(timeout) {
        setTimeout(function () {
            $('#overlay').remove();
        }, timeout);
    }

    function isEnLang() {
        var flag = true;
        var footerText = $("footer p").text();
        if (footerText.search("Dev") === -1) {
            flag = false;
        }
        return flag;
    }
});


