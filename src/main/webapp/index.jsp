<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="WEB-INF/jsp/include/main-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="WEB-INF/jsp/include/head-common.jsp" %>
    <meta name="keywords" content="${main_keywords}">
    <meta name="description" content="${main_description}">
    <title>${main_title}</title>
</head>
<body>
<%@include file="WEB-INF/jsp/include/header.jsp" %>
<main>
    <div class="container">
        <div class="row centered">
            <h1>${mainheader}</h1>
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <!--Индикаторы слайдов-->
                <ol class="carousel-indicators">
                    <li class="active" data-target="#carousel" data-slide-to="0"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>

                <!--Слайды-->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="static/img/carousel/stadium.png" alt="">
                        <div class="carousel-caption">
                            <h3>${slide1}</h3>

                        </div>
                    </div>
                    <div class="item">
                        <img src="static/img/carousel/money.png" alt="">
                        <div class="carousel-caption">
                            <h3>${slide2}</h3>
                        </div>
                    </div>
                    <div class="item">
                        <img src="static/img/carousel/profit.png" alt="">
                        <div class="carousel-caption">
                            <h3>${slide3}</h3>
                        </div>
                    </div>
                </div>

                <!--Стрелки переключения слайдов-->
                <a href="#carousel" class="left carousel-control" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a href="#carousel" class="right carousel-control" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>
</main>
<input type="hidden" name="page" value="" id="page">
<%@include file="WEB-INF/jsp/include/footer.jsp" %>
<!--for register form validation js -->
<script type="text/javascript" src="static/js/reg.js"></script>
</body>
</html>
