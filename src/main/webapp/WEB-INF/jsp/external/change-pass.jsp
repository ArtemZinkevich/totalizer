<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/change-pass-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <div class="pass-form">
        <c:if test="${not empty error}">
            <div class="alert alert-danger centered">
                    ${pageScope[error]}
            </div>
        </c:if>
        <c:if test="${not empty success}">
            <div class="alert alert-success centered">
                    ${pageScope[success]}
            </div>
        </c:if>
        <form id="pass-form" accept-charset="UTF-8" method="post">
            <div><label for="old-pass">${pass}</label></div>
            <input type="password" name="oldPass" id="old-pass" placeholder="${pass}">
            <div><label for="new-pass">${new_pass}</label></div>
            <input type="password" name="newPass" id="new-pass" placeholder="${new_pass}">
            <br>
            <input type="submit" value="${change_pass}" id="change-pass">
        </form>
    </div>
</div>
</body>
</html>
