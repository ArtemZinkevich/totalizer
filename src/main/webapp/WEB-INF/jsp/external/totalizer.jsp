<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/totalizer-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <c:choose>
        <c:when test="${(not empty validationErrors and not empty validationErrors.stateError)
        or not empty success or not empty error}">
            <c:set scope="page" var="stateError" value="${validationErrors.stateError}"/>
            <c:if test="${not empty stateError}">
                <div class="row ">
                    <div class="alert alert-danger centered">
                            ${pageScope[stateError]}
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty success}">
                <div class="row">
                    <div class="alert alert-success centered">
                            ${pageScope[success]}
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty error}">
                <div class="row">
                    <div class="alert alert-danger centered">
                            ${pageScope[error]}
                    </div>
                </div>
            </c:if>
        </c:when>
        <c:otherwise>
            <div class="toto-form">
                <div class="container">
                    <c:if test="${not empty validationErrors}">
                        <c:forEach var="err" items="${validationErrors}">
                            <div class="row">
                                <div class="alert alert-danger centered">
                                        ${pageScope[err.value]}
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>
                    <form id="toto-form" accept-charset="UTF-8" method="post">
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <div><label for="toto-name">${totoName}</label></div>
                                <input class="form-control" type="text" name="name" id="toto-name"
                                       placeholder="${totoName}"
                                       value="${totalizer.name}" required>
                            </div>
                            <div class="col-sm-5">
                                <div><label for="jackpot">${jackpot}</label></div>
                                <input class="form-control" type="text" name="jackpot" id="jackpot"
                                       placeholder="${jackpot}"
                                <c:set var="jackpotString" scope="page">
                                    <fmt:formatNumber value="${totalizer.jackpot}" minFractionDigits="0"/>
                                </c:set>
                                       value="${ctg:removeSpace(jackpotString)}"
                                       required>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${not empty totalizer and not empty totalizer.events}">
                                <c:forEach var="ev" items="${totalizer.events}" varStatus="state">
                                    <div class="row">
                                        <h6>
                                                ${event} - ${state.count}
                                        </h6>
                                    </div>
                                    <div class="event">
                                        <div class="form-group row">
                                            <div class="col-xs-10">
                                                <div><label for="date-${state.count}">${date} : UTC</label></div>
                                                <input class="form-control date" name="date-${state.count}"
                                                       type="datetime-local"
                                                       id="date-${state.count}" value="${ev.date}" required>
                                            </div>
                                        </div>
                                        <div class=" form-group row" id="event-${state.count}">
                                            <div class="col-md-2">
                                                <div><label for="eventtype-${state.count}">${type}</label></div>
                                                <select class="form-control type" id="eventtype-${state.count}"
                                                        name="eventtype">
                                                    <ctg:eventSelect type="${ev.type}"/>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="sport-${state.count}">${sport}</label></div>
                                                <input class="form-control sport" type="text"
                                                       name="sport-${state.count}"
                                                       id="sport-${state.count}"
                                                       placeholder="${sport}" value="${ev.sport}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="t1-${state.count}">${t1}</label></div>
                                                <input class="form-control team1" type="text"
                                                       name="team1-${state.count}"
                                                       id="t1-${state.count}"
                                                       placeholder="${t1}" value="${ev.team1}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="t2-${state.count}">${t2}</label></div>
                                                <input class="form-control team2" type="text"
                                                       name="team2-${state.count}"
                                                       id="t2-${state.count}"
                                                       placeholder="${t2}" value="${ev.team2}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="total">
                                                    <div><label for="score-${state.count}">${total}</label></div>
                                                    <input class="form-control score" type="text"
                                                           name="score-${state.count}"
                                                           id="score-${state.count}"
                                                           placeholder="${total}" value="${ev.total}"
                                                           pattern="^\d{1,3}\.5$" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                                <c:forEach begin="1" end="10" varStatus="state">
                                    <div class="row">
                                        <h6>
                                                ${event} - ${state.count}
                                        </h6>
                                    </div>
                                    <div class="event">
                                        <div class="form-group row">
                                            <div class="col-xs-10">
                                                <div><label for="date-${state.count}">${date} : UTC</label></div>
                                                <input class="form-control date" name="date-${state.count}"
                                                       type="datetime-local"
                                                       id="date-${state.count}" required>
                                            </div>
                                        </div>
                                        <div class=" form-group row" id="event-${state.count}">
                                            <div class="col-md-2">
                                                <div><label for="eventtype-${state.count}">${type}</label></div>
                                                <select class="form-control type" id="eventtype-${state.count}"
                                                        name="eventtype">
                                                    <option value="result">${result}</option>
                                                    <option value="score">${score}</option>
                                                    <option value="total">${total}</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="sport-${state.count}">${sport}</label></div>
                                                <input class="form-control sport" type="text"
                                                       name="sport-${state.count}"
                                                       id="sport-${state.count}"
                                                       placeholder="${sport}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="t1-${state.count}">${t1}</label></div>
                                                <input class="form-control team1" type="text"
                                                       name="team1-${state.count}"
                                                       id="t1-${state.count}"
                                                       placeholder="${t1}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div><label for="t2-${state.count}">${t2}</label></div>
                                                <input class="form-control team2" type="text"
                                                       name="team2-${state.count}"
                                                       id="t2-${state.count}"
                                                       placeholder="${t2}" required>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="total">
                                                    <div><label for="score-${state.count}">${total}</label></div>
                                                    <input class="form-control score" type="text"
                                                           name="score-${state.count}"
                                                           id="score-${state.count}"
                                                           placeholder="${total}" pattern="^\d{1,3}\.5$" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:otherwise>
                        </c:choose>
                        <div class="form-group row">
                            <input class="btn btn-success" type="submit" value="${saveBtn}" id="save">
                        </div>

                    </form>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
