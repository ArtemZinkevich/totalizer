<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/userlist-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <c:if test="${not empty error}">
        <div class="alert alert-danger">
                ${pageScope[error]}
        </div>
    </c:if>
    <c:if test="${not empty success}">
        <div class="alert alert-success">
                ${pageScope[success]}
        </div>
    </c:if>
    <c:choose>
        <c:when test="${not empty userlist}">
            <form id="ban-form" accept-charset="UTF-8" method="post">
                <div><label for="userId">${banUser}</label></div>
                <input type="number" name="userId" id="userId" placeholder="ID">
                <br><br>
                <input class="btn btn-danger" type="submit" value="${ban}" id="ban-button">
            </form>
            <br><br>
            <div class="table-responsive">
                <table class="table table-hover" id="userlist">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>${firstname}</th>
                        <th>${lastname}</th>
                        <th>${mail}</th>
                        <th>${regDate}</th>
                        <th>${money}</th>
                        <th>${banStatus}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="elem" items="${userlist}">
                        <tr>
                            <td>${elem.id}</td>
                            <td>${elem.name}</td>
                            <td>${elem.lastName}</td>
                            <td>${elem.email}</td>
                            <td><span class="date">${ctg:toMilliseconds(elem.registrationDate)}</span></td>
                            <td>${elem.money}</td>
                            <td>${elem.banned? "+" : "-" }</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <h4>${noRegistered}</h4>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
