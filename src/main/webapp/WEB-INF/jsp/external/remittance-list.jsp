<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/remittances-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <c:choose>
        <c:when test="${not empty remittances}">
            <div class="table-responsive">
                <table class="table table-hover" id="remittances">
                    <thead>
                    <tr>
                        <th>${message}</th>
                        <th>${sum}</th>
                        <th>${date}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="elem" items="${remittances}">
                        <c:choose>
                            <c:when test="${not elem.type.incoming}">
                                <tr class="red">
                                    <td>${pageScope[elem.type.name]}</td>
                                    <td>${elem.sum}</td>
                                    <td><span class="hide">${ctg:toMilliseconds(elem.dateTime)}</span><span
                                            class="date">${ctg:toMilliseconds(elem.dateTime)}</span></td>
                                </tr>
                            </c:when>
                            <c:otherwise>
                                <tr class="green">
                                    <td>${pageScope[elem.type.name]}</td>
                                    <td>${elem.sum}</td>
                                    <td><span class="hide">${ctg:toMilliseconds(elem.dateTime)}</span><span
                                            class="date">${ctg:toMilliseconds(elem.dateTime)}</span></td>
                                </tr>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:when>
        <c:otherwise>
            <h4>${noRegistered}</h4>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>
