<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/deposit-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <c:if test="${not empty validationErrors}">
                <c:set scope="page" var="sumError" value="${validationErrors.illegalSum}"/>
                <c:set scope="page" var="cardError" value="${validationErrors.illegalCard}"/>
                <c:if test="${not empty sumError}">
                    <div class="alert alert-danger">
                            ${pageScope[sumError]}
                    </div>
                </c:if>
                <c:if test="${not empty cardError}">
                    <div class="alert alert-danger">
                            ${pageScope[cardError]}
                    </div>
                </c:if>
            </c:if>
            <c:if test="${not empty success}">
                <div class="alert alert-success">
                        ${pageScope[success]}
                </div>
            </c:if>
            <div class="credit-card-div">
                <form id="deposit-form" accept-charset="UTF-8" method="post">
                    <div class="panel panel-default">
                        <div class="panel-heading">

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="error" id="error-money"></div>
                                    <label for="c1"><i class="fa fa-usd" aria-hidden="true"></i></label>
                                    <input type="text" placeholder="00000.00" class="currency" maxlength="8"
                                           id="c1" name="money" pattern="^\d{1,5}(\.\d{1,2})?$" step="0.01"
                                           data-error="#error-money"/>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h5 class="text-muted"> ${creditCardNumber}</h5>
                                </div>
                                <div class="error" id="error-number"></div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" class="form-control " placeholder="0000"
                                           maxlength="4" name="n1" id="n1" data-error="#error-number"/>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" class="form-control " placeholder="0000"
                                           maxlength="4" name="n2" id="n2" data-error="#error-number"/>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" class="form-control " placeholder="0000"
                                           maxlength="4" name="n3" id="n3" data-error="#error-number"/>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <input type="text" class="form-control " placeholder="0000"
                                           maxlength="4" name="n4" id="n4" data-error="#error-number"/>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-xs-6">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <span class="help-block text-muted small-font"> ${expiryMonth}</span>
                                            <input type="text" class="form-control" placeholder="MM"
                                                   name="month" data-error="#error-date"
                                                   id="mm" maxlength="2"/>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <span class="help-block text-muted small-font"> ${expiryYear}</span>
                                            <input type="text" class="form-control" placeholder="YY" name="year"
                                                   maxlength="2" id="yy" data-error="#error-date"/>
                                        </div>
                                    </div>
                                    <div class="error" id="error-date"></div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <span class="help-block text-muted small-font">CCV</span>
                                    <input type="text" class="form-control" placeholder="CCV"
                                           maxlength="3" minlength="3" name="cvv"/>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAGp0lEQVR4nO1YPY8cxxF9NdOzPbN7JG/PkuiA9gVHGDBlKiVghfoFBMhAkAAJzvkrqIgBI8b8AWcSTu3YBgQJJmRDDkQ4smgTMAjwyL3jzs7OTD8HM/01O3c8wwKsYBt7d9XV1VWvPrq694Dt2I7t2I7t+B+GjDHv3Lkzyv+hBgkA/K/2PHjwYHRDBPT+/fs70+n0o7Zt94ZrPySYt+1h/AsAUFXV8unTp38yxvz74cOHteU7kPfu3du5efPm7w8ODn49bnTc4BiYIY+e+db1Ifhw7dmzZ3+/e/fux+v1+q+Hh4ctACRWyWw2++jq1asfioh1SgBID1w8XyRcE4H0PDh56dcDPYhoxvpp6cBuB15AT1+58rNfXP/gg0/atr1gcSsAuHXrlrx8+XLv22//5jyO/oL202XCRiv4Sytg5y7iRLeFG3T3octI70yvi4E9z3v96tVPARQAXjkH6rrGP75/Jr99/Dta46TxtOE4P5wbgrRyXsYYkjRWPtAZ6fEyvS3TzQPdnTxEEoi4ylEAYAxBY0BjJFAoxhsVD9DJxHPTy8UypKGQBoZu3YKX03U48B2GQF7SNOouyqUZgDEmjGxAuyjARNEP5MzIvjEeebqdc8inSRIcOecArCADBXGKzUgZ2MiaQO6U1EfrfTbN0NY55BNyLAPdgTWuhIYp7tPqyyQ0LN6AES8/VipDPW8rrU15UgFjDhD9wTEj5WMBm8F8JPVRiZmhntPLzZxTfnjnKABI0xQCAVtjS8BG5qxOE5TXRtmRNF1ziOXPLJXzym84kGUZ3n//Gj77/HMJrr7+Yulp3/gtHyQIUHopS7MrU1qRsb3sL6mOZqzDsXuZTk0nc3h4iCdPnsQOKKWgtcZ8dxcWXHi4Y1rcejj3YnbNFqY/Y26VMX+MdnYZaCGgdb6ZgQCgC3pABnCs5ghMOGfAxBjtyVA8pulv9ci94QMvcoD+HYKyLKmyTKwjXSqHtHNZGKSZfdkg5o3Q3TuoD1Wnc8Dv3kGEUoqz6Uxi6BsZcKAgItCTie8GPiv9T/BeCeZdMvp5B8IlKJyTXt+yLGHaFoad3SRJ0DQNRARN0yDLMiiVubfW0Im4hHxoYYyNle0MDjQHoBk44mNqQdPFGX7J6/z6q69AGs5mO3j+/Dn29/e5WCxwdHSE+XzOX12/Dq21K+ngsG060D9rbVsbpN4BlCDSXQl5ZNIDdpdRXCpdCfUGhCB+ee0aiyKXqlrj5/v7nM2mcvTyCAdXD1gUUymKwqbvbSXUeWBHWZa9w4OuEnQXf37DMtuU88/hsNQ6O3t7c4BAnueutN67fDnSkSRJv+8UB1zH6tFOtB7tEiGWMzvImT1mnLHReYKPTXjUk+IMuAUJwi7BUnDRdJdU0CGHF0/PcyBHad+pQAxo38tt2TC8HMccsOk/X5+OW3K4yUVvM7phhs6INEZFguWBB4Mz4KN5fHwiIoLWtBSIJEkCY1oaUnKdI8syLMsl26YVQ4NpMeXxybEkSUIAorXGJJtwtVpJ/55n27aSJAmatqX02djZ2WHTNFIuSxhjmKpUQKBpW2ZKSdO2mM1mYZZOdyA8eOt6japaI89zqDTFqlphkk2wWpVdWwNRVWu0bYu6rrFYHENrjbouoXUOkRqZylCtK5ycvEGWZUiSBAJAZRnW6zWKogAJNE2LumlgTIuT5RIgkaYK1IRSCj6HZ5UQwmwTaap48aLGalVB5ZokofUEVVUxTVKARJYpiICZUpjOpnj9esFLly5huSyZZQoEMckm/MmeRtM0WNdrpkkKYwzqumaSJNCTCZRKaYyB1jmmxZQEUa0qZJlimqa2A4110fD7APp7oBO+eOGCAERRFAQh3c0Mzudzd4h7Y+7ZsLt7SUBQ69y9RidaC0CoLGNe5E52Z2fm7plEEpnP574RgJhMdNcw6A6OhIdtMwNBrx4cUkcHh3GT9ufUZ9odu4DnT2NUGvRG3N6hPEfSEL2FHOABmNPoMSfCWnUQzgC36VgMOHYUwMADBQBN06Cumyp48Yx0zY3Lyje/UzrjWFsMnBksB33SOxj40FGtaWsAJnLg0aNH3N3d/fLFixf/eufdd67A1pxXJ4ESGQCOL7JxnsUngcsSAB7Ie1vuawkpi8Xi5Ol33/0ZwDJyAADKsvznF1/c/fTGjRu/yfP8MoHUp4yeHPA2pxzlbZbA+XiWs16vy798880f37x58wcAJ1Ys+hf67du3M2PMLoALCP7x+yMZDYAFgNePHz9u/99gtmM7tmM7tuPHMf4DjEOG/uidi0QAAAAASUVORK5CYII="
                                         class="img-rounded"/>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-12 pad-adjust">

                                    <input type="text" class="form-control"
                                           name="person"
                                           placeholder="${nameOnCard}" required/>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                    <a href="profile" class="btn btn-danger">${buttonCancel}</a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 pad-adjust">
                                    <input type="submit" class="btn btn-warning btn-block"
                                           value="${buttonDeposit}"
                                           id="make-deposit"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
