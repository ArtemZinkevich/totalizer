<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/operation-result-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head></head>
<body>
<div id="content">
    <c:choose>
        <c:when test="${not empty success }">
            <div class="row ">
                <div class="alert alert-success centered">
                        ${pageScope[success]}
                </div>
            </div>
        </c:when>
        <c:when test="${not empty validationErrors}">
            <c:forEach var="er" items="${validationErrors}">
                <div class="row ">
                    <div class="alert alert-danger centered">
                            ${pageScope[er.value]}
                    </div>
                </div>
            </c:forEach>
        </c:when>
        <c:when test="${not empty error}">
                <div class="row ">
                    <div class="alert alert-danger centered">
                            ${pageScope[error]}
                    </div>
                </div>
        </c:when>
    </c:choose>
</div>
</body>
</html>
