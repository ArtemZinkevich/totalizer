<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/errorpages-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../include/head-common.jsp" %>
    <meta name="keywords" content="${forbidden_keywords}">
    <meta name="description" content="${forbidden_description}">
    <title>${forbidden_title}</title>
</head>
<body>
<%@include file="../include/header.jsp" %>
<main>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div class="col-xs-12" id="content">
                <br>
                <h1>
                    <b> ${pageContext.errorData.statusCode}</b>,${forbidden_PageMessage}
                </h1>
            </div>
        </div>
    </div>
</main>
<%@include file="../include/footer.jsp" %>
</body>
</html>
