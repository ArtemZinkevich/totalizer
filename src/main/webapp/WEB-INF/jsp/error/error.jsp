<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../include/errorpages-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="../include/head-common.jsp" %>
    <meta name="keywords" content="${error_keywords}">
    <meta name="description" content="${error_description}">
    <title>${error_title}</title>
</head>
<body>
<%@include file="../include/header.jsp" %>
<main>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div class="col-xs-12" id="content">
                <br>
                <h1>
                    <b> ${pageContext.errorData.statusCode}</b>,${error_PageMessage}
                </h1>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">${stackTrace}</h3>
                    </div>
                    <div class="panel-body">
                        <ctg:printStackTrace></ctg:printStackTrace>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<%@include file="../include/footer.jsp" %>
</body>
</html>
