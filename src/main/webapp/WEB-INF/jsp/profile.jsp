<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="ctg" uri="customtags" %>
<%@include file="include/profile-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="include/head-common.jsp" %>
    <meta name="keywords" content="${profile_keywords}">
    <meta name="description" content="${profile_description}">
    <link href="static/css/loading.css" rel="stylesheet">
    <title>${profile_title}</title>
</head>
<body>
<%@include file="include/header.jsp" %>
<main>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#userNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="userNavbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="javascript:void(0)" id="change-pass-page">${changePass}</a>
                    </li>
                    <ctg:isAdmin>
                        <li><a href="javascript:void(0)" id="user-list">${users}</a></li>
                        <li><a href="javascript:void(0)" id="create-toto">${create_toto}</a></li>
                        <li><a href="javascript:void(0)" id="edit-toto">${edit_toto}</a></li>
                        <li><a href="javascript:void(0)" id="start-toto">${start_toto}</a></li>
                        <li><a href="javascript:void(0)" id="cancel-toto">${cancel_toto}</a></li>
                        <li><a href="javascript:void(0)" id="finish-toto">${finish_toto}</a></li>
                    </ctg:isAdmin>
                    <ctg:isUser>
                        <li><a href="javascript:void(0)" id="deposit">${deposit}</a></li>
                        <li><a href="javascript:void(0)" id="cancel-bet">${cancel_bets}</a></li>
                        <li><a href="javascript:void(0)" id="repay-loan">${return_loan}</a></li>
                        <li><a href="javascript:void(0)" id="remittance-list">${remittances}</a></li>
                    </ctg:isUser>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div class="col-xs-12" id="external">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">${info_title}</h3>
                    </div>
                    <div class="panel-body">
                        <p> ${user_welcome} ${user.name} ${user.lastName}</p>
                        <c:if test="${user.role ne 'admin'}">
                            <p> ${user_balance} : ${user.money} </p>
                            <p> ${user_credit} : <fmt:formatNumber value="${credit}" maxFractionDigits="2"/></p>
                        </c:if>

                        <p> ${user_registered} :<span class="date">${ctg:toMilliseconds(user.registrationDate)}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">${ajaxError}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">${modalClose}</button>
                </div>
            </div>
        </div>
    </div>
</main>
<input type="hidden" name="page" value="profile" id="page">
<%@include file="include/footer.jsp" %>
<!--for register form validation js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script type="text/javascript" src="static/js/profile.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>
<ctg:isAdmin>
    <script type="text/javascript" src="static/js/admin.js"></script>
</ctg:isAdmin>
<ctg:isUser>
    <script type="text/javascript" src="static/js/user.js"></script>
</ctg:isUser>
</body>
</html>
