<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/history-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="include/head-common.jsp" %>
    <meta name="keywords" content="${game_keywords}">
    <meta name="description" content="${game_description}">
    <link href="static/css/history.css" rel="stylesheet">
    <title>${game_title}</title>
</head>
<body>
<%@include file="include/header.jsp" %>
<main>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div class="col-xs-12">
                <c:choose>
                    <c:when test="${not empty events}">
                        <div class="table-responsive">
                            <table class="table table-hover" id="remittances">
                                <thead>
                                <tr>
                                    <th>${sport}</th>
                                    <th>${t1}</th>
                                    <th>${t2}</th>
                                    <th>${score1}</th>
                                    <th>${score2}</th>
                                    <th>${date}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="elem" items="${events}">
                                    <tr>
                                        <td>${elem.sport}</td>
                                        <td>${elem.team1}</td>
                                        <td>${elem.team2}</td>
                                        <td>${elem.score1}</td>
                                        <td>${elem.score2}</td>
                                        <td><span class="hide">${ctg:toMilliseconds(elem.date)}</span><span
                                                class="date">${ctg:toMilliseconds(elem.date)}</span></td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="row">
                            <div class="alert alert-warning centered">
                                <h4>${noFinished}</h4>
                            </div>
                        </div>

                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</main>
<input type="hidden" name="page" value="history" id="page">
<%@include file="include/footer.jsp" %>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>
<script type="text/javascript" src="static/js/history.js"></script>
</body>
</html>
