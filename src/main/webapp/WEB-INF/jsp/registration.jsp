<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/register-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="include/head-common.jsp" %>
    <meta name="keywords" content="${reg_keywords}">
    <meta name="description" content="${reg_description}">
    <link rel="stylesheet" href="static/css/regpage.css">
    <title>${reg_title}</title>
</head>
<body>
<%@include file="include/header.jsp" %>
<main>
    <div class="registration-form">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                    <div class="panel panel-login">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="login-form-link">${loglink}</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" id="register-form-link">${register}</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="login-form" accept-charset="UTF-8" action="login" method="post"
                                          role="form" style="display: block;">
                                        <c:if test="${not empty loginError}">
                                            <div class="alert alert-danger">
                                                    ${pageScope[loginError]}
                                            </div>
                                            <c:remove var="loginError" scope="session"/>
                                        </c:if>
                                        <c:if test="${not empty registerOk}">
                                            <div class="alert alert-success">
                                                    ${pageScope[registerOk]}
                                            </div>
                                            <c:remove var="registerOk" scope="session"/>
                                        </c:if>
                                        <div class="form-group">
                                            <input type="email" name="email" id="email"
                                                   class="form-control" placeholder="${mail}" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="log-password" id="log-password"
                                                   class="form-control" placeholder="${pass}" required>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="login-submit" id="login-submit"
                                                           class="form-control btn btn-login"
                                                           value="${logbtn}">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <form id="register-form" accept-charset="UTF-8" action="create" method="post"
                                          role="form" style="display: none;">
                                        <div class="form-group">
                                            <div class="err">
                                                <c:set scope="request" var="nameError" value="${errors.name}"/>
                                                <span id="err-name" class="err">${pageScope[nameError]}</span>
                                            </div>
                                            <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user-circle" aria-hidden="true"></i>
                </span>
                                                <input type="text" name="name" id="firstName"
                                                       class="form-control" placeholder="${firstname}" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="err">
                                                <c:set scope="request" var="lnameError" value="${errors.lastName}"/>
                                                <span id="err-lname" class="err">${pageScope[lnameError]}</span>
                                            </div>
                                            <div class="input-group">
                <span class="input-group-addon">
                  <i class="fa fa-user-circle" aria-hidden="true"></i>
                </span>
                                                <input type="text" name="lastName" id="lastName"
                                                       class="form-control" placeholder="${lastname}" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="err">
                                                <c:set scope="request" var="mailError" value="${errors.mail}"/>
                                                <span id="err-mail" class="err">${pageScope[mailError]}</span>
                                            </div>
                                            <div class="input-group">
                <span class="input-group-addon">
                 <i class="fa fa-envelope-open-o" aria-hidden="true"></i>
                </span>
                                                <input type="email" name="mail" id="mail"
                                                       class="form-control" placeholder="${mail}" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="err">
                                                <c:set scope="request" var="passError" value="${errors.password}"/>
                                                <span id="err-password" class="err">${pageScope[passError]}</span>
                                            </div>
                                            <div class="input-group">
                <span class="input-group-addon">
                 <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
                                                <input type="password" name="password" id="password"
                                                       class="form-control" placeholder="${pass}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="err">
                                                <c:set scope="request" var="pass2Error" value="${errors.password2}"/>
                                                <span id="err-password2" class="err">${pageScope[pass2Error]}</span>
                                            </div>
                                            <div class="input-group">
                <span class="input-group-addon">
                 <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
                                                <input type="password" name="confirm-password" id="confirm-password"
                                                       class="form-control" placeholder="${pass_confirm}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="register-submit" id="register-submit"
                                                           class="form-control btn btn-register"
                                                           value="${register}">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br><br><br>
    </div>
</main>
<input type="hidden" name = "page" value="register" id="page">
<%@include file="include/footer.jsp" %>
<!--for register form validation js -->
<script type="text/javascript" src="static/js/reg.js"></script>
<c:if test="${not empty errors}">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#register-form-link").click();
        })
    </script>
    <c:remove var="errors" scope="session"/>
</c:if>
</body>
</html>