<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/game-i18n.jsp" %>
<%@taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="include/head-common.jsp" %>
    <meta name="keywords" content="${game_keywords}">
    <meta name="description" content="${game_description}">
    <link href="static/css/loading.css" rel="stylesheet">
    <link href="static/css/game.css" rel="stylesheet">
    <title>${game_title}</title>
</head>
<body>
<%@include file="include/header.jsp" %>
<main>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div id="external">
                <div class="col-xs-12" id="content">
                    <c:choose>
                        <c:when test="${not empty success }">
                            <div class="row">
                                <div class="alert alert-success centered">
                                        ${pageScope[success]}
                                </div>
                            </div>
                        </c:when>
                        <c:when test="${not empty totalizer and not empty totalizer.events}">
                            <c:if test="${not empty validationErrors}">
                                <c:forEach var="er" items="${validationErrors}">
                                    <div class="row ">
                                        <div class="alert alert-danger centered">
                                                ${pageScope[er.value]}
                                        </div>
                                    </div>
                                </c:forEach>
                            </c:if>
                            <div class="row toto-rules">
                                <div class=" col-xs-10 col-xs-offset-1 panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">${totoName} : ${totalizer.name}</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="alert alert-success centered">
                                                ${jackpot} : <fmt:formatNumber value="${totalizer.jackpot}"
                                                                               maxFractionDigits="0"/>$
                                        </div>
                                        <div class="alert alert-success centered">
                                                ${betSum} :${totalizer.betSum} $
                                        </div>
                                        <h4>${shortRules} : </h4>
                                        <ul class="list-group">
                                            <c:if test="${empty user}">
                                                <li class="list-group-item list-group-item-warning">${onlyLogined}
                                                    <a href="register">${loglink}</a></li>
                                            </c:if>
                                            <li class="list-group-item list-group-item-warning">${creditRule}</li>
                                            <li class="list-group-item list-group-item-warning">${allEventRule}</li>
                                            <li class="list-group-item list-group-item-warning">${resultRule}</li>
                                            <li class="list-group-item list-group-item-warning">${totalRule}</li>
                                            <li class="list-group-item list-group-item-warning">${scoreRule}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="bets-form col-xs-12">
                                    <form id="bet-form" accept-charset="UTF-8" method="post">
                                        <c:forEach var="ev" items="${totalizer.events}" varStatus="state">
                                            <div class="bet">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <input type="hidden" name="eventId" value="${ev.id}"
                                                               id="eventId">
                                                        <input type="hidden" name="eventType" id="eventType"
                                                               value="${ev.type.name}">
                                                        <h6>
                                                                ${event} - ${state.count}
                                                        </h6>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                            ${date} : <span
                                                            class="date">${ctg:toMilliseconds(ev.date)}</span>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="centered">${sport}</div>
                                                        <br>
                                                        <div class="centered">${ev.sport}</div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="centered">${t1}</div>
                                                        <br>
                                                        <div class="centered">${ev.team1}</div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="centered">${t2}</div>
                                                        <br>
                                                        <div class="centered">${ev.team2}</div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="centered">${pageScope[ev.type.name]}</div>
                                                        <br>
                                                        <div class="centered">
                                                            <c:choose>
                                                                <c:when test="${ev.type eq 'RESULT'}">
                                                                    <div class="result">
                                                                        <div class="form-group row">
                                                                            <div class="col-xs-4">
                                                                                <label class="radio-inline">
                                                                                    <input type="radio"
                                                                                           name="result-${state.count}"
                                                                                           value="win"
                                                                                           id="win-${state.count}"
                                                                                           checked>
                                                                                    X1
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <label class="radio-inline">
                                                                                    <input type="radio"
                                                                                           name="result-${state.count}"
                                                                                           value="draw"
                                                                                           id="draw-${state.count}"
                                                                                    >
                                                                                    X0
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <label class="radio-inline">
                                                                                    <input type="radio"
                                                                                           name="result-${state.count}"
                                                                                           value="loss"
                                                                                           id="loss-${state.count}"
                                                                                    >
                                                                                    X2
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${ev.type eq 'TOTAL'}">
                                                                    <div class="total">
                                                                        <div class="form-group row">
                                                                            <div class="col-xs-4">
                                                                                <label class="radio-inline">
                                                                                    <input type="radio"
                                                                                           name="total-${state.count}"
                                                                                           value="false"
                                                                                           id="less-${state.count}"
                                                                                           checked>
                                                                                        ${less}
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                    ${ev.total}
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <label class="radio-inline">
                                                                                    <input type="radio"
                                                                                           name="total-${state.count}"
                                                                                           value="true"
                                                                                           id="more-${state.count}">
                                                                                        ${more}
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${ev.type eq 'SCORE'}">
                                                                    <div class="score">
                                                                        <div class="form-group row">
                                                                            <div class="col-xs-3 col-xs-offset-2">
                                                                                <input class="scoreT1" type="text"
                                                                                       name="scoreT1-${state.count}"
                                                                                       id="scoreT1-${state.count}"
                                                                                       size="3" maxlength="3">
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                -
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <input class="scoreT2" type="text"
                                                                                       name="scoreT2-${state.count}"
                                                                                       id="scoreT2-${state.count}"
                                                                                       size="3" maxlength="3">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:when>
                                                            </c:choose>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                        <div class="row form-group centered">
                                            <div class="col-xs-6">
                                                <div>
                                                    <label for="bet-sum">${yourBet} :</label>
                                                </div>
                                                <input type="text" id="bet-sum" name="betSum" required>
                                            </div>
                                            <div class="col-xs-6">
                                                <div>
                                                    <label for="bet-credit">${yourCredit} :</label>
                                                </div>
                                                <input type="number" id="bet-credit" name="betCredit">
                                            </div>
                                        </div>
                                        <c:choose>
                                            <c:when test="${empty user}">
                                                <div class="row ">
                                                    <div class="alert alert-warning centered">
                                                            ${onlyLogined}
                                                    </div>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <ctg:isUser>
                                                    <div class="row form-group centered">
                                                        <input class="btn btn-success" type="submit"
                                                               value="${makeBetBtn}"
                                                               id="make-bet">
                                                    </div>
                                                </ctg:isUser>
                                                <ctg:isAdmin>
                                                    <div class="row ">
                                                        <div class="alert alert-warning centered">
                                                                ${notforAdmin}
                                                        </div>
                                                    </div>
                                                </ctg:isAdmin>
                                            </c:otherwise>
                                        </c:choose>
                                    </form>
                                </div>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <c:choose>
                                <c:when test="${not empty validationErrors}">
                                    <c:forEach var="er" items="${validationErrors}">
                                        <div class="row ">
                                            <div class="alert alert-danger centered">
                                                    ${pageScope[er.value]}
                                            </div>
                                        </div>
                                    </c:forEach>
                                </c:when>
                                <c:otherwise>
                                    <div class="row ">
                                        <div class="alert alert-warning centered">
                                                ${notAvailable}
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">×</button>
                    <h4 class="modal-title">${ajaxError}</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-default" type="button" data-dismiss="modal">${modalClose}</button>
                </div>
            </div>
        </div>
    </div>
</main>
<input type="hidden" name="page" value="game" id="page">
<%@include file="include/footer.jsp" %>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script type="text/javascript" src="static/js/game.js"></script>
</body>
</html>
