<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="include/rules-i18n.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="include/head-common.jsp" %>
    <meta name="keywords" content="${rules_keywords}">
    <meta name="description" content="${rules_description}">
    <title>${rules_title}</title>
</head>
<body>
<%@include file="include/header.jsp" %>
<main>
    <div class="container-fluid">
        <div class="row" id="overlay-container">
            <div class="col-xs-12">
                <jsp:include page="${link}"/>
            </div>
        </div>
    </div>
</main>
<input type="hidden" name="page" value="rules" id="page">
<%@include file="include/footer.jsp" %>
</body>
</html>
