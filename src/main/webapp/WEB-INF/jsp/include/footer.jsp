<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="footer-i18n.jsp" %>
<footer>
    <div class="container">
        <div class="row centered">
            <a href="https://twitter.com" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
            <a href="www.facebook.com/" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
            <a href="https://vk.com/" target="_blank"><i class="fa fa-vk fa-2x" aria-hidden="true"></i></a>
            <a href="https://bitbucket.org/ArtemZinkevich/totalizer" target="_blank"><i class="fa fa-bitbucket fa-2x"
                                                                                        aria-hidden="true"></i></a>
        </div><!-- row -->
    </div><!-- container -->
    <div class="container">
        <p class="text-center text-info">${footertxt}<i class="fa fa-copyright" aria-hidden="true"></i></p>
    </div>
</footer>
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="static/js/header.js"></script>
