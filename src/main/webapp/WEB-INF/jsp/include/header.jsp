<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="header-i18n.jsp" %>
<header>
    <!-- Fixed navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href=""><i class="fa fa-bolt" aria-hidden="true"></i>P<i
                        class="fa fa-circle"></i>RT</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="">${homebtn} <i
                            class="fa fa-home"
                            aria-hidden="true"></i></a>
                    </li>
                    </li>
                    <li><a href="rules">${rulesbtn} <i
                            class="fa fa-file-text-o"
                            aria-hidden="true"></i></a>
                    </li>
                    <li><a href="javascript:void(0)" id="game">${playbtn} <i
                            class="fa fa-futbol-o"
                            aria-hidden="true"></i></a></li>
                    <li><a href="history" id="history">${history}</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${not empty user}">
                            <li><a href="javascript:void(0)" id="profile"
                                   onclick="post('go-profile-updateTotalizerBetSum')"><span
                                    class="glyphicon glyphicon-user"></span>${profilebtn}</a></li>
                            <li><a href="javascript:void(0)" id="logout" onclick="post('logout')"><span
                                    class="glyphicon glyphicon-off"></span>${logoutbtn}</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="register"><span class="glyphicon glyphicon-user"></span>${loginbtn}</a></li>
                        </c:otherwise>
                    </c:choose>

                    <li class="dropdown">
                        <a class=" btn dropdown-toggle" data-toggle="dropdown" href="#"><img
                                src="${langicon}"/>${langtext}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a id="en-lang" href="javascript:void(0)"> <img
                                        src="static/img/flags/US.png"/>English</a></li>
                            <li>
                                <a id="ru-lang" href="javascript:void(0)"> <img
                                        src="static/img/flags/RU.png"/>Русский</a></li>
                            <li>
                        </ul>
                </ul>
            </div>
        </div>
    </nav>
</header>