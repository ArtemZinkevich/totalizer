<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.rules.keywords" var="rules_keywords"/>
<fmt:message bundle="${loc}" key="project.rules.description" var="rules_description"/>
<fmt:message bundle="${loc}" key="project.rules.title" var="rules_title"/>
<fmt:message bundle="${loc}" key="project.rules.link" var="link"/>
