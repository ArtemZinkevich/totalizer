<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.home" var="homebtn" />
<fmt:message bundle="${loc}" key="project.toto" var="playbtn" />
<fmt:message bundle="${loc}" key="project.history" var="history" />
<fmt:message bundle="${loc}" key="project.rules" var="rulesbtn" />
<fmt:message bundle="${loc}" key="project.login-register" var="loginbtn" />
<fmt:message bundle="${loc}" key="project.profile" var="profilebtn" />
<fmt:message bundle="${loc}" key="project.logout" var="logoutbtn" />
<fmt:message bundle="${loc}" key="project.langtext" var="langtext" />
<fmt:message bundle="${loc}" key="project.langicon" var="langicon" />

