<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>


<fmt:message bundle="${loc}" key="project.operation.cancellationFailed" var="cancellationFailed" />
<fmt:message bundle="${loc}" key="project.totalizer.deletionError" var="deletionError"/>
<fmt:message bundle="${loc}" key="project.totalizer.deletionSuccess" var="cancellationSuccess"/>
<fmt:message bundle="${loc}" key="project.operation.couponDelSuccess" var="couponDelSuccess"/>
<fmt:message bundle="${loc}" key="project.operation.couponDelFailed" var="couponDelFailed"/>
<fmt:message bundle="${loc}" key="project.game.dateError" var="timeIsOver"/>
<fmt:message bundle="${loc}" key="project.operation.finishSuccess" var="finishSuccess"/>
<fmt:message bundle="${loc}" key="project.operation.finishFailed" var="finishFailed"/>
<fmt:message bundle="${loc}" key="project.operation.finishingError" var="finishingError"/>
<fmt:message bundle="${loc}" key="project.operation.repaySuccess" var="repaySuccess"/>
<fmt:message bundle="${loc}" key="project.operation.repayFailed" var="repayFailed"/>




