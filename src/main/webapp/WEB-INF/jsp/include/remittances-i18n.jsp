<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.remittances.message" var="message"/>
<fmt:message bundle="${loc}" key="project.remittances.sum" var="sum"/>
<fmt:message bundle="${loc}" key="project.remittances.date" var="date"/>
<fmt:message bundle="${loc}" key="project.remittances.bet" var="bet"/>
<fmt:message bundle="${loc}" key="project.remittances.debt" var="debt"/>
<fmt:message bundle="${loc}" key="project.remittances.refund" var="refund"/>
<fmt:message bundle="${loc}" key="project.remittances.deposit" var="deposit"/>


