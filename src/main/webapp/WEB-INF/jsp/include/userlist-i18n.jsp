<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.userlist.banUser" var="banUser"/>
<fmt:message bundle="${loc}" key="project.userlist.banButton" var="ban"/>
<fmt:message bundle="${loc}" key="project.userlist.firstname" var="firstname"/>
<fmt:message bundle="${loc}" key="project.userlist.lastname" var="lastname"/>
<fmt:message bundle="${loc}" key="project.userlist.mail" var="mail"/>
<fmt:message bundle="${loc}" key="project.userlist.regDate" var="regDate"/>
<fmt:message bundle="${loc}" key="project.userlist.banStatus" var="banStatus"/>
<fmt:message bundle="${loc}" key="project.userlist.noRegistered" var="noRegistered"/>
<fmt:message bundle="${loc}" key="project.userlist.money" var="money"/>
<fmt:message bundle="${loc}" key="project.userlist.success" var="banSuccess"/>
<fmt:message bundle="${loc}" key="project.userlist.error" var="banFail"/>
