<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.profile.description" var="profile_description" />
<fmt:message bundle="${loc}" key="project.profile.keywords" var="profile_keywords" />
<fmt:message bundle="${loc}" key="project.profile.title" var="profile_title" />
<fmt:message bundle="${loc}" key="project.profile.changepass" var="changePass" />
<fmt:message bundle="${loc}" key="project.profile.users" var="users" />
<fmt:message bundle="${loc}" key="project.profile.create-totolizer" var="create_toto" />
<fmt:message bundle="${loc}" key="project.profile.edit-totolizer" var="edit_toto" />
<fmt:message bundle="${loc}" key="project.profile.start-totolizer" var="start_toto" />
<fmt:message bundle="${loc}" key="project.profile.cancel-totolizer" var="cancel_toto" />
<fmt:message bundle="${loc}" key="project.profile.finish_toto" var="finish_toto" />
<fmt:message bundle="${loc}" key="project.profile.manage-card" var="manage_card" />
<fmt:message bundle="${loc}" key="project.profile.deposit" var="deposit" />
<fmt:message bundle="${loc}" key="project.profile.return-loan" var="return_loan" />
<fmt:message bundle="${loc}" key="project.profile.cancel-bets" var="cancel_bets" />
<fmt:message bundle="${loc}" key="project.profile.remittances" var="remittances" />
<fmt:message bundle="${loc}" key="project.profile.info-title" var="info_title"/>
<fmt:message bundle="${loc}" key="project.profile.user-welcome" var="user_welcome" />
<fmt:message bundle="${loc}" key="project.profile.user-balance" var="user_balance" />
<fmt:message bundle="${loc}" key="project.profile.user-credit" var="user_credit"/>
<fmt:message bundle="${loc}" key="project.profile.user-registered" var="user_registered"/>
<fmt:message bundle="${loc}" key="project.profile.ajax-error" var="ajaxError"/>
<fmt:message bundle="${loc}" key="project.profile.modal-close" var="modalClose"/>
