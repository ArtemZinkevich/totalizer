<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.change-pass.pass" var="pass" />
<fmt:message bundle="${loc}" key="project.change-pass.newpass" var="new_pass" />
<fmt:message bundle="${loc}" key="project.change-pass.change" var="change_pass" />
<fmt:message bundle="${loc}" key="project.change-pass.passfailed" var="error_pass" />
<fmt:message bundle="${loc}" key="project.change-pass.passok" var="ok_pass" />

