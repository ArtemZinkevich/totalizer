<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.reg.description" var="reg_description" />
<fmt:message bundle="${loc}" key="project.reg.keywords" var="reg_keywords" />
<fmt:message bundle="${loc}" key="project.reg.title" var="reg_title" />

<fmt:message bundle="${loc}" key="project.reg.reglink" var="register" />
<fmt:message bundle="${loc}" key="project.reg.loglink" var="loglink" />
<fmt:message bundle="${loc}" key="project.reg.loginbtn" var="logbtn" />
<fmt:message bundle="${loc}" key="project.reg.email-field" var="mail" />
<fmt:message bundle="${loc}" key="project.reg.password-field" var="pass" />
<fmt:message bundle="${loc}" key="project.reg.password-confirm" var="pass_confirm" />
<fmt:message bundle="${loc}" key="project.reg.firstname-field" var="firstname" />
<fmt:message bundle="${loc}" key="project.reg.lastname-field" var="lastname" />
<fmt:message bundle="${loc}" key="project.reg.remember" var="remember" />
<fmt:message bundle="${loc}" key="project.reg.emptyField" var="emptyField" />
<fmt:message bundle="${loc}" key="project.reg.incorrectMail" var="incorrectMail" />
<fmt:message bundle="${loc}" key="project.reg.mailInUse" var="mailInUse" />
<fmt:message bundle="${loc}" key="project.reg.incorrectPass" var="incorrectPass" />
<fmt:message bundle="${loc}" key="project.reg.unconfirmed" var="inconfirmed" />
<fmt:message bundle="${loc}" key="project.login.bad-data" var="loginFailed" />
<fmt:message bundle="${loc}" key="project.login.banned" var="banned"/>
<fmt:message bundle="${loc}" key="project.reg.registerOk" var="successfulRegistration"/>