<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.totalizer.totoName" var="totoName"/>
<fmt:message bundle="${loc}" key="project.totalizer.jackpot" var="jackpot"/>
<fmt:message bundle="${loc}" key="project.game.betSum" var="betSum"/>
<fmt:message bundle="${loc}" key="project.game.shortRules" var="shortRules"/>
<fmt:message bundle="${loc}" key="project.game.onlyLogined" var="onlyLogined"/>
<fmt:message bundle="${loc}" key="project.game.creditRule" var="creditRule"/>
<fmt:message bundle="${loc}" key="project.game.allEventRule" var="allEventRule"/>
<fmt:message bundle="${loc}" key="project.game.resultRule" var="resultRule"/>
<fmt:message bundle="${loc}" key="project.game.makeBetBtn" var="makeBetBtn"/>
<fmt:message bundle="${loc}" key="project.game.more" var="more"/>
<fmt:message bundle="${loc}" key="project.game.less" var="less"/>
<fmt:message bundle="${loc}" key="project.game.yourBet" var="yourBet"/>
<fmt:message bundle="${loc}" key="project.game.yourCredit" var="yourCredit"/>
<fmt:message bundle="${loc}" key="project.game.totalRule" var="totalRule"/>
<fmt:message bundle="${loc}" key="project.game.scoreRule" var="scoreRule"/>
<fmt:message bundle="${loc}" key="project.reg.loglink" var="loglink"/>
<fmt:message bundle="${loc}" key="project.totalizer.event" var="event"/>
<fmt:message bundle="${loc}" key="project.totalizer.notAvailable" var="notAvailable"/>
<fmt:message bundle="${loc}" key="project.totalizer.notforAdmin" var="notforAdmin"/>
<fmt:message bundle="${loc}" key="project.totalizer.date" var="date"/>
<fmt:message bundle="${loc}" key="project.totalizer.type" var="type"/>
<fmt:message bundle="${loc}" key="project.totalizer.result" var="result"/>
<fmt:message bundle="${loc}" key="project.totalizer.score" var="score"/>
<fmt:message bundle="${loc}" key="project.totalizer.total" var="total"/>
<fmt:message bundle="${loc}" key="project.totalizer.sport" var="sport"/>
<fmt:message bundle="${loc}" key="project.totalizer.t1" var="t1"/>
<fmt:message bundle="${loc}" key="project.totalizer.t2" var="t2"/>
<fmt:message bundle="${loc}" key="project.profile.ajax-error" var="ajaxError"/>
<fmt:message bundle="${loc}" key="project.profile.modal-close" var="modalClose"/>
<fmt:message bundle="${loc}" key="project.game.creationSuccess" var="creationSuccess"/>
<fmt:message bundle="${loc}" key="project.game.alreadyExist" var="alreadyExist"/>
<fmt:message bundle="${loc}" key="project.game.dateError" var="timeIsOver"/>
<fmt:message bundle="${loc}" key="project.game.noUserError" var="noUserError"/>
<fmt:message bundle="${loc}" key="project.game.lowBalanceError" var="lowBalanceError"/>
<fmt:message bundle="${loc}" key="project.game.betsError" var="betsError"/>
<fmt:message bundle="${loc}" key="project.totalizer.notAvailable" var="totalizerNotAvailable"/>
<fmt:message bundle="${loc}" key="project.game.game_keywords" var="game_keywords"/>
<fmt:message bundle="${loc}" key="project.game.game_description" var="game_description"/>
<fmt:message bundle="${loc}" key="project.totalizer.game_title" var="game_title"/>

