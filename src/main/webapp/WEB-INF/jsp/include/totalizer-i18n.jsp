<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.totalizer.totoName" var="totoName"/>
<fmt:message bundle="${loc}" key="project.totalizer.jackpot" var="jackpot"/>
<fmt:message bundle="${loc}" key="project.totalizer.event" var="event"/>
<fmt:message bundle="${loc}" key="project.totalizer.date" var="date"/>
<fmt:message bundle="${loc}" key="project.totalizer.type" var="type"/>
<fmt:message bundle="${loc}" key="project.totalizer.result" var="result"/>
<fmt:message bundle="${loc}" key="project.totalizer.score" var="score"/>
<fmt:message bundle="${loc}" key="project.totalizer.total" var="total"/>
<fmt:message bundle="${loc}" key="project.totalizer.sport" var="sport"/>
<fmt:message bundle="${loc}" key="project.totalizer.t1" var="t1"/>
<fmt:message bundle="${loc}" key="project.totalizer.t2" var="t2"/>
<fmt:message bundle="${loc}" key="project.totalizer.saveBtn" var="saveBtn"/>
<fmt:message bundle="${loc}" key="project.totalizer.creationError" var="creationError"/>
<fmt:message bundle="${loc}" key="project.totalizer.editionError" var="editionError"/>
<fmt:message bundle="${loc}" key="project.totalizer.deletionError" var="deletionError"/>
<fmt:message bundle="${loc}" key="project.totalizer.nameError" var="nameError"/>
<fmt:message bundle="${loc}" key="project.totalizer.jackpotError" var="jackpotError"/>
<fmt:message bundle="${loc}" key="project.totalizer.typeError" var="typeError"/>
<fmt:message bundle="${loc}" key="project.totalizer.sportError" var="sportError"/>
<fmt:message bundle="${loc}" key="project.totalizer.teamError" var="teamError"/>
<fmt:message bundle="${loc}" key="project.totalizer.dateError" var="dateError"/>
<fmt:message bundle="${loc}" key="project.totalizer.totalScoreError" var="totalScoreError"/>
<fmt:message bundle="${loc}" key="project.totalizer.timeError" var="timeError"/>
<fmt:message bundle="${loc}" key="project.totalizer.creationSuccess" var="creationSuccess"/>
<fmt:message bundle="${loc}" key="project.totalizer.editionSuccess" var="editionSuccess"/>
<fmt:message bundle="${loc}" key="project.totalizer.deletionSuccess" var="deletionSuccess"/>
<fmt:message bundle="${loc}" key="project.totalizer.startSuccess" var="startSuccess"/>
<fmt:message bundle="${loc}" key="project.totalizer.notAvailable" var="notAvailable"/>


