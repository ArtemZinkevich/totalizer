<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>


<fmt:message bundle="${loc}" key="project.totalizer.sport" var="sport"/>
<fmt:message bundle="${loc}" key="project.totalizer.t1" var="t1"/>
<fmt:message bundle="${loc}" key="project.totalizer.t2" var="t2"/>
<fmt:message bundle="${loc}" key="project.totalizer.date" var="date"/>
<fmt:message bundle="${loc}" key="project.history.history_keywords" var="game_keywords"/>
<fmt:message bundle="${loc}" key="project.history.history_description" var="game_description"/>
<fmt:message bundle="${loc}" key="project.history.history_title" var="game_title"/>
<fmt:message bundle="${loc}" key="project.history.no_finished" var="noFinished"/>
<fmt:message bundle="${loc}" key="project.history.score1" var="score1"/>
<fmt:message bundle="${loc}" key="project.history.score2" var="score2"/>

