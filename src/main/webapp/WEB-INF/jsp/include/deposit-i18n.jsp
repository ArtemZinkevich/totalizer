<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.deposit.wrongSum" var="illegalSum" />
<fmt:message bundle="${loc}" key="project.deposit.wrongCard" var="illegalCard"/>
<fmt:message bundle="${loc}" key="project.deposit.depositSuccess" var="depositSuccess"/>
<fmt:message bundle="${loc}" key="project.deposit.creditCardNumber" var="creditCardNumber" />
<fmt:message bundle="${loc}" key="project.deposit.expiryMonth" var="expiryMonth"/>
<fmt:message bundle="${loc}" key="project.deposit.expiryYear" var="expiryYear"/>
<fmt:message bundle="${loc}" key="project.deposit.nameOnCard" var="nameOnCard" />
<fmt:message bundle="${loc}" key="project.deposit.buttonCancel" var="buttonCancel"/>
<fmt:message bundle="${loc}" key="project.deposit.buttonDeposit" var="buttonDeposit"/>


