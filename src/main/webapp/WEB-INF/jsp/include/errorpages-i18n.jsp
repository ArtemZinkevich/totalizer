<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc" scope="page"/>

<fmt:message bundle="${loc}" key="project.errorpage.nfkeywords" var="notFound_keywords"/>
<fmt:message bundle="${loc}" key="project.errorpage.nfdescroption" var="notFound_description"/>
<fmt:message bundle="${loc}" key="project.errorpage.nftitle" var="notFound_title"/>
<fmt:message bundle="${loc}" key="project.errorpage.nfpageMessage" var="notFoundPageMessage"/>
<fmt:message bundle="${loc}" key="project.errorpage.unauthKeywords" var="unauthenticated_keywords"/>
<fmt:message bundle="${loc}" key="project.errorpage.unauthDescroption" var="unauthenticated_description"/>
<fmt:message bundle="${loc}" key="project.errorpage.unauthTitle" var="unauthenticated_title"/>
<fmt:message bundle="${loc}" key="project.errorpage.unauthMessage" var="unauthenticated_PageMessage"/>
<fmt:message bundle="${loc}" key="project.reg.loglink" var="loglink"/>
<fmt:message bundle="${loc}" key="project.errorpage.forbiddenKeywords" var="forbidden_keywords"/>
<fmt:message bundle="${loc}" key="project.errorpage.forbiddenDescroption" var="forbidden_description"/>
<fmt:message bundle="${loc}" key="project.errorpage.forbiddenTitle" var="forbidden_title"/>
<fmt:message bundle="${loc}" key="project.errorpage.forbiddenMessage" var="forbidden_PageMessage"/>
<fmt:message bundle="${loc}" key="project.errorpage.errorKeywords" var="error_keywords"/>
<fmt:message bundle="${loc}" key="project.errorpage.errorDescroption" var="error_description"/>
<fmt:message bundle="${loc}" key="project.errorpage.errorTitle" var="error_title"/>
<fmt:message bundle="${loc}" key="project.errorpage.errorMessage" var="error_PageMessage"/>
<fmt:message bundle="${loc}" key="project.errorpage.stackTrace" var="stackTrace"/>




