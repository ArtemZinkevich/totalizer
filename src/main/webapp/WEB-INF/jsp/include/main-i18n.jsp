<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="project" var="loc"/>

<fmt:message bundle="${loc}" key="project.main.header" var="mainheader" />
<fmt:message bundle="${loc}" key="project.main.firstslide" var="slide1" />
<fmt:message bundle="${loc}" key="project.main.secondslide" var="slide2" />
<fmt:message bundle="${loc}" key="project.main.thirdslide" var="slide3" />
<fmt:message bundle="${loc}" key="project.main.keywords" var="main_keywords" />
<fmt:message bundle="${loc}" key="project.main.description" var="main_description" />
<fmt:message bundle="${loc}" key="project.main.title" var="main_title" />

