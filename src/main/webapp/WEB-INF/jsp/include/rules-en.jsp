<%@page contentType="text/html;charset=UTF-8" %>
<article>
    <h2> Tote rules: how to play and win? </h2>
    <section>
        <h3> 1. General provisions </h3>
        <p>
            <b> 1.1 </b> Bets are accepted only from people who agree with
            These rules. Any bet is a confirmation that
            The client knows these rules and agrees with them. Administration
            Has the right to cancel invalid bids from
            Violation of these Rules.
        </p>
        <p>
            <b> 1.2 </b> The administration has the right not to accept bets from individuals who do not
            Fulfilling the present rules.
        </p>
        <p>
            <b> 1.3 </b> In case of fraudulent activities related to
            Acceptance of rates or financial operations, the administration has the right to
            Prevent such actions by declaring the rates invalid and
            Appeals to law enforcement agencies on the fact of committing
            Fraudulent activities.
        </p>
        <p>
            <b> 1.4 </b> Claims for disputed issues are accepted within 10
            Days after the end of the event. The final decision on all disputable
            Situations is taken by the administration.
        </p>
        <p>
            <b> 1.5 </b> The administration has the right to cancel the tote without
            Explanations of the reasons. In this case, all users' funds
            Return in full volume.
        </p>
    </section>
    <section>
        <h3> 2. Opening, replenishment of deposit and receipt of winnings </h3>
        <p>
            <b> 2.1 </b> First of all, the client must register in the system.
            The registration must include the surname, name and e-mail address.
            In case of incorrectly specified registration data, the administration leaves
            The right to suspend operations of this client and to recognize rates
            Invalid.
        </p>
        <p>
            <b> 2.2 </b> For increased security, we recommend periodically
            Change password. You can change the password in the subsection "Change Password"
            Profile page.
        </p>
        <p>
            <b> 2.3 </b> The client does not have the right to authorize third parties to use
            Your account.
        </p>
        <p>
            <b> 2.4 </b> Payouts are made within 3 working days
            Days from the date of the last coupon game.
        </p>
        <p>
            <b> 2.5 </b> Bets are not accepted for the participants
            Events for which bets are made (athletes, coaches, referees and
            Etc.), as well as acting on behalf of the participants of the events. The rates are not
            Are accepted from persons representing the interests of other sweepstakes.
        </p>
    </section>
    <section>
        <h3> 3. Basic rules for taking bets </h3>
        <p>
            <b> 3.1 </b> Bets are accepted before the start of the first event of the current tote.
        </p>
        <p>
            <b> 3.2 </b> The minimum bid is $ 1.
        </p>
    </section>
    <section>
        <h3>4. Special calculation rules</h3>
        <p>
            <b>4.1</b> <b>The prize fund</b> - part of the pool (90%), intended for the payment of winnings. The prize fund is divided into four winning categories:
        </p>
        <ol>
            <li>30% of the pool - in the distribution of that part of the prize fund involved rates, the results of which guessed 10 events;</li>
            <li>20% of the pool - in the distribution includes all bets, which results guessed 10 or 9 events;</li>
            <li>20% of the pool - in the distribution involved all bets, which results guessed 10, 9 or 8 events;</li>
            <li>20% of the pool - in the distribution involved all bets, which results guessed 10, 9, 8 or 7 events.</li>
        </ol>
        <p>
            The more events guessed, the more winning categories it is involved in. The prize pool will be split among the winning bets in proportion to the amount of each of them (ie, the higher the bet, the bigger the payout) in each of the categories in which they participate in the distribution.
        </p>
        <p>
            <b>4.2 Jackpot</b> - additional gain, which is distributed among the bets where guessed the results of 10 matches, in proportion to the value of these bets (ie, the higher the bet, the bigger the payout).
        </p>
        <p>
            <b>4.3</b> The organizer of the totalizator has the right to increase the jackpot from its own funds.
        </p>
    </section>
    <section>
        <h3> 5. The basic rules of the game on credit </h3>
        <p>
            <b> 5.1 </b> The loan amount is selected by the user at the moment of the bid.
        </p>
        <p>
            <b> 5.2 </b> The minimum loan is 1 $. The maximum is 50% of the bid amount.
        </p>
        <p>
            <b> 5.3 </b> You can repay the loan in the profile settings before the start of the first event.
        </p>
        <p>
            <b> 5.4 </b> The interest on the loan is 10% when redeemed before the start of the 1st event.
            The interest on the loan is 20% which is deducted from the bet amount automatically when
            Calculating the amount of winnings in case the loan is not repaid before the beginning of the 1st event.
        </p>
    </section>

</article>