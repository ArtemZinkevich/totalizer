package com.rtmznk.totalizer.constant;

/**
 * Class represents constant names for entities set as attribute in request object
 *
 * @author Artem Zinkevich
 */
public class SessionAttributeName {

    public final static String USER = "user";
    public final static String LANGUAGE = "language";
    public final static String PARAMETER_VALIDATION_ERRORS = "errors";
    public final static String REGISTRATION_OK = "registerOk";

    private SessionAttributeName() {
    }
}
