package com.rtmznk.totalizer.constant;

/**
 * Class represents constant names for entities set as attribute in request object
 *
 * @author Artem Zinkevich
 */
public class RequestAttributeName {
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
    public final static String THROWABLE = "throwable";
    public final static String ERROR_PAGE_MESSAGE = "errorMessage";
    public final static String VALIDATION_ERRORS = "validationErrors";
    public final static String USER_LIST = "userlist";
    public final static String TOTALIZER = "totalizer";
    public final static String CREDIT = "credit";
    public final static String REMITTANCES = "remittances";
    public final static String EVENTS = "events";


    private RequestAttributeName() {
    }
}
