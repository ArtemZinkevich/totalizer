package com.rtmznk.totalizer.constant;

/**
 * Class represents constants for searching winners and counting winners prizes
 *
 * @author Artem Zinkevich
 */
public class WinnersGroupParameter {
    public static final int FIRST_GUESSED_COUNT = 10;
    public static final int SECOND_GUESSED_COUNT = 9;
    public static final int THIRD_GUESSED_COUNT = 8;
    public static final int FOURTH_GUESSED_COUNT = 7;
    public static final double FIRST_PERCENTAGE = 0.3;
    public static final double SECOND_PERCENTAGE = 0.2;
    public static final double THIRD_PERCENTAGE = 0.2;
    public static final double FOURTH_PERCENTAGE = 0.2;

    private WinnersGroupParameter() {
    }
}
