package com.rtmznk.totalizer.constant;

/**
 * Class represents constant names for entities stored in request as parameters
 *
 * @author Artem Zinkevich
 */
public class RequestParameterName {
    public final static String PAGE = "page";
    public final static String MAIL = "mail";
    public final static String NAME = "name";
    public final static String LAST_NAME = "lastName";
    public final static String PASSWORD = "password";
    public final static String CONFIRM_PASSWORD = "password2";
    public final static String EMAIL = "email";
    public final static String LOGIN_PASSWORD = "log-password";
    public final static String OLD_PASSWORD = "oldPass";
    public final static String NEW_PASSWORD = "newPass";
    public final static String MONEY_SUM = "money";
    public final static String CARD_NUMBER = "number";
    public final static String MONTH = "month";
    public final static String YEAR = "year";
    public final static String CVV = "cvv";
    public final static String PERSON = "person";
    public final static String ID = "id";
    public final static String JSON = "json";


    private RequestParameterName() {
    }
}
