package com.rtmznk.totalizer.constant;

import javax.servlet.http.HttpServletResponse;

/**
 * This class represents object for transition of error page message
 *
 * @author Artem Zinkevich
 */
public enum ErrorPageMessage {
    PAGE_NOT_FOUND(HttpServletResponse.SC_NOT_FOUND, "notFound"),
    ACCESS_FORBIDDEN(HttpServletResponse.SC_FORBIDDEN, "forbidden"),
    UNAUTORIZED(HttpServletResponse.SC_UNAUTHORIZED, "unauthorized"),
    INTERNAL_ERROR(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "internal");
    private int errorCode;
    private String message;


    ErrorPageMessage(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }


    public int getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }


}
