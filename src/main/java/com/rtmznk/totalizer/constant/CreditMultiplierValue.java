package com.rtmznk.totalizer.constant;

/**
 * Class represents constants multiplier for credit repay
 * {@code CREDIT_MULTIPLIER_NORMAL} for credit repay before 1st event started
 * {@code  CREDIT_MULTIPLIER_EXPIRED} for credit repay after 1st event started
 *
 * @author Artem Zinkevich
 */
public class CreditMultiplierValue {
    public static final double CREDIT_MULTIPLIER_NORMAL = 1.1d;
    public static final double CREDIT_MULTIPLIER_EXPIRED = 1.2d;

    private CreditMultiplierValue() {
    }
}
