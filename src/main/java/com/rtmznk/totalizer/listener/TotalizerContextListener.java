package com.rtmznk.totalizer.listener;

import com.rtmznk.totalizer.database.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * This class initialize connection pool on start of application
 *
 * @author Artem Zinkevich
 * @see ServletContextListener
 */
@WebListener
public class TotalizerContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ConnectionPool.getInstance();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ConnectionPool.getInstance().destroyConnectionPool();
    }
}
