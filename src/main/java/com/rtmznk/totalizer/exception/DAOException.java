package com.rtmznk.totalizer.exception;

/**
 * Thrown when an exceptional situation within DAO layer has occurred. For
 * example, problems with access to database.
 *
 * @author Artem Zinkevich
 */
public class DAOException extends Exception {
    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
