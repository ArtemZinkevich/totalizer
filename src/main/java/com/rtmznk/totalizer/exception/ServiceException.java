package com.rtmznk.totalizer.exception;

/**
 * Thrown when an exceptional situation within Service layer has occurred. For
 * example, problems with parsing incoming request data.
 *
 * @author Artem Zinkevich
 */
public class ServiceException extends Exception {
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
