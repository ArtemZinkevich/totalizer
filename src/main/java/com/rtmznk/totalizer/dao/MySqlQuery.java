package com.rtmznk.totalizer.dao;

/**
 * This class represents all queries to database tables, which is user in preparedStatement
 *
 * @author Artem Zinkevich
 */
class MySqlQuery {
    private MySqlQuery() {
    }

    /**
     * Queries below used in UserDao
     */
    static final String CREATE_USER = "INSERT INTO users (password,name,last_name,mail) VALUES (?,?,?,?)";
    static final String UPDATE_USER = "UPDATE users" +
            " SET password=?, name=?, last_name=?, balance=?, banned=?, locale=?,opt_lock=?" +
            " WHERE id=? AND opt_lock=?";
    static final String FIND_USER_BY_MAIL = "SELECT id,password,name,last_name,mail,role,balance,registered,banned,locale,opt_lock" +
            " FROM users WHERE mail = ?";
    static final String FIND_USER_BY_ID = "SELECT id,password,name,last_name,mail,role,balance,registered,banned,locale,opt_lock" +
            " FROM users WHERE id = ?";
    static final String FIND_USERS = "SELECT id,password,name,last_name,mail,role,balance,registered,banned,locale,opt_lock" +
            " FROM users";
    static final String CREATE_REMITTANCE = "INSERT INTO remittances (user_id,sum,type) VALUES (?,?,?)";
    static final String FIND_REMITTANCES = "SELECT sum,type,created FROM remittances WHERE user_id=?";
    /**
     * Queries below used in AdminDao
     */
    static final String CREATE_TOTALIZER = "INSERT INTO totalizers (name,jackpot) VALUES (?,?)";
    static final String FIND_LAST_TOTALIZER = "SELECT id,name,jackpot,state,last,opt_lock,bet_sum from totalizers " +
            "WHERE id=(select max(id) from totalizers) FOR UPDATE;";
    static final String UPDATE_TOTALIZER = "UPDATE totalizers SET name=?,jackpot=?,state=?,last=?,opt_lock=? " +
            "WHERE id=? AND opt_lock=?";
    static final String CREATE_EVENT = "INSERT INTO events " +
            "(toto_id,team1,team2,type,total,sport,start_date) " +
            "VALUES(?,?,?,?,?,?,?)";
    static final String UPDATE_EVENT = "UPDATE events " +
            "SET id=?,toto_id=?,team1=?,team2=?,type=?,total=?,sport=?,start_date=?,opt_lock=?,score1=?,score2=?,finished=? " +
            "WHERE id=? AND opt_lock=?";
    static final String FIND_EVENTS_FOR_UPDATE = "SELECT id,team1,team2,score1,score2,finished,type,total,sport,start_date,opt_lock" +
            " FROM events WHERE toto_id=? FOR UPDATE";
    static final String FIND_FINISHED_EVENTS = "SELECT id,team1,team2,score1,score2,finished,type,total,sport,start_date,opt_lock" +
            " FROM events where finished=1";
    static final String FIND_COUPONS_USER_FOR_UPDATE = "SELECT c.id,c.amount,c.credit,u.id,u.balance FROM coupons c " +
            "JOIN users u ON c.us_id=u.id WHERE toto_id=? FOR UPDATE";
    static final String FIND_USER_BALANCE_FOR_UPDATE = "SELECT balance" +
            " FROM users WHERE id = ? FOR UPDATE";
    static final String UPDATE_USER_BALANCE = "UPDATE users SET balance=?,opt_lock=opt_lock+1" +
            " WHERE id = ?";
    static final String DELETE_TOTALIZER = "DELETE from totalizers where id=?";
    static final String UPDATE_BETS_STATUS = "UPDATE bets,events set bets.guessed=IF((events.type='score' " +
            "AND events.score1=bets.score1 AND events.score2=bets.score2) " +
            "OR (events.type='total' AND (events.score1+events.score2>events.total AND bets.more_total=1) " +
            "OR (events.score1+events.score2<events.total AND bets.more_total=0) " +
            "OR (events.type='result' AND (events.score1>events.score2 AND bets.result='win') " +
            "OR (events.score1<events.score2 AND bets.result='loss') " +
            "OR (events.score1=events.score2 AND bets.result='draw'))),1,0),bets.opt_lock=bets.opt_lock+1 " +
            "WHERE bets.ev_id=events.id AND events.toto_id=?;";
    static final String FIND_WON_COUPONS_FOR_COUNT = "SELECT coupons.id,coupons.amount,coupons.credit,coupons.us_id,coupons.opt_lock " +
            "FROM coupons " +
            "JOIN(SELECT coupon_id as cid, count(bets.id) AS cc FROM bets WHERE guessed=1 GROUP BY coupon_id HAVING cc=? ) " +
            "AS result ON coupons.id=result.cid FOR UPDATE;";

    static final String EVALUATE_COUPONS_FOR_TOTALIZER = "UPDATE coupons SET evaluated=true WHERE toto_id=?";
    /**
     * Queries below used in GambleDao
     */
    static final String FIND_AVAILABLE_TOTALIZER = "SELECT id,name,jackpot,opt_lock,bet_sum from totalizers " +
            "WHERE id=(select max(id) from totalizers) AND last=1 AND state='playable';";
    static final String FIND_AVAILABLE_EVENTS = "SELECT id,team1,team2,type,total,sport,start_date" +
            " FROM events WHERE toto_id=?";
    static final String CREATE_COUPON = "INSERT INTO coupons (toto_id,us_id,amount,credit) VALUES(?,?,?,?)";
    static final String CREATE_BET = "INSERT INTO bets (coupon_id,ev_id,score1,score2,result,more_total) VALUES(?,?,?,?,?,?)";
    static final String UPDATE_TOTALIZER_BET_SUM = "UPDATE totalizers SET bet_sum=?,opt_lock=? " +
            "WHERE id=? AND opt_lock=?";
    static final String FIND_COUPON = "SELECT c.id,amount,credit,created,evaluated,won,c.opt_lock,b.id,ev_id,guessed," +
            "result,score1,score2,more_total,b.opt_lock from coupons c" +
            " JOIN bets b ON c.id = coupon_id WHERE c.us_id=? AND c.toto_id=?;";
    static final String DELETE_COUPON = "DELETE from coupons where id=?";
    static final String UPDATE_COUPON = "UPDATE coupons set amount=?,credit=?,evaluated=?,won=?,opt_lock=? WHERE id=? and opt_lock=?";

}
