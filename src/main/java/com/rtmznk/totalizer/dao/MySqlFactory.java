package com.rtmznk.totalizer.dao;


import com.rtmznk.totalizer.database.ConnectionPool;

import java.sql.Connection;

/**
 * Represents a factory of all MySql DAO.
 */
public class MySqlFactory extends DAOFactory {

    MySqlFactory() {
    }

    private Connection receiveConnection() {
        return ConnectionPool.getInstance().takeConnection();
    }

    public GambleDAO getGambleDAO() {
        return new MySqlGambleDAO(receiveConnection());
    }

    public UserDAO getUserDAO() {
        return new MySqlUserDAO(receiveConnection());
    }

    public AdminDAO getAdminDAO() {
        return new MySqlAdminDAO(receiveConnection());
    }

    public GambleDAO getGambleDAO(Connection connection) {
        return new MySqlGambleDAO(connection);
    }

    public UserDAO getUserDAO(Connection connection) {
        return new MySqlUserDAO(connection);
    }

    public AdminDAO getAdminDAO(Connection connection) {
        return new MySqlAdminDAO(connection);
    }
}
