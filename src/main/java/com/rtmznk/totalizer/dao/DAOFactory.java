package com.rtmznk.totalizer.dao;

import java.sql.Connection;

/**
 * Represents abstract factory of DAO factories. Used for providing factories.
 */
public abstract class DAOFactory {
    /**
     * List of DAO types supported by the factory
     */
    public static final int MYSQL = 1;


    public abstract UserDAO getUserDAO();

    public abstract AdminDAO getAdminDAO();

    public abstract GambleDAO getGambleDAO();

    public abstract UserDAO getUserDAO(Connection connection);

    public abstract AdminDAO getAdminDAO(Connection connection);

    public abstract GambleDAO getGambleDAO(Connection connection);


    public static DAOFactory getDAOFactory(
            int whichFactory) {

        switch (whichFactory) {
            case MYSQL: {
                return new MySqlFactory();
            }
            default:
                return null;
        }
    }
}