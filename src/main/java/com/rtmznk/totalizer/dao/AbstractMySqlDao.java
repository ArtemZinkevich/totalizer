package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.exception.DAOException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class represents abstract DAO for MySql. It's a superclass for all MySql DAO.
 * It implements all methods from {@link Transactional} and {@link AutoCloseable} interfaces
 *
 * @author Artem Zinkevich
 */
abstract class AbstractMySqlDao implements Transactional, AutoCloseable {
    /**
     * This connection is used in subclasses. That is the reason of being package-private.
     */
    Connection connection;

    AbstractMySqlDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void close() throws DAOException {
        try {
            if (!connection.getAutoCommit()) {
                connection.rollback();
                connection.setAutoCommit(true);
                connection.close();
                throw new DAOException("Connection closing during opened transaction");
            } else {
                connection.close();
            }
        } catch (SQLException e) {
            throw new DAOException("Can not return connection to ConnectionPool");
        }
    }

    @Override
    public void startTransaction() throws DAOException {
        try {
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DAOException("Can not set autocommit to false", e);
        }
    }

    @Override
    public void setTransactionIsolation(int level) throws DAOException {
        try {
            connection.setTransactionIsolation(level);
        } catch (SQLException e) {
            throw new DAOException("Can not set isolation level", e);
        }
    }

    @Override
    public void commit() throws DAOException {
        try {
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DAOException("Can not commit", e);
        }
    }

    @Override
    public void rollback() throws DAOException {
        try {
            connection.rollback();
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DAOException("Can not rollback", e);
        }
    }

    @Override
    public Connection shareConnection() {
        return connection;
    }


}
