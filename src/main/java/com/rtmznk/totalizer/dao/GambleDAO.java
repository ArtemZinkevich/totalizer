package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.*;
import com.rtmznk.totalizer.exception.DAOException;

import java.util.List;

/**
 * Represents GambleDAO interface, implementation of which provides appropriate
 * realization of all necessary methods.
 * Mostly used for all game play related actions.
 *
 * @author Artem Zinkevich
 */
public interface GambleDAO extends AutoCloseable, Transactional {
    Totalizer findAvailableTotalizer() throws DAOException;

    List<Event> findAvailableEvents(Totalizer totalizer) throws DAOException;

    Long createCoupon(Coupon coupon, Long userId, Long totalizerId) throws DAOException;

    boolean createBets(List<Bet> bets, Long couponId) throws DAOException;

    boolean updateTotalizerBetSum(Totalizer forUpdate) throws DAOException;

    Coupon findCoupon(User user, Totalizer totalizer) throws DAOException;

    boolean deleteCoupon(Coupon coupon) throws DAOException;

    boolean updateCoupons(List<Coupon> wonCoupons) throws DAOException;

    @Override
    void close() throws DAOException;

}
