package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.Coupon;
import com.rtmznk.totalizer.entity.Event;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * Represents AdminDAO interface, implementation of which provides appropriate
 * realization of all necessary methods.
 * Used mostly  for DAO operations with Totalizers table, Events table.
 *
 * @author Artem Zinkevich
 */
public interface AdminDAO extends AutoCloseable, Transactional {
    /**
     * @return the {@code Totalizer} object
     * @throws DAOException
     */
    Totalizer findLastTotalizer() throws DAOException;

    /**
     * @param totalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */

    boolean createTotalizer(Totalizer totalizer) throws DAOException;

    /**
     * @param totalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */
    boolean updateTotalizer(Totalizer totalizer) throws DAOException;

    /**
     * @param totalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */
    boolean createEvents(Totalizer totalizer) throws DAOException;

    /**
     * @param totalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */

    boolean updateEvents(Totalizer totalizer) throws DAOException;

    /**
     * @param totalizer the reference totalizer object with params
     * @return {@code List} with {@link Event} values
     * @throws DAOException
     */
    List<Event> findEvents(Totalizer totalizer) throws DAOException;

    /**
     * @param lastTotalizer the reference totalizer object with params
     * @return {@code Map} with {@link User} as keys and {@link Coupon} as value
     * @throws DAOException
     */

    Map<User, Coupon> findCouponsWithUsers(Totalizer lastTotalizer) throws DAOException;

    /**
     * @param lastTotalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */

    boolean deleteTotalizer(Totalizer lastTotalizer) throws DAOException;

    /**
     * @param actualTotalizer the reference totalizer object with params
     * @throws DAOException
     */

    void updateBetsStatus(Totalizer actualTotalizer) throws DAOException;

    /**
     * @param firstGuessedCount {@code int} number of guessed bets.
     * @return {@code List} with {@link Coupon} values
     * @throws DAOException
     */

    List<Coupon> findWonCouponsForCount(int firstGuessedCount) throws DAOException;

    /**
     * @param key {@code long} id of user.
     * @return {@code BigDecimal} object with amount of user money
     * @throws DAOException
     */

    BigDecimal findUserMoneyForUpdate(Long key) throws DAOException;

    /**
     * @param key {@code long} id of user.
     * @param add {@code BigDecimal} object with amount of user money
     * @return {@code boolean} primitive value result
     * @throws DAOException
     */

    boolean updateUserBalance(Long key, BigDecimal add) throws DAOException;

    /**
     * @param lastTotalizer the reference totalizer object with params
     * @return {@code boolean} primitive value
     * @throws DAOException
     */

    boolean updateCouponStatus(Totalizer lastTotalizer) throws DAOException;

    /**
     * @return {@code List} with {@link Event} values
     * @throws DAOException
     */

    List<Event> findFinishedEvents() throws DAOException;

    @Override
    void close() throws DAOException;

}
