package com.rtmznk.totalizer.dao;

/**
 * Contains column names from database tables.
 *
 * @author Artem Zinkevich
 */
class ColumnName {
    private ColumnName() {
    }

    static final String ID = "id";
    static final String NAME = "name";
    static final String LAST_NAME = "last_name";
    static final String LOCALE = "locale";
    static final String PASSWORD = "password";
    static final String MAIL = "mail";
    static final String ROLE = "role";
    static final String BANNED = "banned";
    static final String REGISTERED = "registered";
    static final String BALANCE = "balance";
    static final String VERSION = "opt_lock";
    static final String JACKPOT = "jackpot";
    static final String BET_SUM = "bet_sum";
    static final String STATE = "state";
    static final String LAST = "last";
    static final String TEAM_1 = "team1";
    static final String TEAM_2 = "team2";
    static final String FINISHED = "finished";
    static final String SCORE_1 = "score1";
    static final String SCORE_2 = "score2";
    static final String TYPE = "type";
    static final String TOTAL = "total";
    static final String SPORT = "sport";
    static final String START_DATE = "start_date";
    static final String SUM = "sum";
    static final String CREATED = "created";


}
