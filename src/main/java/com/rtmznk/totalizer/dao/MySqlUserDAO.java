package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.Remittance;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;
import com.rtmznk.totalizer.utility.CalendarUTC;
import com.rtmznk.totalizer.utility.LocaleUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.rtmznk.totalizer.dao.MySqlQuery.*;

/**
 * Class represents implementation of all {@link UserDAO}
 * and is subclass of {@link AbstractMySqlDao}
 *
 * @author Artem Zinkevich
 * @see UserDAO
 * @see AbstractMySqlDao
 */
public class MySqlUserDAO extends AbstractMySqlDao implements UserDAO {

    MySqlUserDAO(Connection connection) {
        super(connection);
    }

    @Override
    public boolean create(User user) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(CREATE_USER)) {
            statement.setString(1, user.getPassword());
            statement.setString(2, user.getName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getEmail());
            if (statement.executeUpdate() > 0) {
                return true;
            } else {
                throw new DAOException("User wasn't created. Invalid query parameters");
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
    }

    @Override
    public boolean update(User user) throws DAOException {
        boolean queryResult;
        long userVersion = user.getVersion();
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER)) {
            statement.setString(1, user.getPassword());
            statement.setString(2, user.getName());
            statement.setString(3, user.getLastName());
            statement.setBigDecimal(4, user.getMoney());
            statement.setBoolean(5, user.getBanned());
            statement.setString(6, user.getUserLocale().getLanguage());
            statement.setLong(7, ++userVersion);
            statement.setLong(8, user.getId());
            statement.setLong(9, user.getVersion());
            queryResult = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return queryResult;
    }

    @Override
    public boolean createRemittance(Remittance remittance, Long userId) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_REMITTANCE)) {
            statement.setLong(1, userId);
            statement.setBigDecimal(2, remittance.getSum());
            statement.setString(3, remittance.getType().getName());
            if (statement.executeUpdate() > 0) {
                result = true;
            } else {
                result = false;
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public List<Remittance> findUserRemittances(Long userId) throws DAOException {
        List<Remittance> remittances = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_REMITTANCES)) {
            statement.setLong(1, userId);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Remittance remittance = new Remittance();
                remittance.setType(Remittance.Type.valueOf(resultSet.getString(ColumnName.TYPE).toUpperCase()));
                remittance.setSum(resultSet.getBigDecimal(ColumnName.SUM));
                remittance.setDateTime(resultSet.getTimestamp(ColumnName.CREATED, CalendarUTC.getInstance().getCalendarUTC())
                        .toLocalDateTime());
                remittances.add(remittance);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return remittances;
    }

    @Override
    public User findUserByMail(String mail) throws DAOException {
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_MAIL)) {
            statement.setString(1, mail);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                user = buildUser(rs);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return user;
    }

    @Override
    public User findUserById(long id) throws DAOException {
        User user = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_ID)) {
            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                user = buildUser(rs);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return user;
    }

    @Override
    public List<User> findUsers() throws DAOException {
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_USERS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = buildUser(resultSet);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return users;
    }

    /**
     * Method - helper for building {@link User} object from result set row
     *
     * @param rs {@code ResultSet}
     * @return {@code User} object
     * @throws SQLException
     */
    private User buildUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getLong(ColumnName.ID));
        user.setPassword(rs.getString(ColumnName.PASSWORD));
        user.setName(rs.getString(ColumnName.NAME));
        user.setLastName(rs.getString(ColumnName.LAST_NAME));
        user.setRole(rs.getString(ColumnName.ROLE));
        user.setEmail(rs.getString(ColumnName.MAIL));
        user.setMoney(rs.getBigDecimal(ColumnName.BALANCE));
        user.setRegistrationDate(rs.getTimestamp(ColumnName.REGISTERED,
                CalendarUTC.getInstance().getCalendarUTC()).toLocalDateTime());
        user.setBanned(rs.getInt(ColumnName.BANNED) == 1);
        user.setVersion(rs.getLong(ColumnName.VERSION));
        String locale = rs.getString(ColumnName.LOCALE);
        LocaleUtility localeUtility = LocaleUtility.getInstance();
        if (locale != null && !locale.isEmpty()) {
            user.setUserLocale(localeUtility.getLocaleByString(rs.getString(ColumnName.LOCALE)));
        } else {
            user.setUserLocale(localeUtility.getDefaultLocale());
        }
        return user;
    }
}
