package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.*;
import com.rtmznk.totalizer.exception.DAOException;
import com.rtmznk.totalizer.utility.CalendarUTC;
import jdk.internal.org.objectweb.asm.Type;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.rtmznk.totalizer.dao.MySqlQuery.*;

/**
 * Class represents implementation of all {@link GambleDAO}
 * and is subclass of {@link AbstractMySqlDao}
 *
 * @author Artem Zinkevich
 * @see GambleDAO
 * @see AbstractMySqlDao
 */
public class MySqlGambleDAO extends AbstractMySqlDao implements GambleDAO {

    MySqlGambleDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Totalizer findAvailableTotalizer() throws DAOException {
        Totalizer totalizer = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_AVAILABLE_TOTALIZER)) {
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                totalizer = new Totalizer();
                totalizer.setId(resultSet.getLong(ColumnName.ID));
                totalizer.setName(resultSet.getString(ColumnName.NAME));
                totalizer.setJackpot(resultSet.getBigDecimal(ColumnName.JACKPOT));
                totalizer.setBetSum(resultSet.getBigDecimal(ColumnName.BET_SUM));
                totalizer.setVersion(resultSet.getLong(ColumnName.VERSION));
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return totalizer;
    }

    @Override
    public List<Event> findAvailableEvents(Totalizer totalizer) throws DAOException {
        List<Event> events = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_AVAILABLE_EVENTS)) {
            statement.setLong(1, totalizer.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Event event = new Event();
                event.setId(resultSet.getLong(ColumnName.ID));
                event.setSport(resultSet.getString(ColumnName.SPORT));
                event.setTeam1(resultSet.getString(ColumnName.TEAM_1));
                event.setTeam2(resultSet.getString(ColumnName.TEAM_2));
                BigDecimal total = resultSet.getBigDecimal(ColumnName.TOTAL);
                if (total != null) {
                    event.setTotal(total.doubleValue());
                }
                event.setType(Event.Type.valueOf(resultSet.getString(ColumnName.TYPE).toUpperCase()));
                event.setDate(resultSet.getTimestamp(ColumnName.START_DATE,
                        CalendarUTC.getInstance().getCalendarUTC()).toLocalDateTime());
                events.add(event);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return events;
    }

    @Override
    public Long createCoupon(Coupon coupon, Long userId, Long totalizerId) throws DAOException {
        Long result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_COUPON,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, totalizerId);
            statement.setLong(2, userId);
            statement.setBigDecimal(3, coupon.getAmount());
            statement.setBigDecimal(4, coupon.getCredit());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating coupon failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    result = generatedKeys.getLong(1);
                } else {
                    throw new DAOException("Creating coupon failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public boolean createBets(List<Bet> bets, Long couponId) throws DAOException {
        int updateCounts[];
        try (PreparedStatement statement = connection.prepareStatement(CREATE_BET)) {
            for (Bet bet : bets) {
                statement.setLong(1, couponId);
                statement.setLong(2, bet.getEventId());
                if (bet.getScore1() != null) {
                    statement.setInt(3, bet.getScore1());
                } else {
                    statement.setNull(3, Types.INTEGER);
                }
                if (bet.getScore2() != null) {
                    statement.setInt(4, bet.getScore2());
                } else {
                    statement.setNull(4, Types.INTEGER);
                }
                if (bet.getResult() == null) {
                    statement.setNull(5, Types.VARCHAR);
                } else {
                    statement.setString(5, bet.getResult().name().toLowerCase());
                }
                if (bet.getMoreThanTotal() == null) {
                    statement.setNull(6, Types.BOOLEAN);
                } else {
                    statement.setBoolean(6, bet.getMoreThanTotal());
                }
                statement.addBatch();
            }
            updateCounts = statement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return updateCounts.length == bets.size();
    }

    @Override
    public boolean updateTotalizerBetSum(Totalizer forUpdate) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_TOTALIZER_BET_SUM)) {
            long version = forUpdate.getVersion();
            statement.setBigDecimal(1, forUpdate.getBetSum());
            statement.setLong(2, ++version);
            statement.setLong(3, forUpdate.getId());
            statement.setLong(4, forUpdate.getVersion());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public Coupon findCoupon(User user, Totalizer totalizer) throws DAOException {
        Coupon coupon = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_COUPON)) {
            statement.setLong(1, user.getId());
            statement.setLong(2, totalizer.getId());
            ResultSet resultSet = statement.executeQuery();
            List<Bet> bets = new ArrayList<>();
            while (resultSet.next()) {
                if (coupon == null) {
                    coupon = buildCoupon(resultSet);
                    coupon.setUserId(user.getId());
                    coupon.setTotalizerId(totalizer.getId());
                }
                bets.add(buildBet(resultSet));
            }
            if (coupon != null) {
                coupon.setBets(bets);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return coupon;
    }

    @Override
    public boolean deleteCoupon(Coupon coupon) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_COUPON)) {
            statement.setLong(1, coupon.getId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public boolean updateCoupons(List<Coupon> wonCoupons) throws DAOException {
        int updateCounts[];
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_COUPON)) {
            for (Coupon coupon : wonCoupons) {
                System.out.println(coupon.getAmount());
                Long version = coupon.getVersion();
                statement.setBigDecimal(1, coupon.getAmount());
                statement.setBigDecimal(2, coupon.getCredit());
                if (coupon.getEvaluated() != null) {
                    statement.setBoolean(3, coupon.getEvaluated());
                } else {
                    statement.setNull(3, Type.BOOLEAN);
                }
                if (coupon.getWon() != null) {
                    statement.setBoolean(4, coupon.getWon());
                } else {
                    statement.setNull(4, Type.BOOLEAN);
                }
                statement.setLong(5, ++version);
                statement.setLong(6, coupon.getId());
                statement.setLong(7, coupon.getVersion());
                statement.addBatch();
            }
            updateCounts = statement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return updateCounts.length == wonCoupons.size();
    }

    /**
     * Method for building coupon from result set row
     *
     * @param rs {@code ResultSet}
     * @return {@code Coupon} object
     * @throws SQLException
     */
    private Coupon buildCoupon(ResultSet rs) throws SQLException {
        Coupon coupon = new Coupon();
        coupon.setId(rs.getLong(1));
        coupon.setAmount(rs.getBigDecimal(2));
        coupon.setCredit(rs.getBigDecimal(3));
        coupon.setCreated(rs.getTimestamp(4, CalendarUTC.getInstance().getCalendarUTC()).toLocalDateTime());
        coupon.setEvaluated(rs.getBoolean(5));
        Boolean won = rs.getBoolean(6);
        if (rs.wasNull()) {
            won = null;
        }
        coupon.setWon(won);
        coupon.setVersion(rs.getLong(7));
        return coupon;
    }

    /**
     * Method for building bet from result set row
     *
     * @param rs {@code ResultSet}
     * @return {@code Bet} object
     * @throws SQLException
     */
    private Bet buildBet(ResultSet rs) throws SQLException {
        Bet bet = new Bet();
        bet.setId(rs.getLong(8));
        bet.setEventId(rs.getLong(9));
        Boolean guessed = rs.getBoolean(10);
        if (rs.wasNull()) {
            guessed = null;
        }
        bet.setGuessed(guessed);
        String result = rs.getString(11);
        if (result != null) {
            bet.setResult(Bet.Result.valueOf(result.toUpperCase()));
        }
        Integer score1 = rs.getInt(12);
        if (rs.wasNull()) {
            score1 = null;
        }
        bet.setScore1(score1);
        Integer score2 = rs.getInt(13);
        if (rs.wasNull()) {
            score2 = null;
        }
        bet.setScore2(score2);
        Boolean moreThanTotal = rs.getBoolean(14);
        if (rs.wasNull()) {
            moreThanTotal = null;
        }
        bet.setMoreThanTotal(moreThanTotal);
        bet.setVersion(rs.getLong(15));
        return bet;
    }

}
