package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.Coupon;
import com.rtmznk.totalizer.entity.Event;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

import static com.rtmznk.totalizer.dao.MySqlQuery.*;

/**
 * Class represents implementation of all {@link AdminDAO}
 * and is subclass of {@link AbstractMySqlDao}
 *
 * @author Artem Zinkevich
 * @see AdminDAO
 * @see AbstractMySqlDao
 */
public class MySqlAdminDAO extends AbstractMySqlDao implements AdminDAO {

    MySqlAdminDAO(Connection connection) {
        super(connection);
    }

    @Override
    public Totalizer findLastTotalizer() throws DAOException {
        Totalizer totalizer = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_LAST_TOTALIZER)) {
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                totalizer = buildTotalizer(rs);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return totalizer;
    }


    @Override
    public boolean createTotalizer(Totalizer totalizer) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_TOTALIZER,
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, totalizer.getName());
            statement.setBigDecimal(2, totalizer.getJackpot());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new DAOException("Creating totalizer failed, no rows affected.");
            }
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    totalizer.setId(generatedKeys.getLong(1));
                    result = true;
                } else {
                    throw new DAOException("Creating totalizer failed, no ID obtained.");
                }
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public boolean updateTotalizer(Totalizer totalizer) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_TOTALIZER)) {
            long version = totalizer.getVersion();
            statement.setString(1, totalizer.getName());
            statement.setBigDecimal(2, totalizer.getJackpot());
            statement.setString(3, totalizer.getState().name().toLowerCase());
            statement.setBoolean(4, totalizer.isLast());
            statement.setLong(5, ++version);
            statement.setLong(6, totalizer.getId());
            statement.setLong(7, totalizer.getVersion());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
    }

    @Override
    public boolean createEvents(Totalizer totalizer) throws DAOException {
        int updateCounts[];
        try (PreparedStatement statement = connection.prepareStatement(CREATE_EVENT)) {
            for (Event event : totalizer.getEvents()) {
                statement.setInt(1, totalizer.getId().intValue());
                statement.setString(2, event.getTeam1());
                statement.setString(3, event.getTeam2());
                statement.setString(4, event.getType().name().toLowerCase());
                if (event.getTotal() != null) {
                    statement.setDouble(5, event.getTotal());
                } else {
                    statement.setNull(5, Types.DECIMAL);
                }
                statement.setString(6, event.getSport());
                statement.setTimestamp(7, Timestamp.valueOf(event.getDate()));
                statement.addBatch();
            }
            updateCounts = statement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return updateCounts.length == totalizer.getEvents().size();
    }

    @Override
    public boolean updateEvents(Totalizer totalizer) throws DAOException {
        int updateCounts[];
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_EVENT)) {
            for (Event event : totalizer.getEvents()) {
                long eventVersion = event.getVersion();
                statement.setInt(1, event.getId().intValue());
                statement.setInt(2, totalizer.getId().intValue());
                statement.setString(3, event.getTeam1());
                statement.setString(4, event.getTeam2());
                statement.setString(5, event.getType().name().toLowerCase());
                if (event.getTotal() != null) {
                    statement.setDouble(6, event.getTotal());
                } else {
                    statement.setNull(6, Types.DECIMAL);
                }
                statement.setString(7, event.getSport());
                statement.setTimestamp(8, Timestamp.valueOf(event.getDate()));
                statement.setLong(9, ++eventVersion);
                if (event.getScore1() != null) {
                    statement.setInt(10, event.getScore1());
                } else {
                    statement.setNull(10, Types.INTEGER);
                }

                if (event.getScore2() != null) {
                    statement.setInt(11, event.getScore2());
                } else {
                    statement.setNull(11, Types.INTEGER);
                }
                statement.setBoolean(12, event.isFinished());
                statement.setInt(13, event.getId().intValue());
                statement.setLong(14, event.getVersion());
                statement.addBatch();
            }
            updateCounts = statement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        System.out.println(Arrays.asList(updateCounts));
        return updateCounts.length == totalizer.getEvents().size();
    }

    @Override
    public List<Event> findEvents(Totalizer totalizer) throws DAOException {
        List<Event> events = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_EVENTS_FOR_UPDATE)) {
            statement.setInt(1, totalizer.getId().intValue());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Event event = buildEvent(resultSet);
                events.add(event);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return events;
    }

    @Override
    public Map<User, Coupon> findCouponsWithUsers(Totalizer lastTotalizer) throws DAOException {
        Map<User, Coupon> resultMap = new HashMap<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_COUPONS_USER_FOR_UPDATE)) {
            statement.setLong(1, lastTotalizer.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                Coupon coupon = new Coupon();
                coupon.setId(resultSet.getLong(1));
                coupon.setAmount(resultSet.getBigDecimal(2));
                coupon.setCredit(resultSet.getBigDecimal(3));
                user.setId(resultSet.getLong(4));
                user.setMoney(resultSet.getBigDecimal(5));
                resultMap.put(user, coupon);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return resultMap;
    }

    @Override
    public boolean deleteTotalizer(Totalizer lastTotalizer) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_TOTALIZER)) {
            statement.setLong(1, lastTotalizer.getId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public void updateBetsStatus(Totalizer actualTotalizer) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_BETS_STATUS)) {
            statement.setLong(1, actualTotalizer.getId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
    }

    @Override
    public List<Coupon> findWonCouponsForCount(int firstGuessedCount) throws DAOException {
        List<Coupon> coupons = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_WON_COUPONS_FOR_COUNT)) {
            statement.setInt(1, firstGuessedCount);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Coupon coupon = new Coupon();
                coupon.setId(resultSet.getLong(1));
                coupon.setAmount(resultSet.getBigDecimal(2));
                coupon.setCredit(resultSet.getBigDecimal(3));
                coupon.setUserId(resultSet.getLong(4));
                coupon.setVersion(resultSet.getLong(5));
                coupons.add(coupon);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return coupons;
    }

    @Override
    public BigDecimal findUserMoneyForUpdate(Long key) throws DAOException {
        BigDecimal money = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_USER_BALANCE_FOR_UPDATE)) {
            statement.setLong(1, key);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                money = resultSet.getBigDecimal(1);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return money;
    }

    @Override
    public boolean updateUserBalance(Long key, BigDecimal value) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_USER_BALANCE)) {
            statement.setBigDecimal(1, value);
            statement.setLong(2, key);
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public boolean updateCouponStatus(Totalizer lastTotalizer) throws DAOException {
        boolean result;
        try (PreparedStatement statement = connection.prepareStatement(EVALUATE_COUPONS_FOR_TOTALIZER)) {
            statement.setLong(1, lastTotalizer.getId());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return result;
    }

    @Override
    public List<Event> findFinishedEvents() throws DAOException {
        List<Event> events = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_FINISHED_EVENTS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Event event = buildEvent(resultSet);
                events.add(event);
            }
        } catch (SQLException e) {
            throw new DAOException("Database access error", e);
        }
        return events;
    }

    private Totalizer buildTotalizer(ResultSet rs) throws SQLException {
        Totalizer totalizer = new Totalizer();
        totalizer.setId(rs.getLong(ColumnName.ID));
        totalizer.setName(rs.getString(ColumnName.NAME));
        totalizer.setBetSum(rs.getBigDecimal(ColumnName.BET_SUM));
        totalizer.setJackpot(rs.getBigDecimal(ColumnName.JACKPOT));
        totalizer.setState(Totalizer.State.valueOf(rs.getString(ColumnName.STATE).toUpperCase()));
        totalizer.setVersion(rs.getLong(ColumnName.VERSION));
        totalizer.setLast(rs.getBoolean(ColumnName.LAST));
        return totalizer;
    }

    private Event buildEvent(ResultSet rs) throws SQLException {
        Event event = new Event();
        event.setId(rs.getLong(ColumnName.ID));
        event.setTeam1(rs.getString(ColumnName.TEAM_1));
        event.setTeam2(rs.getString(ColumnName.TEAM_2));
        Integer score1 = rs.getInt(ColumnName.SCORE_1);
        if (rs.wasNull()) {
            score1 = null;
        }
        event.setScore1(score1);
        Integer score2 = rs.getInt(ColumnName.SCORE_2);
        if (rs.wasNull()) {
            score2 = null;
        }
        event.setScore2(score2);
        event.setFinished(rs.getBoolean(ColumnName.FINISHED));
        event.setType(Event.Type.valueOf(rs.getString(ColumnName.TYPE).toUpperCase()));
        BigDecimal total = rs.getBigDecimal(ColumnName.TOTAL);
        if (total != null) {
            event.setTotal(total.doubleValue());
        }
        event.setSport(rs.getString(ColumnName.SPORT));
        event.setDate(rs.getTimestamp(ColumnName.START_DATE).toLocalDateTime());
        event.setVersion(rs.getLong(ColumnName.VERSION));
        return event;
    }
}
