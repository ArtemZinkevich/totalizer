package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.exception.DAOException;

import java.sql.Connection;

/**
 * Interface with methods necessary for working with transcations
 */
public interface Transactional {

    default void startTransaction() throws DAOException {
        throw new DAOException("This operation is not supported");
    }

    default void setTransactionIsolation(int level) throws DAOException {
        throw new DAOException("This operation is not supported");
    }

    default void commit() throws DAOException {
        throw new DAOException("This operation is not supported");
    }

    default void rollback() throws DAOException {
        throw new DAOException("This operation is not supported");
    }

    default Connection shareConnection() throws DAOException {
        throw new DAOException("This operation is not supported");
    }

}
