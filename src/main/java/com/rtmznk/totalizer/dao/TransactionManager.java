package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.exception.DAOException;

/**
 * Class used for join one dao to another one existed.
 *
 * @author Artem Zinkevich
 */
public class TransactionManager {
    private static TransactionManager instance = new TransactionManager();

    private TransactionManager() {
    }

    public static TransactionManager getInstance() {
        return instance;
    }

    public UserDAO getUserDaoWithinSameTransaction(Transactional transactional, DAOFactory daoFactory) throws DAOException {
        return daoFactory.getUserDAO(transactional.shareConnection());
    }

    public AdminDAO getAdminDaoWithinSameTransaction(Transactional transactional, DAOFactory daoFactory) throws DAOException {
        return daoFactory.getAdminDAO((transactional.shareConnection()));
    }

    public GambleDAO getGambleDaoWithinSameTransaction(Transactional transactional, DAOFactory daoFactory) throws DAOException {
        return daoFactory.getGambleDAO((transactional.shareConnection()));
    }
}
