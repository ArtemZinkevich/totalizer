package com.rtmznk.totalizer.dao;

import com.rtmznk.totalizer.entity.Remittance;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;

import java.util.List;

/**
 * Represents UserDAO interface, implementation of which provides appropriate
 * realization of all necessary methods.
 * Used mostly  for DAO operations with Users table, Remittances table.
 *
 * @author Artem Zinkevich
 */
public interface UserDAO extends AutoCloseable, Transactional {

    boolean create(User user) throws DAOException;

    boolean update(User user) throws DAOException;

    boolean createRemittance(Remittance remittance, Long userId) throws DAOException;

    List<Remittance> findUserRemittances(Long userId) throws DAOException;

    User findUserById(long id) throws DAOException;

    User findUserByMail(String mail) throws DAOException;

    List<User> findUsers() throws DAOException;

    @Override
    void close() throws DAOException;
}
