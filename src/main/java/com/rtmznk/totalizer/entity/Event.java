package com.rtmznk.totalizer.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

/**
 * Represents Events.
 *
 * @author Artem Zinkevich
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Event extends Entity {

    private static final long serialVersionUID = 1349651125597498769L;

    /**
     * Represents possible event types.
     */
    public enum Type {
        @JsonProperty("total")
        TOTAL,
        @JsonProperty("score")
        SCORE,
        @JsonProperty("result")
        RESULT;

        public static boolean isValidType(String string) {
            boolean result;
            try {
                Type.valueOf(string.toUpperCase());
                result = true;
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        public String getName() {
            return this.name().toLowerCase();
        }
    }

    private Long totolizerId;
    private String team1;
    private String team2;
    private String sport;
    private Integer score1;
    private Integer score2;
    private Boolean finished;
    private Type type;
    private Double total;
    private LocalDateTime date;

    public Event() {
    }

    public Long getTotolizerId() {
        return totolizerId;
    }

    public void setTotolizerId(Long totolizerId) {
        this.totolizerId = totolizerId;
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

}
