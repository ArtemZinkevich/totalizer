package com.rtmznk.totalizer.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

/**
 * Represents Totalizers.
 *
 * @author Artem Zinkevich
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Totalizer extends Entity {
    private static final long serialVersionUID = 6080146552223791219L;

    /**
     * Represents possible state of totalizer
     */
    public enum State {
        FINISHED, EDITABLE, PLAYABLE
    }

    private String name;
    private State state;
    private Boolean last;
    private BigDecimal jackpot;
    private BigDecimal betSum;
    private List<Event> events;

    public Totalizer() {
        events = Collections.emptyList();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public BigDecimal getJackpot() {
        return jackpot;
    }

    public void setJackpot(BigDecimal jackpot) {
        this.jackpot = jackpot;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public BigDecimal getBetSum() {
        return betSum;
    }

    public void setBetSum(BigDecimal betSum) {
        this.betSum = betSum;
    }

}
