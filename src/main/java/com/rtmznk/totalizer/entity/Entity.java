package com.rtmznk.totalizer.entity;

import java.io.Serializable;

/**
 * Represents abstract superclass for all entities.
 * Contains fields necessary for all classes
 *
 * @author Artem Zinkevich
 */
public abstract class Entity implements Serializable {

    private static final long serialVersionUID = -7299544214547483809L;
    private Long id;
    private Long version;

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
