package com.rtmznk.totalizer.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Represents Remittances.
 *
 * @author Artem Zinkevich
 */
public class Remittance {
    /**
     * Represents types of remittances
     */
    public enum Type {
        DEPOSIT(true, "deposit"), DEBT(false, "debt"), BET(false, "bet"), PRIZE(true, "prize"), REFUND(true, "refund");

        /**
         * Constructs Remittance type
         *
         * @param incoming {@code boolean} parameter defines is Remittance of this type income or outcome
         * @param name     {@code String} parameter to simplify work and used for localization on JSP.
         */
        Type(boolean incoming, String name) {
            this.incoming = incoming;
            this.name = name;
        }

        private String name;
        private boolean incoming;

        public String getName() {
            return name;
        }

        public boolean isIncoming() {
            return incoming;
        }
    }

    private BigDecimal sum;
    private LocalDateTime dateTime;
    private Type type;

    public Remittance() {
    }

    public Remittance(BigDecimal sum, Type remittanceType) {
        this.sum = sum;
        this.type = remittanceType;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
