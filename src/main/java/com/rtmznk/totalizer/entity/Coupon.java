package com.rtmznk.totalizer.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Represents Coupons.
 *
 * @author Artem Zinkevich
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Coupon extends Entity {
    private static final long serialVersionUID = 7467251678083299285L;
    private Long userId;
    private Long totalizerId;
    private BigDecimal amount;
    private BigDecimal credit;
    private LocalDateTime created;
    private Boolean evaluated;
    private Boolean won;
    private List<Bet> bets;

    public Coupon() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTotalizerId() {
        return totalizerId;
    }

    public void setTotalizerId(Long totalizerId) {
        this.totalizerId = totalizerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public Boolean getWon() {
        return won;
    }

    public void setWon(Boolean won) {
        this.won = won;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coupon)) return false;

        Coupon coupon = (Coupon) o;
        if (getId() != null ? !getId().equals(coupon.getId()) : coupon.getId() != null)
            return false;
        if (getVersion() != null ? !getVersion().equals(coupon.getVersion()) : coupon.getVersion() != null)
            return false;
        if (getUserId() != null ? !getUserId().equals(coupon.getUserId()) : coupon.getUserId() != null) return false;
        if (getTotalizerId() != null ? !getTotalizerId().equals(coupon.getTotalizerId()) : coupon.getTotalizerId() != null)
            return false;
        if (getAmount() != null ? !getAmount().equals(coupon.getAmount()) : coupon.getAmount() != null) return false;
        if (getCredit() != null ? !getCredit().equals(coupon.getCredit()) : coupon.getCredit() != null) return false;
        if (getCreated() != null ? !getCreated().equals(coupon.getCreated()) : coupon.getCreated() != null)
            return false;
        if (getEvaluated() != null ? !getEvaluated().equals(coupon.getEvaluated()) : coupon.getEvaluated() != null)
            return false;
        if (getWon() != null ? !getWon().equals(coupon.getWon()) : coupon.getWon() != null) return false;
        return getBets() != null ? getBets().equals(coupon.getBets()) : coupon.getBets() == null;
    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        result = 31 * result + (getVersion() != null ? getVersion().hashCode() : 0);
        result = 31 * result + (getTotalizerId() != null ? getTotalizerId().hashCode() : 0);
        result = 31 * result + (getAmount() != null ? getAmount().hashCode() : 0);
        result = 31 * result + (getCredit() != null ? getCredit().hashCode() : 0);
        result = 31 * result + (getCreated() != null ? getCreated().hashCode() : 0);
        result = 31 * result + (getEvaluated() != null ? getEvaluated().hashCode() : 0);
        result = 31 * result + (getWon() != null ? getWon().hashCode() : 0);
        result = 31 * result + (getBets() != null ? getBets().hashCode() : 0);
        return result;
    }

}
