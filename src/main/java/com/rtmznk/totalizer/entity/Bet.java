package com.rtmznk.totalizer.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents Bets.
 *
 * @author Artem Zinkevich
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bet extends Entity {
    private static final long serialVersionUID = 4735427188245358706L;

    /**
     * Enum with possible results
     */
    public enum Result {
        @JsonProperty("win")
        WIN,
        @JsonProperty("draw")
        DRAW,
        @JsonProperty("loss")
        LOSS;
    }

    private Long couponId;
    private Long eventId;
    private Integer score1;
    private Integer score2;
    private Result result;
    @JsonProperty("total")
    private Boolean moreThanTotal;
    private Boolean guessed;
    private Event.Type eventType;

    public Bet() {
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Boolean getMoreThanTotal() {
        return moreThanTotal;
    }

    public void setMoreThanTotal(Boolean moreThanTotal) {
        this.moreThanTotal = moreThanTotal;
    }

    public Boolean getGuessed() {
        return guessed;
    }

    public void setGuessed(Boolean guessed) {
        this.guessed = guessed;
    }

    public Event.Type getEventType() {
        return eventType;
    }

    public void setEventType(Event.Type eventType) {
        this.eventType = eventType;
    }

}
