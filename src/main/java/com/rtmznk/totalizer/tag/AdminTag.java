package com.rtmznk.totalizer.tag;

import com.rtmznk.totalizer.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Represents a tag with body which shows body content only if user is not null
 * and user role is "admin"
 *
 * @author Artem Zinkevich
 */
public class AdminTag extends TagSupport {
    private static final String USER_ATTR = "user";
    private static final String ROLE_ADMIN = "admin";


    @Override
    public int doStartTag() throws JspException {
        User user = (User) pageContext.getSession().getAttribute(USER_ATTR);
        if (user != null) {
            if (user.getRole().equals(ROLE_ADMIN)) {
                return EVAL_BODY_INCLUDE;
            } else {
                return SKIP_BODY;
            }
        } else {
            return SKIP_BODY;
        }
    }
}