package com.rtmznk.totalizer.tag;

/**
 * Represents a function for removing all spaces from formatted {@code BigDecimal}
 * for purpose of being shown in text field which forbid spaces in numbers
 *
 * @author Artem Zinkevich
 */
public class SpaceReplacerFunction {
    public static String replaceSpace(String string) {
        String res;
        if (string == null) {
            res = "";
        } else {
            res = string.replaceAll("\\D", "");
        }
        return res;
    }
}
