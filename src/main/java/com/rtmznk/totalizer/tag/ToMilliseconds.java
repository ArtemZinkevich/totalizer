package com.rtmznk.totalizer.tag;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Represents a function for transforming {@link LocalDateTime} object into amount
 * of milliseconds since the epoch of 1970-01-01T00:00:00Z int UTC ZoneOffset
 *
 * @author Artem Zinkevich
 */
public class ToMilliseconds {
    public static String milliseconds(LocalDateTime dateTime) {
        String res;
        if (dateTime == null) {
            res = "";
        } else {
            res = String.valueOf(dateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli());
        }
        return res;
    }
}
