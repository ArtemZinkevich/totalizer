package com.rtmznk.totalizer.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static com.rtmznk.totalizer.constant.RequestAttributeName.THROWABLE;

/**
 * Represents a tag without body which prints stackTrace of exception.
 *
 * @author Artem Zinkevich
 */
public class StackTraceTag extends TagSupport {

    @Override
    public int doStartTag() throws JspException {
        Throwable throwable = (Throwable) pageContext.getRequest().getAttribute(THROWABLE);
        if (throwable == null) {
            throwable = pageContext.getException();
        }
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            throwable.printStackTrace(pw);
            String exception = sw.toString();
            exception = "<div>" + exception + "</div>";
            pageContext.getOut().print(exception);
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }
}