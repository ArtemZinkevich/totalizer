package com.rtmznk.totalizer.tag;

import com.rtmznk.totalizer.entity.Event;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Represents a tag without body which draws a radio button group with one selected
 * depending on {@link Event.Type}
 *
 * @author Artem Zinkevich
 */
public class SelectedEventTag extends TagSupport {
    private String type;
    private static final String LOCALIZED_SCORE = "score";
    private static final String LOCALIZED_TOTAL = "total";
    private static final String LOCALIZED_RESULT = "result";
    private static final String[] NO_TYPE = {"<option value=\"result\">", "</option>\n" +
            "<option value=\"score\">", "</option>\n" +
            "<option value=\"total\">", "</option>"};
    private static final String[] RESULT_TYPE = {"<option value=\"result\" selected>", "</option>\n" +
            "<option value=\"score\">", "</option>\n" +
            "<option value=\"total\">", "</option>"};
    private static final String[] TOTAL_TYPE = {"<option value=\"result\">", "</option>\n" +
            "<option value=\"score\">", "</option>\n" +
            "<option value=\"total\" selected>", "</option>"};
    private static final String[] SCORE_TYPE = {"<option value=\"result\">", "</option>\n" +
            "<option value=\"score\" selected>", "</option>\n" +
            "<option value=\"total\">", "</option>"};

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int doStartTag() throws JspException {
        String[] output = null;
        if (Event.Type.isValidType(type)) {
            Event.Type currentType = Event.Type.valueOf(type.toUpperCase());
            switch (currentType) {
                case SCORE: {
                    output = SCORE_TYPE;
                    break;
                }
                case TOTAL: {
                    output = TOTAL_TYPE;
                    break;
                }
                case RESULT: {
                    output = RESULT_TYPE;
                    break;
                }
            }
        } else {
            output = NO_TYPE;
        }
        try {
            String score = (String) pageContext.getAttribute(LOCALIZED_SCORE, PageContext.PAGE_SCOPE);
            String total = (String) pageContext.getAttribute(LOCALIZED_TOTAL, PageContext.PAGE_SCOPE);
            String result = (String) pageContext.getAttribute(LOCALIZED_RESULT, PageContext.PAGE_SCOPE);
            String[] values = {result, score, total};
            StringBuffer res = new StringBuffer();
            for (int i = 0; i < output.length; i++) {
                res.append(output[i]);
                if (i < values.length) {
                    res.append(values[i]);
                }
            }
            pageContext.getOut().write(res.toString());
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
