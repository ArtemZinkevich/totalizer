package com.rtmznk.totalizer.tag;

import com.rtmznk.totalizer.entity.User;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Represents a tag with body which shows body content only if user is not null
 * and user role is "user"
 *
 * @author Artem Zinkevich
 */
public class UserTag extends TagSupport {
    private static final String USER_ATTR = "user";
    private static final String ROLE_USER = "user";


    @Override
    public int doStartTag() throws JspException {
        User user = (User) pageContext.getSession().getAttribute(USER_ATTR);
        int result;
        if (user != null) {
            if (user.getRole().equals(ROLE_USER)) {
                result = EVAL_BODY_INCLUDE;
            } else {
                result = SKIP_BODY;
            }
        } else {
            result = SKIP_BODY;
        }
        return result;
    }
}