package com.rtmznk.totalizer.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import static java.sql.DriverManager.*;

/**
 * Class represents pool of {@link ProxyConnection} objects.
 * Class is thread-safe.
 *
 * @author Artem Zinkevich
 */
public class ConnectionPool {
    private static Logger logger = LoggerFactory.getLogger(ConnectionPool.class);
    private BlockingQueue<Connection> availableConnections;
    private BlockingQueue<Connection> connectionsInUse;
    private static AtomicBoolean created = new AtomicBoolean();
    private static ReentrantLock lock = new ReentrantLock();
    private static double DUTY_FACTOR = 0.8d;
    private static ConnectionPool instance;
    private ConnectionManager connectionManager;


    private ConnectionPool() {
        connectionManager = ConnectionManager.getInstance();
        initConnectionPool();
    }

    /**
     * Initialize connection pool. Fill it with connections until it is
     * full with a duty factor at least {@param DUTY_FACTOR}
     */
    private void initConnectionPool() {
        int poolSize = connectionManager.getPoolSize();
        availableConnections = new ArrayBlockingQueue<>(poolSize);
        connectionsInUse = new ArrayBlockingQueue<>(poolSize);
        logger.info("Connection Pool filling.");
        try {
            fillPool(poolSize);
        } catch (SQLException e) {
            logger.error("SQL Exception in connection instance class, sql state is: " + e.getSQLState() +
                    "\nerror code is: " + e.getErrorCode(), e);
        }
        int currentPoolSize = availableConnections.size();
        if (currentPoolSize < poolSize) {
            try {
                fillPool(poolSize - currentPoolSize);
            } catch (SQLException e) {
                logger.error("SQL Exception in connection instance class, sql state is: " + e.getSQLState() +
                        "\nerror code is: " + e.getErrorCode(), e);
            }
        }
        currentPoolSize = availableConnections.size();
        if (currentPoolSize >= DUTY_FACTOR * poolSize) {
            logger.info(poolSize + " connections was created");
        } else {
            throw new RuntimeException("Pool is not filled with enough connections");
        }
    }

    /**
     * Add connections to pool
     *
     * @param amount {@code int} amount of connections
     * @throws SQLException if it's impossible to get a connection
     */
    private void fillPool(int amount) throws SQLException {
        for (int i = 0; i < amount; i++) {
            availableConnections.add(
                    connectionManager.receiveConnection());
        }
    }

    /**
     * @return single instance of {@link ConnectionPool}
     */

    public static ConnectionPool getInstance() {
        if (!created.get()) {
            lock.lock();
            try {
                if (!created.get()) {
                    instance = new ConnectionPool();
                    created.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Take connection from pool
     *
     * @return {@code Connection} object
     */
    public Connection takeConnection() {
        Connection connection = null;
        try {
            connection = availableConnections.take();
            connectionsInUse.put(connection);
        } catch (InterruptedException e) {
            logger.error("Thread waiting for connection was interrupted", e);
        }
        return connection;
    }

    /**
     * Return connection in pool
     */
    void returnConnection(Connection connection) {
        if (connectionsInUse.remove(connection)) {
            try {
                availableConnections.put(connection);
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
        } else {
            logger.warn("Element " + connection + " is not from this instance");
        }
    }

    /**
     * Close all connections and call deregister all drivers
     *
     * @see ConnectionManager
     */
    public void destroyConnectionPool() {
        logger.warn("destroying the instance, any connections will be closed and removed, and no longer available");
        if (created.get()) {
            for (int i = 0; i < availableConnections.size(); i++) {
                try {
                    ProxyConnection proxyCon = (ProxyConnection) availableConnections.take();
                    proxyCon.realClose();
                } catch (SQLException e) {
                    logger.error("database access error occurs", e);
                } catch (InterruptedException e) {
                    logger.error("thread was interrupted while waiting for available connection", e);
                }
            }
        }
        connectionManager.deregisterDrivers();
    }
}
