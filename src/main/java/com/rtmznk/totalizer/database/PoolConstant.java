package com.rtmznk.totalizer.database;

/**
 * Class represents constant which is used while connection obtaining.
 *
 * @author Artem Zinkevich
 */
class PoolConstant {
    final static String DB_URL = "url";
    final static String DB_DRIVER = "driver";
    final static String DB_USER = "user";
    final static String DB_PASSWORD = "password";
    final static String DB_POOL_SIZE = "poolSize";

    private PoolConstant() {
    }
}
