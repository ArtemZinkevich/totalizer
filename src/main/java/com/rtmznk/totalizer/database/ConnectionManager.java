package com.rtmznk.totalizer.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import static java.sql.DriverManager.*;
import static com.rtmznk.totalizer.database.PoolConstant.*;
import static com.rtmznk.totalizer.database.PoolConstant.DB_PASSWORD;

/**
 * Class helper to achieve connection to database
 * with parameters from properties;
 *
 * @author Artem Zinkevich
 */
class ConnectionManager {
    private static Logger logger = LoggerFactory.getLogger(ConnectionManager.class);
    private int poolSize;
    private String dbURL;
    private String dbUser;
    private String dbPassword;
    private static ConnectionManager instance = new ConnectionManager();

    private ConnectionManager() {
        Properties dbProperties = new Properties();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        try (InputStream is = classloader.getResourceAsStream("db.properties")) {
            dbProperties.load(is);
            Class.forName(dbProperties.getProperty(DB_DRIVER));
            poolSize = Integer.parseInt(dbProperties.getProperty(DB_POOL_SIZE));
            dbURL = dbProperties.getProperty(DB_URL);
            dbUser = dbProperties.getProperty(DB_USER);
            dbPassword = dbProperties.getProperty(DB_PASSWORD);
        } catch (ClassNotFoundException e) {
            logger.error("ClassNotFoundException in connection instance class:\n" + e.getMessage());
            throw new RuntimeException(e);
        } catch (IOException e) {
            logger.error("IO Exception during connection instance initializing:\n" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    /**
     * @return single instance of {@link ConnectionManager}
     */
    static ConnectionManager getInstance() {
        return instance;
    }

    Connection receiveConnection() throws SQLException {
        return new ProxyConnection(getConnection(dbURL, dbUser, dbPassword));
    }

    int getPoolSize() {
        return poolSize;
    }

    void deregisterDrivers() {
        Enumeration<Driver> drivers = getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                deregisterDriver(driver);
                logger.info(String.format("deregister jdbc driver: %s", driver));
            } catch (SQLException e) {
                logger.error(String.format("Error deregister driver %s", driver), e);
            }
        }
    }
}
