package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.User;

import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import static com.rtmznk.totalizer.command.CommandManager.CommandEnum.GO_TO_REGISTER_PAGE;
import static com.rtmznk.totalizer.constant.RequestParameterName.EMAIL;
import static com.rtmznk.totalizer.constant.RequestParameterName.LOGIN_PASSWORD;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for
 * login in system. Appends {@link User} object to session.
 *
 * @author Artem Zinkevich
 */
class LoginCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(LoginCommand.class);
    private static String USER_BANNED_MESSAGE = "banned";
    private static String USER_DATA_INCORRECT = "loginFailed";
    private static String LOGIN_ERROR = "loginError";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        HttpSession session = req.getSession();
        String mail = req.getParameter(EMAIL);
        String password = req.getParameter(LOGIN_PASSWORD);
        UserService userService = ServiceFactory.getInstance().getUserService();
        User user;
        try {
            user = userService.loginAction(mail, password);
            if (user != null) {
                if (!user.getBanned()) {
                    session.setAttribute(SessionAttributeName.USER, user);
                    commandResult = new CommandResult(CommandManager.CommandEnum.GO_TO_PROFILE.getCommandUrl(),
                            CommandResult.ResultAction.REDIRECT);

                } else {
                    session.setAttribute(LOGIN_ERROR, USER_BANNED_MESSAGE);
                    commandResult = new CommandResult(GO_TO_REGISTER_PAGE.getCommandUrl(), CommandResult.ResultAction.REDIRECT);
                }
            } else {
                session.setAttribute(LOGIN_ERROR, USER_DATA_INCORRECT);
                commandResult = new CommandResult(GO_TO_REGISTER_PAGE.getCommandUrl(), CommandResult.ResultAction.REDIRECT);
            }
        } catch (ServiceException e) {
            logger.error("Can't log in", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            commandResult = new CommandResult();
            commandResult.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return commandResult;
    }
}
