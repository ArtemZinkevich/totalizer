package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.TOTOLIZER_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.ERROR;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;

/**
 * This command updates totalizer status to playable, that makes it
 * accessible for users.
 * Available only for "admin".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class StartTotalizerCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(StartTotalizerCommand.class);
    private static final String START_SUCCESS = "startSuccess";
    private static final String EVENT_TIME_ERROR = "timeError";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        Map<String, String> errors;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        try {
            errors = adminService.checkEditionPossibility();
            if (errors.isEmpty()) {
                if (adminService.startTotalizer()) {
                    req.setAttribute(SUCCESS, START_SUCCESS);
                } else {
                    req.setAttribute(ERROR, EVENT_TIME_ERROR);
                }
            } else {
                req.setAttribute(VALIDATION_ERRORS, errors);
            }
            result = new CommandResult(TOTOLIZER_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Totolizer start failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}

