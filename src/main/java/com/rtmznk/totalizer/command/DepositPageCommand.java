package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.PageLinks.DEPOSIT_PAGE;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the deposit page. Available only for "user".
 *
 * @author Artem Zinkevich
 */
class DepositPageCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        return new CommandResult(DEPOSIT_PAGE, CommandResult.ResultAction.FORWARD);
    }
}
