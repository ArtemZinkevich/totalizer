package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.PageLinks.REGISTRATION_AND_LOGIN;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page with form for register new user account.
 *
 * @author Artem Zinkevich
 */
class RegistrationPageCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        commandResult = new CommandResult(REGISTRATION_AND_LOGIN, CommandResult.ResultAction.FORWARD);
        return commandResult;
    }
}
