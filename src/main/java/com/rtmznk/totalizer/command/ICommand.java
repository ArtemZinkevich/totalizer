package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Represents ICommand interface implementation of which provides appropriate realization of <code>execute</code> method.
 *
 * @author Artem Zinkevich
 */
interface ICommand {
    /**
     * Executes the request that is sent to the servlet and prepares the response object to be returned to the client.
     *
     * @param req  the {@link javax.servlet.http.HttpServletRequest} object that is sent to the servlet.
     * @param resp the {@link javax.servlet.http.HttpServletResponse} object that the servlet uses to return the headers to the client.
     * @return The {@code CommandResult} object which represents the object that contains necessary information.
     * @see HttpServletRequest
     * @see HttpServletResponse
     */
    CommandResult execute(HttpServletRequest req, HttpServletResponse resp);
}
