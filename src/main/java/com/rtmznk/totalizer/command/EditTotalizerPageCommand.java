package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.TOTOLIZER_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.TOTALIZER;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page where it is possible to edit totalizer. Available only for "admin".
 *
 * @author Artem Zinkevich
 */
class EditTotalizerPageCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(EditTotalizerPageCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        Map<String, String> errors;
        try {
            errors = adminService.checkEditionPossibility();
            if (!errors.isEmpty()) {
                req.setAttribute(VALIDATION_ERRORS, errors);
            } else {
                Totalizer totalizer = adminService.findLastTotalizerWithEvents();
                req.setAttribute(TOTALIZER, totalizer);
            }
            result = new CommandResult(TOTOLIZER_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not obtain totalizer edition page", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
