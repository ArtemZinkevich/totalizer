package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import java.util.Optional;

/**
 * Represents the class which provides enum of all available
 * command names and implementations of {@link com.rtmznk.totalizer.command.ICommand}.
 * <p>
 * Instance of this class get HttpServletRequest and HttpServletResponse from controller servlet
 *
 * @author Artem Zinkevich
 * @see com.rtmznk.totalizer.servlet.Controller . Find command according to request url
 * and return result of this command execution back to servlet.
 */
public class CommandManager {
    private static Logger logger = LoggerFactory.getLogger(CommandManager.class);
    private static final CommandManager instance = new CommandManager();
    private static final String COMMAND_PREFIX = "/command/";

    /**
     * enum of possible HTTP methods for this application
     */
    enum HttpMethod {
        POST("POST"), GET("GET");
        private String name;

        HttpMethod(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    /**
     * enum that contains all available commands that implemts
     * {@link com.rtmznk.totalizer.command.ICommand}
     */
    enum CommandEnum {
        LOGIN("login", HttpMethod.POST, new LoginCommand()),
        CREATE_USER("create", HttpMethod.POST, new CreateUserCommand()),
        CHECK_MAIL("mail-check", HttpMethod.POST, new CheckMailCommand()),
        CHANGE_LANG("i18n", HttpMethod.POST, new ChangeLangCommand()),
        CHANGE_PASS("profile/change-password", HttpMethod.POST, new ChangePasswordCommand()),
        LOGOUT("logout", HttpMethod.POST, new LogoutCommand()),
        GO_TO_REGISTER_PAGE("register", HttpMethod.GET, new RegistrationPageCommand()),
        GO_TO_MAIN_PAGE("", HttpMethod.GET, new MainPageCommand()),
        GO_TO_PROFILE("profile", HttpMethod.GET, new ProfilePageCommand()),
        GO_TO_HISTORY("history", HttpMethod.GET, new EventsHistoryPageCommand()),
        GO_PROFILE_UPDATE("go-profile-update", HttpMethod.POST, new ProfileRedirectCommand()),
        GO_TO_RULES_PAGE("rules", HttpMethod.GET, new RulesPageCommand()),
        GO_TO_PASS_PAGE("profile/pass-page", HttpMethod.POST, new PassPageCommand()),
        GO_TO_DEPOSIT_PAGE("profile/user/deposit-page", HttpMethod.POST, new DepositPageCommand()),
        MAKE_DEPOSIT("profile/user/make-deposit", HttpMethod.POST, new MakeDepositCommand()),
        USER_LIST("profile/admin/user-list", HttpMethod.POST, new UserListCommand()),
        BAN_USER("profile/admin/ban-unban", HttpMethod.POST, new BanUserCommand()),
        GO_TO_CREATE_TOTALIZER_PAGE("profile/admin/create-toto-page", HttpMethod.POST, new CreateTotalizerPageCommand()),
        CREATE_TOTALIZER("profile/admin/create-totalizer", HttpMethod.POST, new CreateTotalizerCommand()),
        GO_TO_EDIT_TOTALIZER("profile/admin/edit-toto-page", HttpMethod.POST, new EditTotalizerPageCommand()),
        UPDATE_TOTALIZER("profile/admin/update-totalizer", HttpMethod.POST, new UpdateTotalizerCommand()),
        START_TOTALIZER("profile/admin/start-totalizer", HttpMethod.POST, new StartTotalizerCommand()),
        GO_TO_GAME_PAGE("game", HttpMethod.POST, new GamePageCommand()),
        GAME_PAGE("game", HttpMethod.GET, new GamePageCommand()),
        MAKE_BET("profile/user/make-bet", HttpMethod.POST, new MakeBetCommand()),
        CANCEL_TOTALIZER("profile/admin/cancel-totalizer", HttpMethod.POST, new CancelTotalizerCommand()),
        FINISH_TOTALIZER("profile/admin/finish-totalizer", HttpMethod.POST, new FinishTotalizerCommand()),
        CANCEL_BET("profile/user/cancel-bet", HttpMethod.POST, new CancelBetCommand()),
        REPAY_LOAN("profile/user/repay-loan", HttpMethod.POST, new RepayCreditCommand()),
        REMITTANCE_LIST("profile/user/remittances", HttpMethod.POST, new RemittanceListCommand());

        private String commandUrl;
        private HttpMethod method;
        private ICommand command;

        CommandEnum(String commandName, HttpMethod method, ICommand command) {
            this.commandUrl = commandName;
            this.method = method;
            this.command = command;
        }


        public String getCommandUrl() {
            return commandUrl;
        }

        public HttpMethod getMethod() {
            return method;
        }

        public ICommand getCommand() {
            return command;
        }

        /**
         * @param url from HttpRequest
         * @return ICommand instance from enum
         */
        public static ICommand receiveCommandByUrl(String url) {
            Optional<CommandEnum> result = Arrays.stream(CommandEnum.values()).filter(commandEnum ->
                    commandEnum.getCommandUrl().equalsIgnoreCase(url)).findFirst();
            return result.map(CommandEnum::getCommand).orElse(null);
        }

        public static boolean containsCommand(String url, String method) {
            return Arrays.stream(CommandEnum.values()).anyMatch(commandEnum ->
                    commandEnum.getCommandUrl().equalsIgnoreCase(url) &&
                            commandEnum.getMethod().getName().equalsIgnoreCase(method));
        }
    }

    private CommandManager() {
    }

    /**
     *
     * @return single instance of {@link CommandManager}
     */

    public static CommandManager getInstance() {
        return instance;
    }

    /**
     * This command execute command according to request
     * @param req {@link javax.servlet.http.HttpServletRequest}
     * @param resp {@link javax.servlet.http.HttpServletResponse}
     * @return result of command execution
     */
    public CommandResult executeCommand(HttpServletRequest req, HttpServletResponse resp) {
        ICommand command;
        String commandName = req.getRequestURI().replace(COMMAND_PREFIX, "");
        String contextPath = req.getServletContext().getContextPath();
        commandName = commandName.replace(contextPath, "");
        CommandResult result;
        if (CommandEnum.containsCommand(commandName, req.getMethod())) {
            command = CommandEnum.receiveCommandByUrl(commandName);
            result = command.execute(req, resp);
            if (result.getResultAction() == CommandResult.ResultAction.REDIRECT) {
                result.setResultRoute(contextPath + "/" + result.getResultRoute());
            }
        } else {
            logger.info("Unsupported command: " + commandName);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.PAGE_NOT_FOUND);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
