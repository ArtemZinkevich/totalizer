package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.GambleService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.PageLinks.GAME_PAGE;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page where users can make bets.
 *
 * @author Artem Zinkevich
 */
class GamePageCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(GamePageCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        GambleService service = ServiceFactory.getInstance().getGambleService();
        Totalizer totalizer = null;
        try {
            totalizer = service.findAvailableTotalizer();
            System.out.println(totalizer);
            if (totalizer != null) {
                req.setAttribute(RequestAttributeName.TOTALIZER, totalizer);
            }
            commandResult = new CommandResult(GAME_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not find available totalizer ban status", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            commandResult = new CommandResult();
            commandResult.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return commandResult;
    }
}
