package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.GambleService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.GAME_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.TOTALIZER;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;
import static com.rtmznk.totalizer.constant.RequestParameterName.JSON;
import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * This command create new {@link com.rtmznk.totalizer.entity.Bet} object from request incoming parameters.
 * Available only for "user".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class MakeBetCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(MakeBetCommand.class);
    private static final String ALREADY_EXIST = "alreadyExist";
    private static final String CREATION_SUCCESS = "creationSuccess";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        Map<String, Object> resultMap;
        Map<String, String> errors = new HashMap<>();
        User user = (User) req.getSession().getAttribute(USER);
        Totalizer totalizer = null;
        GambleService gambleService = ServiceFactory.getInstance().getGambleService();
        String couponJson = req.getParameter(JSON);
        try {
            if (!gambleService.checkMakeBetPossibility(user)) {
                errors.put(ALREADY_EXIST, ALREADY_EXIST);
            }
            if (errors.isEmpty()) {
                resultMap = gambleService.createCoupon(couponJson, user);
                if (!resultMap.isEmpty()) {
                    errors.putAll((Map<String, String>) resultMap.get(VALIDATION_ERRORS));
                    totalizer = (Totalizer) resultMap.get(TOTALIZER);
                }
            }
            if (!errors.isEmpty()) {
                req.setAttribute(VALIDATION_ERRORS, errors);
                req.setAttribute(TOTALIZER, totalizer);
            } else {
                req.setAttribute(SUCCESS, CREATION_SUCCESS);
            }
            result = new CommandResult(GAME_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Totolizer making bet failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
