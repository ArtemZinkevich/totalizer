package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import com.rtmznk.totalizer.utility.RequestDataReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;


/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for
 * creating a new user account.
 *
 * @author Artem Zinkevich
 */
class CreateUserCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(CreateUserCommand.class);
    private static final String REGISTRATION_SUCCESSFUL = "successfulRegistration";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        HttpSession session = req.getSession();
        Map<String, String> requestParams = RequestDataReceiver.getInstance().receiveRegistrationData(req);
        Map<String, String> registrationErrors;
        try {
            UserService service = ServiceFactory.getInstance().getUserService();
            registrationErrors = service.registerUser(requestParams);
            if (registrationErrors.isEmpty()) {
                session.setAttribute(SessionAttributeName.REGISTRATION_OK, REGISTRATION_SUCCESSFUL);
                commandResult = new CommandResult(CommandManager.CommandEnum.GO_TO_REGISTER_PAGE.getCommandUrl(),
                        CommandResult.ResultAction.REDIRECT);
            } else {
                session.setAttribute(SessionAttributeName.PARAMETER_VALIDATION_ERRORS, registrationErrors);
                commandResult = new CommandResult(CommandManager.CommandEnum.GO_TO_REGISTER_PAGE.getCommandUrl(),
                        CommandResult.ResultAction.REDIRECT);
            }
        } catch (ServiceException e) {
            logger.error("Can not create new user", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            commandResult = new CommandResult();
            commandResult.setResultAction(CommandResult.ResultAction.ERROR);
        }

        return commandResult;
    }
}
