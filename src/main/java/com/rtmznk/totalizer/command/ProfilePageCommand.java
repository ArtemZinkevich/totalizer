package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.CreditMultiplierValue;
import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.Coupon;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.GambleService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.math.BigDecimal;
import java.util.Optional;

import static com.rtmznk.totalizer.command.PageLinks.PROFILE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.CREDIT;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the profile page.
 *
 * @author Artem Zinkevich
 */
class ProfilePageCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(ProfilePageCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(SessionAttributeName.USER);
        if (user != null) {
            GambleService service = ServiceFactory.getInstance().getGambleService();
            try {
                Coupon coupon = service.findActualCoupon(user);
                if (coupon != null) {
                    BigDecimal credit = Optional.ofNullable(coupon.getCredit()).orElse(BigDecimal.ZERO);
                    req.setAttribute(CREDIT, credit.multiply(BigDecimal.valueOf(CreditMultiplierValue.CREDIT_MULTIPLIER_NORMAL)));
                } else {
                    req.setAttribute(CREDIT, BigDecimal.ZERO);
                }
            } catch (ServiceException e) {
                logger.error("Can't find profile page data", e);
                req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
                req.setAttribute(RequestAttributeName.THROWABLE, e);
                commandResult = new CommandResult();
                commandResult.setResultAction(CommandResult.ResultAction.ERROR);
            }
            commandResult = new CommandResult(PROFILE, CommandResult.ResultAction.FORWARD);
        } else {
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.UNAUTORIZED);
            commandResult = new CommandResult();
            commandResult.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return commandResult;
    }
}
