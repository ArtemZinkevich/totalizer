package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.rtmznk.totalizer.command.PageLinks.PASSWORD_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.ERROR;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestParameterName.NEW_PASSWORD;
import static com.rtmznk.totalizer.constant.RequestParameterName.OLD_PASSWORD;
import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * This command extract from request  information about password, and
 * change user password according to extracted information.
 * Append message about result of command execution to request.
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class ChangePasswordCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(ChangePasswordCommand.class);
    private static final String ERROR_MESSAGE = "error_pass";
    private static final String SUCCESS_MESSAGE = "ok_pass";

    ChangePasswordCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result = new CommandResult(PASSWORD_PAGE, CommandResult.ResultAction.FORWARD);
        String oldPass = req.getParameter(OLD_PASSWORD);
        String newPass = req.getParameter(NEW_PASSWORD);
        HttpSession session = req.getSession();
        UserService service = ServiceFactory.getInstance().getUserService();
        User user = (User) session.getAttribute(USER);
        try {
            if (service.changeUserPassword(user, oldPass, newPass)) {
                user = service.updateUserInfo(user);
                session.setAttribute(USER, user);
                req.setAttribute(SUCCESS, SUCCESS_MESSAGE);
            } else {
                req.setAttribute(ERROR, ERROR_MESSAGE);
            }
        } catch (ServiceException e) {
            logger.error("Change password command failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
