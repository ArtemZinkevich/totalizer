package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.PageLinks.PASSWORD_PAGE;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page with form to change user password.
 *
 * @author Artem Zinkevich
 */
class PassPageCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        return new CommandResult(PASSWORD_PAGE, CommandResult.ResultAction.FORWARD);
    }
}
