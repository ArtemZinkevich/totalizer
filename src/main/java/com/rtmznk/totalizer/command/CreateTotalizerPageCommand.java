package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.TOTOLIZER_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;


/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page for creating totalizer. Available only for "admin".
 *
 * @author Artem Zinkevich
 */
class CreateTotalizerPageCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(CreateTotalizerPageCommand.class);

    CreateTotalizerPageCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        Map<String, String> errors;
        try {
            errors = adminService.checkCreationPossibility();
            if (!errors.isEmpty()) {
                req.setAttribute(VALIDATION_ERRORS, errors);
            }
            result = new CommandResult(TOTOLIZER_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not obtain totalizer creation page", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
