package com.rtmznk.totalizer.command;

/**
 * This class represents result of command execution
 */
public class CommandResult {
    /**
     * enum which represents type of result controller action
     */
    public enum ResultAction {
        FORWARD, REDIRECT, AJAX, ERROR
    }

    private String resultRoute;
    private ResultAction resultAction;
    private String resultMessage;

    public CommandResult() {
    }

    public CommandResult(String resultRoute, ResultAction resultAction) {
        this.resultRoute = resultRoute;
        this.resultAction = resultAction;
    }

    public CommandResult(String resultRoute, ResultAction resultAction, String resultMessage) {
        this.resultRoute = resultRoute;
        this.resultAction = resultAction;
        this.resultMessage = resultMessage;
    }

    public String getResultRoute() {
        return resultRoute;
    }

    public void setResultRoute(String resultRoute) {
        this.resultRoute = resultRoute;
    }

    public ResultAction getResultAction() {
        return resultAction;
    }

    public void setResultAction(ResultAction resultAction) {
        this.resultAction = resultAction;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
