package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.TOTOLIZER_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.TOTALIZER;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;
import static com.rtmznk.totalizer.constant.RequestParameterName.JSON;

/**
 * This command create totalizer from request incoming parameters.
 * Available only for "admin".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class CreateTotalizerCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(CreateTotalizerCommand.class);
    private static final String CREATION_SUCCESS = "creationSuccess";

    CreateTotalizerCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result = null;
        Map<String, Object> resultMap;
        Map<String, String> errors;
        Totalizer totalizer = null;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        String totolizerJson = req.getParameter(JSON);
        try {
            errors = adminService.checkCreationPossibility();
            if (errors.isEmpty()) {
                resultMap = adminService.createTotalizer(totolizerJson);
                if (!resultMap.isEmpty()) {
                    errors = (Map<String, String>) resultMap.get(VALIDATION_ERRORS);
                    totalizer = (Totalizer) resultMap.get(TOTALIZER);
                }
            }
            if (!errors.isEmpty()) {
                req.setAttribute(VALIDATION_ERRORS, errors);
                req.setAttribute(TOTALIZER, totalizer);
            } else {
                req.setAttribute(SUCCESS, CREATION_SUCCESS);
            }
            result = new CommandResult(TOTOLIZER_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Totolizer creation failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
