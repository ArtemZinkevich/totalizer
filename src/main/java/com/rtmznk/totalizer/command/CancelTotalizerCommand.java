package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.OPERATION_RESULT_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;

/**
 * This command cancel last created totalizer, delete it from database
 * and return users money back Available only for "admin".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class CancelTotalizerCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(CancelTotalizerCommand.class);
    private static String CANCELLATION_SUCCESS = "cancellationSuccess";
    private static String CANCELLATION_FAILED = "cancellationFailed";

    CancelTotalizerCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        Map<String, String> errors;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        try {
            errors = adminService.checkCancellationPossibility();
            if (errors.isEmpty()) {
                if (adminService.cancelTotalizer()) {
                    req.setAttribute(SUCCESS, CANCELLATION_SUCCESS);
                } else {
                    errors = new HashMap<>();
                    errors.put(CANCELLATION_FAILED, CANCELLATION_FAILED);
                    req.setAttribute(VALIDATION_ERRORS, errors);
                }
            } else {
                req.setAttribute(VALIDATION_ERRORS, errors);
            }
            result = new CommandResult(OPERATION_RESULT_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Totolizer cancellation failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
