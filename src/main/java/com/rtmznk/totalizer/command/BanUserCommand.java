package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.RequestParameterName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.rtmznk.totalizer.command.PageLinks.USER_LIST_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.ERROR;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.USER_LIST;

/**
 * This command switch user ban status like trigger switching status from
 * true to false and backwards.Available only for "admin".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to ban user
 *
 * @author Artem Zinkevich
 */
class BanUserCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(BanUserCommand.class);
    private static final String BAN_SUCCESS = "banSuccess";
    private static final String BAN_FAIL = "banFail";

    BanUserCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        List<User> userList;
        boolean res;
        String id = req.getParameter(RequestParameterName.ID);
        try {
            res = adminService.changeUserBanStatus(id);
            if (res) {
                req.setAttribute(SUCCESS, BAN_SUCCESS);
            } else {
                req.setAttribute(ERROR, BAN_FAIL);
            }
            userList = adminService.findAllUsers();
            req.setAttribute(USER_LIST, userList);
            result = new CommandResult(USER_LIST_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not change ban status", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
