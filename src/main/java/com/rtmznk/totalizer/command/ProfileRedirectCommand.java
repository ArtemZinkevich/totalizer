package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static com.rtmznk.totalizer.command.CommandManager.CommandEnum.GO_TO_PROFILE;
import static com.rtmznk.totalizer.command.CommandManager.CommandEnum.GO_TO_REGISTER_PAGE;
import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the profile page, but before it update user data from database.
 *
 * @author Artem Zinkevich
 */
class ProfileRedirectCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(ProfilePageCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute(USER);
        CommandResult commandResult;
        UserService service = ServiceFactory.getInstance().getUserService();
        try {
            user = service.updateUserInfo(user);
            if (user.getBanned()) {
                session.removeAttribute(USER);
                session.invalidate();
                commandResult = new CommandResult(GO_TO_REGISTER_PAGE.getCommandUrl(), CommandResult.ResultAction.REDIRECT);
            } else {
                req.getSession().setAttribute(USER, user);
                commandResult = new CommandResult(GO_TO_PROFILE.getCommandUrl(), CommandResult.ResultAction.REDIRECT);
            }
        } catch (ServiceException e) {
            logger.error("Redirect to profile page with updated data failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            commandResult = new CommandResult();
            commandResult.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return commandResult;
    }
}
