package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.Remittance;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.rtmznk.totalizer.command.PageLinks.REMITTANCES_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.REMITTANCES;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the page with list of user remittances. Available for "user".
 * This command put list of {@link Remittance} objects in request.
 *
 * @author Artem Zinkevich
 */
class RemittanceListCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(RemittanceListCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        UserService userService = ServiceFactory.getInstance().getUserService();
        User user = (User) req.getSession().getAttribute(SessionAttributeName.USER);
        List<Remittance> remittances;
        try {
            remittances = userService.findAllRemittances(user);
            req.setAttribute(REMITTANCES, remittances);
            result = new CommandResult(REMITTANCES_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not obtain user remittances", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
