package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.Event;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.rtmznk.totalizer.command.PageLinks.HISTORY_PAGE;


/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 * for a command to go to the page with event results.
 *
 * @author Artem Zinkevich
 */
class EventsHistoryPageCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(EventsHistoryPageCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        UserService userService = ServiceFactory.getInstance().getUserService();
        List<Event> events = null;
        try {
            events = userService.findAllFinishedEvents();
            req.setAttribute(RequestAttributeName.EVENTS, events);
            result = new CommandResult(HISTORY_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not obtain finished events", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}