package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.GambleService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.OPERATION_RESULT_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;
import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * This command subtract value of coupon credit sum from user balance
 * and update both in database.
 * Available only for "user".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class RepayCreditCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(RepayCreditCommand.class);
    private static String REPAY_SUCCESS = "repaySuccess";
    private static String REPAY_FAILED = "repayFailed";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        User user = (User) req.getSession().getAttribute(USER);
        GambleService gambleService = ServiceFactory.getInstance().getGambleService();
        boolean flag;
        try {
            Map<String, String> errors = new HashMap<>();
            errors.putAll(gambleService.checkAnyEventStarted());
            flag = errors.isEmpty();
            if (flag) {
                flag = gambleService.RepayCredit(user);
                if (flag) {
                    req.setAttribute(SUCCESS, REPAY_SUCCESS);
                } else {
                    errors.put(REPAY_FAILED, REPAY_FAILED);
                    req.setAttribute(VALIDATION_ERRORS, errors);
                }
            } else {
                req.setAttribute(VALIDATION_ERRORS, errors);
            }
            result = new CommandResult(OPERATION_RESULT_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Credit repay failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
