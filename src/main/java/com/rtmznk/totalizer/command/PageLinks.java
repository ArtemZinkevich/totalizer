package com.rtmznk.totalizer.command;

/**
 * This class represents constants with links for all available jsp.
 */
class PageLinks {
    static final String INDEX = "/index.jsp";
    static final String PROFILE = "/WEB-INF/jsp/profile.jsp";
    static final String REGISTRATION_AND_LOGIN = "/WEB-INF/jsp/registration.jsp";
    static final String USER_LIST_PAGE = "/WEB-INF/jsp/external/userlist.jsp";
    static final String PASSWORD_PAGE = "/WEB-INF/jsp/external/change-pass.jsp";
    static final String DEPOSIT_PAGE = "/WEB-INF/jsp/external/deposit.jsp";
    static final String TOTOLIZER_PAGE = "/WEB-INF/jsp/external/totalizer.jsp";
    static final String GAME_PAGE = "/WEB-INF/jsp/game.jsp";
    static final String OPERATION_RESULT_PAGE = "/WEB-INF/jsp/external/operation-result.jsp";
    static final String REMITTANCES_PAGE = "/WEB-INF/jsp/external/remittance-list.jsp";
    static final String HISTORY_PAGE = "/WEB-INF/jsp/events-history.jsp";
    static final String RULES_PAGE = "/WEB-INF/jsp/rules.jsp";

    private PageLinks() {
    }
}
