package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the rules page.
 *
 * @author Artem Zinkevich
 */
class RulesPageCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult commandResult;
        commandResult = new CommandResult(PageLinks.RULES_PAGE, CommandResult.ResultAction.FORWARD);
        return commandResult;
    }
}
