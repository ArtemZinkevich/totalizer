package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.constant.RequestParameterName.MAIL;


/**
 * This command extract from request information about e-mail,
 * and check existence of such e-mail in database.
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class CheckMailCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(CheckMailCommand.class);

    CheckMailCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        String mail = req.getParameter(MAIL);
        if (mail != null && !mail.isEmpty()) {
            UserService service = ServiceFactory.getInstance().getUserService();
            boolean exist = false;
            try {
                exist = service.checkMailExistence(mail);
            } catch (ServiceException e) {
                logger.error("Ajax mail check error", e);
            }
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.AJAX);
            result.setResultMessage(String.valueOf(exist));
        } else {
            result = new CommandResult();
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.PAGE_NOT_FOUND);
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
