package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import static com.rtmznk.totalizer.command.PageLinks.USER_LIST_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.USER_LIST;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to get
 * the page with list of users. Available for "admin".
 * This command put list of {@link User} objects in request.
 *
 * @author Artem Zinkevich
 */
class UserListCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(UserListCommand.class);

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        List<User> userList;
        try {
            userList = adminService.findAllUsers();
            req.setAttribute(USER_LIST, userList);
            result = new CommandResult(USER_LIST_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Can not obtain userList", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}
