package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.CommandManager.CommandEnum.GO_TO_MAIN_PAGE;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for
 * logout from system. Remove {@link User} object from session.
 *
 * @author Artem Zinkevich
 */
class LogoutCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().removeAttribute(SessionAttributeName.USER);
        return new CommandResult(GO_TO_MAIN_PAGE.getCommandUrl(),
                CommandResult.ResultAction.REDIRECT);
    }
}
