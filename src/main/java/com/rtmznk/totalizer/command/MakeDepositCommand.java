package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import com.rtmznk.totalizer.utility.RequestDataReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.DEPOSIT_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;
import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * This command updates user balance parameter according to data from request.
 * Available only for "user".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class MakeDepositCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(MakeDepositCommand.class);
    private static final String DEPOSIT_SUCCESS = "depositSuccess";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result = new CommandResult(DEPOSIT_PAGE, CommandResult.ResultAction.FORWARD);
        HttpSession session = req.getSession();
        UserService service = ServiceFactory.getInstance().getUserService();
        User user = (User) session.getAttribute(USER);
        Map<String, String> parameters = RequestDataReceiver.getInstance().receiveDepositData(req);
        try {
            Map<String, String> errors = service.makeDeposit(user, parameters);
            if (!errors.isEmpty()) {
                req.setAttribute(VALIDATION_ERRORS, errors);
            } else {
                req.setAttribute(SUCCESS, DEPOSIT_SUCCESS);
            }
        } catch (ServiceException e) {
            logger.error("Deposit command failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }

}
