package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.utility.LocaleUtility;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Locale;

import static com.rtmznk.totalizer.command.CommandManager.CommandEnum.*;
import static com.rtmznk.totalizer.constant.RequestParameterName.PAGE;

/**
 * This command extract information from request about required language, and
 * change language according to extracted information.
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class ChangeLangCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(ChangeLangCommand.class);
    private static String LANGUAGE_PARAMETER = "lang";

    ChangeLangCommand() {
    }

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        String selectedLanguage = req.getParameter(LANGUAGE_PARAMETER);
        if (selectedLanguage != null) {
            LocaleUtility localeUtility = LocaleUtility.getInstance();
            Locale locale = localeUtility.getLocaleByString(selectedLanguage);
            HttpSession session = req.getSession();
            String referrer = req.getParameter(PAGE);
            if (referrer == null || referrer.isEmpty() || !isValidReferrer(referrer)) {
                referrer = GO_TO_MAIN_PAGE.getCommandUrl();
            }
            User user = (User) session.getAttribute(SessionAttributeName.USER);
            if (user != null) {
                user.setUserLocale(locale);
                UserService service = ServiceFactory.getInstance().getUserService();
                try {
                    boolean updated = service.updateUserLanguageInDB(user);
                    if (!updated) {
                        logger.warn("The value of user preferred language was not updated in DB");
                    }
                } catch (ServiceException e) {
                    logger.error("Can not change user locale", e);
                    req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
                    req.setAttribute(RequestAttributeName.THROWABLE, e);
                    result = new CommandResult();
                    result.setResultAction(CommandResult.ResultAction.ERROR);
                }
            }
            result = new CommandResult(referrer, CommandResult.ResultAction.REDIRECT);
            session.setAttribute(SessionAttributeName.LANGUAGE, locale);
        } else {
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.PAGE_NOT_FOUND);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }

    private boolean isValidReferrer(String referrer) {
        boolean result;
        result = referrer.equals(GO_TO_REGISTER_PAGE.getCommandUrl()) ||
                referrer.equals(GO_TO_PROFILE.getCommandUrl()) ||
                referrer.equals(GO_TO_GAME_PAGE.getCommandUrl()) ||
                referrer.equals(GO_TO_RULES_PAGE.getCommandUrl()) ||
                referrer.equals(GO_TO_HISTORY.getCommandUrl());
        return result;
    }
}
