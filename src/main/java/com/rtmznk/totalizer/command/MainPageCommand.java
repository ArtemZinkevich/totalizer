package com.rtmznk.totalizer.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.rtmznk.totalizer.command.PageLinks.INDEX;

/**
 * Implements {@link com.rtmznk.totalizer.command.ICommand} for a command to go to
 * the main page.
 *
 * @author Artem Zinkevich
 */
class MainPageCommand implements ICommand {
    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        return new CommandResult(INDEX, CommandResult.ResultAction.FORWARD);
    }
}
