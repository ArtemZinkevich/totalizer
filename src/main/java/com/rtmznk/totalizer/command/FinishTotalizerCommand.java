package com.rtmznk.totalizer.command;

import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.AdminService;
import com.rtmznk.totalizer.service.ServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.rtmznk.totalizer.command.PageLinks.OPERATION_RESULT_PAGE;
import static com.rtmznk.totalizer.constant.RequestAttributeName.SUCCESS;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;

/**
 * This finishes current totalizer. Finishes all totalizer events.
 * Set event score. Set event status to "evaluated".
 * Distribute prizes among winners.
 * Available only for "admin".
 * <p>
 * Implements {@link com.rtmznk.totalizer.command.ICommand}
 *
 * @author Artem Zinkevich
 */
class FinishTotalizerCommand implements ICommand {
    private static Logger logger = LoggerFactory.getLogger(FinishTotalizerCommand.class);
    private static String FINISH_SUCCESS = "finishSuccess";
    private static String FINISH_FAILED = "finishFailed";

    @Override
    public CommandResult execute(HttpServletRequest req, HttpServletResponse resp) {
        CommandResult result;
        Map<String, String> errors;
        AdminService adminService = ServiceFactory.getInstance().getAdminService();
        try {
            errors = adminService.checkSummarizePossibility();
            if (errors.isEmpty()) {
                if (adminService.summarizeTotalizer()) {
                    req.setAttribute(SUCCESS, FINISH_SUCCESS);
                } else {
                    errors = new HashMap<>();
                    errors.put(FINISH_FAILED, FINISH_FAILED);
                    req.setAttribute(VALIDATION_ERRORS, errors);
                }
            } else {
                req.setAttribute(VALIDATION_ERRORS, errors);
            }
            result = new CommandResult(OPERATION_RESULT_PAGE, CommandResult.ResultAction.FORWARD);
        } catch (ServiceException e) {
            logger.error("Totolizer finishing failed", e);
            req.setAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE, ErrorPageMessage.INTERNAL_ERROR);
            req.setAttribute(RequestAttributeName.THROWABLE, e);
            result = new CommandResult();
            result.setResultAction(CommandResult.ResultAction.ERROR);
        }
        return result;
    }
}