package com.rtmznk.totalizer.service;

import com.rtmznk.totalizer.dao.DAOFactory;
import com.rtmznk.totalizer.dao.GambleDAO;
import com.rtmznk.totalizer.dao.TransactionManager;
import com.rtmznk.totalizer.dao.UserDAO;
import com.rtmznk.totalizer.entity.Coupon;
import com.rtmznk.totalizer.entity.Remittance;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.constant.CreditMultiplierValue;
import com.rtmznk.totalizer.utility.JsonUtility;
import com.rtmznk.totalizer.utility.RequestDataValidator;
import com.rtmznk.totalizer.utility.TotalizerStateInspector;

import java.math.BigDecimal;
import java.util.*;

import static com.rtmznk.totalizer.constant.CreditMultiplierValue.CREDIT_MULTIPLIER_NORMAL;
import static com.rtmznk.totalizer.constant.RequestAttributeName.TOTALIZER;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;

/**
 * Represents service to execute gambler command
 *
 * @author Artem Zinkevich
 */
public class GambleService {
    private DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);

    GambleService() {
    }

    /**
     * Find playable totalizer
     *
     * @return {@link Totalizer} object
     */
    public Totalizer findAvailableTotalizer() throws ServiceException {
        Totalizer totalizer;
        try (GambleDAO gambleDAO = factory.getGambleDAO()) {
            totalizer = gambleDAO.findAvailableTotalizer();
            if (totalizer != null) {
                totalizer.setEvents(gambleDAO.findAvailableEvents(totalizer));
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find Available totalizer", e);
        }
        return totalizer;
    }

    /**
     * Create new user coupon.
     *
     * @param couponJson coupon data in JSON format to be parsed in {@link Coupon}
     * @param user       user object corresponding user who tries to create this coupon
     * @return result Map object
     */
    public Map<String, Object> createCoupon(String couponJson, User user) throws ServiceException {
        Coupon coupon = JsonUtility.getInstance().parseCouponJson(couponJson);
        Map<String, Object> result = Collections.emptyMap();
        Map<String, String> errors;
        Totalizer totalizer;
        boolean flag;
        try (GambleDAO gambleDAO = factory.getGambleDAO()) {
            UserDAO userDAO = TransactionManager.getInstance().getUserDaoWithinSameTransaction(gambleDAO, factory);
            totalizer = gambleDAO.findAvailableTotalizer();
            totalizer.setEvents(gambleDAO.findAvailableEvents(totalizer));
            User updatedUser = userDAO.findUserById(user.getId());
            errors = RequestDataValidator.getInstance().validateCoupon(coupon, totalizer, updatedUser);
            flag = errors.isEmpty();
            if (flag) {
                gambleDAO.startTransaction();
                flag = gambleDAO.findCoupon(user, totalizer) == null;
                if (flag) {
                    BigDecimal usersMoney = updatedUser.getMoney();
                    updatedUser.setMoney(usersMoney.subtract(coupon.getAmount()));
                    flag = userDAO.update(updatedUser);
                    if (flag) {
                        Remittance remittance = new Remittance(coupon.getAmount(), Remittance.Type.BET);
                        flag = userDAO.createRemittance(remittance, updatedUser.getId());
                    }
                }
                if (flag) {
                    Optional<BigDecimal> creditOptional = Optional.ofNullable(coupon.getCredit());
                    coupon.setAmount(coupon.getAmount().add(creditOptional.orElse(new BigDecimal(0))));
                    Long couponId = gambleDAO.createCoupon(coupon, user.getId(), totalizer.getId());
                    flag = gambleDAO.createBets(coupon.getBets(), couponId);
                }
                if (flag) {
                    Totalizer forUpdate = gambleDAO.findAvailableTotalizer();
                    flag = forUpdate != null && forUpdate.getId().equals(totalizer.getId());
                    if (flag) {
                        flag = false;
                        while (!flag) {
                            forUpdate.setBetSum(forUpdate.getBetSum().add(coupon.getAmount()));
                            flag = gambleDAO.updateTotalizerBetSum(forUpdate);
                            if (!flag) {
                                forUpdate = gambleDAO.findAvailableTotalizer();
                                flag = forUpdate != null && forUpdate.getId().equals(totalizer.getId());
                            }
                        }
                    }
                }
                if (flag) {
                    gambleDAO.commit();
                } else {
                    gambleDAO.rollback();
                    throw new ServiceException("Can not create new coupon");
                }
            } else {
                result = new HashMap<>();
                result.put(VALIDATION_ERRORS, errors);
                result.put(TOTALIZER, totalizer);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not create new coupon", e);
        }
        return result;
    }

    /**
     * Checks whether available totalizer exist and whether this user already make bet
     *
     * @param user object who tries to make bet
     * @return {@code true} if possible to make bet {@code false} otherwise
     */
    public boolean checkMakeBetPossibility(User user) throws ServiceException {
        Totalizer totalizer;
        boolean result;
        try (GambleDAO gambleDAO = factory.getGambleDAO()) {
            totalizer = gambleDAO.findAvailableTotalizer();
            result = totalizer != null && user != null && gambleDAO.findCoupon(user, totalizer) == null;
        } catch (DAOException e) {
            throw new ServiceException("Can not find Available totalizer", e);
        }
        return result;
    }

    /**
     * Delete active coupon from database. Return user bets back.
     *
     * @param user user whose coupon will be deleted
     * @return {@code true} if operation is successful {@code false} otherwise.
     */

    public boolean cancelCoupon(User user) throws ServiceException {
        boolean result;
        result = user != null;
        if (result) {
            try (GambleDAO gambleDAO = factory.getGambleDAO()) {
                gambleDAO.startTransaction();
                UserDAO userDAO = TransactionManager.getInstance().getUserDaoWithinSameTransaction(gambleDAO, factory);
                Totalizer totalizer = gambleDAO.findAvailableTotalizer();
                result = totalizer != null;
                if (result) {
                    totalizer.setEvents(gambleDAO.findAvailableEvents(totalizer));
                    result = TotalizerStateInspector.checkTotalizerTime(totalizer).isEmpty();
                }
                Coupon coupon = null;
                if (result) {
                    coupon = gambleDAO.findCoupon(user, totalizer);
                    result = coupon != null;
                }
                if (result) {
                    User userForUpdate = userDAO.findUserById(user.getId());
                    Optional<BigDecimal> userMoney = Optional.ofNullable(userForUpdate.getMoney());
                    Optional<BigDecimal> betAmount = Optional.ofNullable(coupon.getAmount());
                    Optional<BigDecimal> betCredit = Optional.ofNullable(coupon.getCredit());
                    BigDecimal zero = BigDecimal.valueOf(0);
                    BigDecimal creditMultiplier = BigDecimal.valueOf(CREDIT_MULTIPLIER_NORMAL);
                    BigDecimal minusCredit = betCredit.orElse(zero).multiply(creditMultiplier);
                    BigDecimal refund = betAmount.orElse(zero).subtract(minusCredit);
                    BigDecimal newUserBalance = userMoney.orElse(zero).add(refund);
                    userForUpdate.setMoney(newUserBalance);
                    result = userDAO.update(userForUpdate) && gambleDAO.deleteCoupon(coupon);
                    if (result) {
                        Remittance remittance = new Remittance(refund, Remittance.Type.REFUND);
                        result = userDAO.createRemittance(remittance, userForUpdate.getId());
                    }
                    BigDecimal totalizerSum = Optional.ofNullable(totalizer.getBetSum()).orElse(zero).
                            subtract(betAmount.orElse(zero));
                    totalizer.setBetSum(totalizerSum);
                    while (!(result = gambleDAO.updateTotalizerBetSum(totalizer))) {
                        totalizer = gambleDAO.findAvailableTotalizer();
                        totalizer.setBetSum(totalizerSum);
                        result = gambleDAO.updateTotalizerBetSum(totalizer);
                    }
                }
                if (result) {
                    gambleDAO.commit();
                } else {
                    gambleDAO.rollback();
                }
            } catch (DAOException e) {
                throw new ServiceException("Can't cancel coupon", e);
            }
        }
        return result;
    }

    /**
     * Method to check whether any of totalizer events already started
     *
     * @return {@code true} if started and {@code false} otherwise.
     */
    public Map<String, String> checkAnyEventStarted() throws ServiceException {
        Totalizer totalizer;
        boolean result;
        Map<String, String> errors = Collections.emptyMap();
        try (GambleDAO gambleDAO = factory.getGambleDAO()) {
            totalizer = gambleDAO.findAvailableTotalizer();
            result = totalizer != null;
            if (result) {
                totalizer.setEvents(gambleDAO.findAvailableEvents(totalizer));
                errors = TotalizerStateInspector.checkTotalizerTime(totalizer);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find Available totalizer", e);
        }
        return errors;
    }

    /**
     * Cancel active coupon credit
     *
     * @param user user whose credit will be annul
     * @return {@code true} if operation is successful {@code false} otherwise.
     */
    public boolean RepayCredit(User user) throws ServiceException {
        boolean result;
        result = user != null;
        if (result) {
            result = checkAnyEventStarted().isEmpty();
        }
        if (result) {
            try (GambleDAO gambleDAO = factory.getGambleDAO()) {
                gambleDAO.startTransaction();
                UserDAO userDAO = TransactionManager.getInstance().getUserDaoWithinSameTransaction(gambleDAO, factory);
                Totalizer totalizer = gambleDAO.findAvailableTotalizer();
                result = totalizer != null;
                if (result) {
                    Coupon coupon = gambleDAO.findCoupon(user, totalizer);
                    result = coupon != null;
                    if (result) {
                        BigDecimal sumForRepay = coupon.getCredit().multiply(BigDecimal.valueOf(CREDIT_MULTIPLIER_NORMAL));
                        coupon.setCredit(BigDecimal.ZERO);
                        result = gambleDAO.updateCoupons(Collections.singletonList(coupon));
                        if (result) {
                            User updated = userDAO.findUserById(user.getId());
                            result = updated.getMoney().compareTo(sumForRepay) >= 0;
                            if (result) {
                                BigDecimal newUserMoney = updated.getMoney().subtract(sumForRepay);
                                updated.setMoney(newUserMoney);
                                result = userDAO.update(updated);
                            }
                        }
                        if (result) {
                            Remittance remittance = new Remittance(sumForRepay, Remittance.Type.DEBT);
                            result = userDAO.createRemittance(remittance, user.getId());
                        }
                    }
                }
                if (result) {
                    gambleDAO.commit();
                } else {
                    gambleDAO.rollback();
                }
            } catch (DAOException e) {
                throw new ServiceException("Can't repay credit", e);
            }
        }
        return result;
    }

    /**
     * Finds actual coupon for this user
     *
     * @param user object which coupon is belong to.
     * @return {@code Coupon} object
     */
    public Coupon findActualCoupon(User user) throws ServiceException {
        Coupon coupon = null;
        try (GambleDAO gambleDAO = factory.getGambleDAO()) {
            Totalizer totalizer = gambleDAO.findAvailableTotalizer();
            if (totalizer != null) {
                coupon = gambleDAO.findCoupon(user, totalizer);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't find actual coupon", e);
        }
        return coupon;
    }
}
