package com.rtmznk.totalizer.service;

import com.rtmznk.totalizer.dao.*;
import com.rtmznk.totalizer.entity.*;
import com.rtmznk.totalizer.exception.DAOException;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.utility.*;

import java.math.BigDecimal;
import java.util.*;

import static com.rtmznk.totalizer.constant.RequestAttributeName.TOTALIZER;
import static com.rtmznk.totalizer.constant.RequestAttributeName.VALIDATION_ERRORS;
import static com.rtmznk.totalizer.constant.WinnersGroupParameter.*;

/**
 * Represents service to execute admin command
 *
 * @author Artem Zinkevich
 */
public class AdminService {
    private DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);

    AdminService() {

    }

    /**
     * Find all users
     *
     * @return {@code List<User>} list of all users
     */
    public List<User> findAllUsers() throws ServiceException {
        List<User> result;
        try (UserDAO userDAO = factory.getUserDAO()) {
            result = userDAO.findUsers();
        } catch (DAOException e) {
            throw new ServiceException("Can not find users", e);
        }
        return result;
    }

    /**
     * Switching user ban status
     *
     * @param id user id
     * @return {@code true} if status changed in database {@code otherwise}
     */
    public boolean changeUserBanStatus(String id) throws ServiceException {
        boolean result;
        if (RequestDataValidator.getInstance().validateId(id)) {
            long userId = Long.parseLong(id);
            try (UserDAO userDAO = factory.getUserDAO()) {
                User user = userDAO.findUserById(userId);
                if (user != null) {
                    user.setBanned(!user.getBanned());
                    result = userDAO.update(user);
                    while (!result) {
                        user = userDAO.findUserById(userId);
                        user.setBanned(!user.getBanned());
                        result = userDAO.update(user);
                    }
                } else {
                    result = false;
                }
            } catch (DAOException e) {
                throw new ServiceException("Can not find user", e);
            }
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Create new {@link Totalizer} object from json string and pass it to DAO
     *
     * @param totolizerJson json String data
     * @return {@code Map} of errors occur during execution
     */
    public Map<String, Object> createTotalizer(String totolizerJson) throws ServiceException {
        Map<String, String> errors = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        Totalizer lastCreated;
        Totalizer totalizer;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            boolean flag = true;
            adminDAO.startTransaction();
            lastCreated = adminDAO.findLastTotalizer();
            totalizer = JsonUtility.getInstance().parseTotalizerJson(totolizerJson);
            if (lastCreated != null) {
                errors = TotalizerStateInspector.checkTotalizerState(lastCreated.getState(), Totalizer.State.FINISHED);
                flag = lastCreated.isLast() && errors.isEmpty();
            }
            if (flag) {
                errors = RequestDataValidator.getInstance().validateTotolizerParameters(totalizer);
                flag = errors.isEmpty();
            }
            if (flag) {
                flag = adminDAO.createTotalizer(totalizer);
            }
            if (flag && lastCreated != null) {
                lastCreated.setLast(false);
                flag = adminDAO.updateTotalizer(lastCreated);
            }
            if (flag) {
                flag = adminDAO.createEvents(totalizer);
            }
            if (flag) {
                adminDAO.commit();
            } else {
                adminDAO.rollback();
                if (errors.isEmpty()) {
                    throw new ServiceException("Can not create totalizer. Necessary conditions wasn't satisfied");
                } else {
                    result.put(VALIDATION_ERRORS, errors);
                    if (totalizer != null) {
                        result.put(TOTALIZER, totalizer);
                    }
                }
            }

        } catch (DAOException e) {
            throw new ServiceException("Can not create totalizer", e);
        }
        return result;
    }

    /**
     * Find last created totalizer and corresponding events
     *
     * @return totalizer object with events
     */
    public Totalizer findLastTotalizerWithEvents() throws ServiceException {
        Totalizer lastTotalizer;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            lastTotalizer = adminDAO.findLastTotalizer();
            if (lastTotalizer != null) {
                List<Event> events = adminDAO.findEvents(lastTotalizer);
                lastTotalizer.setEvents(events);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't get totalizer or events", e);
        }
        return lastTotalizer;
    }

    /**
     * Create new {@link Totalizer} object from json string and updates corresponding totalizer in database.
     *
     * @param totolizerJson json String data
     * @return {@code Map} of errors occur during execution
     */
    public Map<String, Object> updateTotalizer(String totolizerJson) throws ServiceException {
        Map<String, String> errors = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        Totalizer lastTotolizer;
        Totalizer totalizer;
        List<Event> events;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            boolean flag = true;
            adminDAO.startTransaction();
            totalizer = JsonUtility.getInstance().parseTotalizerJson(totolizerJson);
            lastTotolizer = adminDAO.findLastTotalizer();
            if (lastTotolizer != null) {
                errors = TotalizerStateInspector.checkTotalizerState(lastTotolizer.getState(), Totalizer.State.EDITABLE);
                flag = lastTotolizer.isLast() && errors.isEmpty();
            }
            if (flag) {
                errors = RequestDataValidator.getInstance().validateTotolizerParameters(totalizer);
                flag = errors.isEmpty();
            }
            if (flag) {
                if (lastTotolizer != null && totalizer != null) {
                    lastTotolizer.setJackpot(totalizer.getJackpot());
                    lastTotolizer.setName(totalizer.getName());
                    flag = adminDAO.updateTotalizer(lastTotolizer);
                } else {
                    flag = false;
                }
            }
            if (flag) {
                events = adminDAO.findEvents(lastTotolizer);
                if (!events.isEmpty()) {
                    updateFirstWithSecond(events, totalizer.getEvents());
                    lastTotolizer.setEvents(events);
                }
                flag = !events.isEmpty();
            }
            if (flag) {
                flag = adminDAO.updateEvents(lastTotolizer);
            }
            if (flag) {
                adminDAO.commit();
            } else {
                adminDAO.rollback();
                if (errors.isEmpty()) {
                    throw new ServiceException("Can not updateTotalizerBetSum totalizer. Necessary conditions wasn't satisfied");
                } else {
                    result.put(VALIDATION_ERRORS, errors);
                    if (totalizer != null) {
                        result.put(TOTALIZER, totalizer);
                    }
                }
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not updateTotalizerBetSum totalizer", e);
        }
        return result;
    }

    /**
     * Update events from first list with corresponding event values from second list
     *
     * @param first  list of events to be updated
     * @param second list of events holding information for update
     */
    private void updateFirstWithSecond(List<Event> first, List<Event> second) {
        Event totalizerEvent;
        for (int i = 0; i < first.size(); i++) {
            totalizerEvent = second.get(i);
            Event current = first.get(i);
            current.setDate(totalizerEvent.getDate());
            current.setSport(totalizerEvent.getSport());
            current.setTotal(totalizerEvent.getTotal());
            current.setTeam1(totalizerEvent.getTeam1());
            current.setTeam2(totalizerEvent.getTeam2());
            current.setType(totalizerEvent.getType());
        }
    }

    /**
     * Make last created totalizer in database accessible for users to make bets
     *
     * @return {@code true} if operation is successful {@code false} otherwise.
     */
    public boolean startTotalizer() throws ServiceException {
        Totalizer lastTotalizer;
        boolean result;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            adminDAO.startTransaction();
            lastTotalizer = adminDAO.findLastTotalizer();
            lastTotalizer.setEvents(adminDAO.findEvents(lastTotalizer));
            if (RequestDataValidator.getInstance().validateTotolizerParameters(lastTotalizer).isEmpty()) {
                lastTotalizer.setState(Totalizer.State.PLAYABLE);
                if (adminDAO.updateTotalizer(lastTotalizer)) {
                    adminDAO.commit();
                    result = true;
                } else {
                    adminDAO.rollback();
                    throw new ServiceException("Can't start totalizer");
                }
            } else {
                result = false;
                adminDAO.rollback();
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't start totalizer", e);
        }
        return result;
    }

    /**
     * Delete last totalizer created from database. Return user bets back.
     *
     * @return {@code true} if operation is successful {@code false} otherwise.
     */
    public boolean cancelTotalizer() throws ServiceException {
        Map<String, String> errors;
        boolean result = true;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            adminDAO.startTransaction();
            Totalizer lastTotalizer = adminDAO.findLastTotalizer();
            if (lastTotalizer != null) {
                errors = TotalizerStateInspector.checkTotalizerState(lastTotalizer.getState(),
                        Totalizer.State.EDITABLE, Totalizer.State.PLAYABLE);
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(null,
                        Totalizer.State.EDITABLE, Totalizer.State.PLAYABLE);
            }
            if (errors.isEmpty()) {
                Map<User, Coupon> resultMap = adminDAO.findCouponsWithUsers(lastTotalizer);
                if (!resultMap.isEmpty()) {
                    GambleDAO gambleDAO = TransactionManager.getInstance().getGambleDaoWithinSameTransaction(adminDAO, factory);
                    UserDAO userDAO = TransactionManager.getInstance().getUserDaoWithinSameTransaction(adminDAO, factory);
                    for (Map.Entry<User, Coupon> entry : resultMap.entrySet()) {
                        if (result) {
                            User user = entry.getKey();
                            Coupon coupon = entry.getValue();
                            Optional<BigDecimal> userMoney = Optional.ofNullable(user.getMoney());
                            Optional<BigDecimal> betAmount = Optional.ofNullable(coupon.getAmount());
                            Optional<BigDecimal> betCredit = Optional.ofNullable(coupon.getCredit());
                            BigDecimal zero = BigDecimal.valueOf(0);
                            BigDecimal betWithoutCredit = betAmount.orElse(zero).subtract(betCredit.orElse(zero));
                            BigDecimal newUserBalance = userMoney.orElse(zero).add(betWithoutCredit);
                            user = userDAO.findUserById(user.getId());
                            user.setMoney(newUserBalance);
                            result = userDAO.update(user);
                            if (result) {
                                Remittance remittance = new Remittance(betWithoutCredit, Remittance.Type.REFUND);
                                result = userDAO.createRemittance(remittance, user.getId());
                            }
                            if (result) {
                                result = gambleDAO.deleteCoupon(coupon);
                            }
                        }
                    }
                    if (result) {
                        result = adminDAO.deleteTotalizer(lastTotalizer);
                    }
                } else {
                    result = adminDAO.deleteTotalizer(lastTotalizer);
                }
                if (result) {
                    Totalizer totalizer = adminDAO.findLastTotalizer();
                    if (totalizer != null) {
                        totalizer.setLast(true);
                        result = adminDAO.updateTotalizer(totalizer);
                    }
                }
                if (result) {
                    adminDAO.commit();
                } else {
                    adminDAO.rollback();
                }
            } else {
                result = false;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't cancel totalizer", e);
        }
        return result;
    }

    /**
     * Checks whether the creation of totalizer possible
     *
     * @return {@code Map} of errors or {@code EmptyMap} if creation is possible
     */
    public Map<String, String> checkCreationPossibility() throws ServiceException {
        Map<String, String> errors;
        Totalizer lastCreated;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            lastCreated = adminDAO.findLastTotalizer();
            if (lastCreated == null) {
                errors = Collections.emptyMap();
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(lastCreated.getState(), Totalizer.State.FINISHED);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't check creation possibility for totalizer", e);
        }
        return errors;
    }

    /**
     * Checks whether the edition of totalizer possible
     *
     * @return {@code Map} of errors or {@code EmptyMap} if edition is possible
     */
    public Map<String, String> checkEditionPossibility() throws ServiceException {
        Map<String, String> errors;
        Totalizer lastCreated;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            lastCreated = adminDAO.findLastTotalizer();
            if (lastCreated == null) {
                errors = TotalizerStateInspector.checkTotalizerState(null, Totalizer.State.EDITABLE);
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(lastCreated.getState(), Totalizer.State.EDITABLE);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't check edition possibility for totalizer", e);
        }
        return errors;
    }

    /**
     * Checks whether the cancellation of totalizer possible
     *
     * @return {@code Map} of errors or {@code EmptyMap} if cancellation is possible
     */
    public Map<String, String> checkCancellationPossibility() throws ServiceException {
        Map<String, String> errors;
        Totalizer lastCreated;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            lastCreated = adminDAO.findLastTotalizer();
            if (lastCreated == null) {
                errors = TotalizerStateInspector.checkTotalizerState(null,
                        Totalizer.State.EDITABLE, Totalizer.State.PLAYABLE);
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(lastCreated.getState(),
                        Totalizer.State.EDITABLE, Totalizer.State.PLAYABLE);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't check cancellation possibility for totalizer", e);
        }
        return errors;
    }

    /**
     * Checks whether the finishing of totalizer possible
     *
     * @return {@code Map} of errors or {@code EmptyMap} if finishing is possible
     */
    public Map<String, String> checkSummarizePossibility() throws ServiceException {
        Map<String, String> errors;
        Totalizer lastCreated;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            lastCreated = adminDAO.findLastTotalizer();
            if (lastCreated == null) {
                errors = TotalizerStateInspector.checkTotalizerState(null,
                        Totalizer.State.PLAYABLE);
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(lastCreated.getState(),
                        Totalizer.State.PLAYABLE);
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't check finish possibility for totalizer", e);
        }
        return errors;
    }

    /**
     * Finish last created totalizer in database
     *
     * @return {@code true} if successful {@code false} otherwise
     */
    public boolean summarizeTotalizer() throws ServiceException {
        Map<String, String> errors;
        boolean result;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            adminDAO.startTransaction();
            GambleDAO gambleDAO = TransactionManager.getInstance().getGambleDaoWithinSameTransaction(adminDAO, factory);
            UserDAO userDAO = TransactionManager.getInstance().getUserDaoWithinSameTransaction(adminDAO, factory);
            Totalizer lastTotalizer = adminDAO.findLastTotalizer();
            if (lastTotalizer == null) {
                errors = TotalizerStateInspector.checkTotalizerState(null,
                        Totalizer.State.PLAYABLE);
            } else {
                errors = TotalizerStateInspector.checkTotalizerState(lastTotalizer.getState(),
                        Totalizer.State.PLAYABLE);
            }
            result = errors.isEmpty();
            if (result) {
                List<Event> events = adminDAO.findEvents(lastTotalizer);
                result = events != null && !events.isEmpty();
                if (result) {
                    ScoreRandomizer.defineEventsScore(events);
                    lastTotalizer.setEvents(events);
                    result = adminDAO.updateEvents(lastTotalizer);
                }
                if (result) {
                    adminDAO.updateBetsStatus(lastTotalizer);
                }
                if (result) {
                    Map<Long, BigDecimal> prizes = new HashMap<>();
                    List<Coupon> wonCoupons = new ArrayList<>();
                    wonCoupons.addAll(adminDAO.findWonCouponsForCount(FIRST_GUESSED_COUNT));
                    addOrSetValuesToMap(calculateSumForGroup(wonCoupons, lastTotalizer, FIRST_PERCENTAGE, lastTotalizer.getJackpot()),
                            prizes);
                    wonCoupons.addAll(adminDAO.findWonCouponsForCount(SECOND_GUESSED_COUNT));
                    addOrSetValuesToMap(calculateSumForGroup(wonCoupons, lastTotalizer, SECOND_PERCENTAGE, null), prizes);
                    wonCoupons.addAll(adminDAO.findWonCouponsForCount(THIRD_GUESSED_COUNT));
                    addOrSetValuesToMap(calculateSumForGroup(wonCoupons, lastTotalizer, THIRD_PERCENTAGE, null), prizes);
                    wonCoupons.addAll(adminDAO.findWonCouponsForCount(FOURTH_GUESSED_COUNT));
                    addOrSetValuesToMap(calculateSumForGroup(wonCoupons, lastTotalizer, FOURTH_PERCENTAGE, null), prizes);
                    gambleDAO.updateCoupons(wonCoupons);
                    adminDAO.updateCouponStatus(lastTotalizer);
                    Iterator<Map.Entry<Long, BigDecimal>> prizesIterator = prizes.entrySet().iterator();
                    while (result && prizesIterator.hasNext()) {
                        Map.Entry<Long, BigDecimal> current = prizesIterator.next();
                        Long key = current.getKey();
                        BigDecimal value = current.getValue();
                        BigDecimal userBalance = adminDAO.findUserMoneyForUpdate(key);
                        result = adminDAO.updateUserBalance(key, value.add(userBalance));
                        if (result) {
                            Remittance remittance = new Remittance(value, Remittance.Type.PRIZE);
                            result = userDAO.createRemittance(remittance, key);
                        }
                    }
                }
                if (result) {
                    lastTotalizer.setState(Totalizer.State.FINISHED);
                    result = adminDAO.updateTotalizer(lastTotalizer);
                }
            }
            if (result) {
                adminDAO.commit();
            } else {
                adminDAO.rollback();
            }
        } catch (DAOException e) {
            throw new ServiceException("Can't summarize totalizer", e);
        }
        return result;
    }

    /**
     * Helper method to distribute prize fund money among winners.
     *
     * @param coupons    coupons of winners
     * @param totalizer  actual totalizer
     * @param percentage percentage for defining amount of money to be distributed among winners
     * @param jackpot    sum of jackpot
     * @return {Map} where key is user id, value corresponding prize
     */
    private Map<Long, BigDecimal> calculateSumForGroup(List<Coupon> coupons, Totalizer totalizer, double percentage, BigDecimal jackpot) {
        Map<Long, BigDecimal> result = Collections.emptyMap();
        BigDecimal share;
        if (jackpot != null) {
            share = totalizer.getBetSum().multiply(BigDecimal.valueOf(FIRST_PERCENTAGE)).
                    add(jackpot);
        } else {
            share = totalizer.getBetSum().multiply(BigDecimal.valueOf(percentage));
        }
        if (!coupons.isEmpty()) {
            result = Bookmaker.getInstance().definePrizesForUsers(coupons, share);
        }
        return result;
    }

    /**
     * Add new key value pair from one map to another if key not exist,update value otherwise
     *
     * @param from map object with new values
     * @param to   updated map
     */
    private void addOrSetValuesToMap(Map<Long, BigDecimal> from, Map<Long, BigDecimal> to) {
        for (Map.Entry<Long, BigDecimal> entry : from.entrySet()) {
            Long key = entry.getKey();
            BigDecimal value = entry.getValue();
            if (to.containsKey(key)) {
                to.put(key, to.get(key).add(value));
            } else {
                to.put(key, value);
            }
        }
    }

}
