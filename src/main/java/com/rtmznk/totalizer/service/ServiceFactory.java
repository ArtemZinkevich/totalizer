package com.rtmznk.totalizer.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a factory of all available services
 *
 * @author Artem Zinkevich
 */
public class ServiceFactory {
    private static Logger logger = LoggerFactory.getLogger(ServiceFactory.class);

    private static ServiceFactory instance = new ServiceFactory();

    private UserService userService;
    private AdminService adminService;
    private GambleService gambleService;

    private ServiceFactory() {
        userService = new UserService();
        adminService = new AdminService();
        gambleService = new GambleService();
        logger.info("ServiceFactory initialized");
    }

    /**
     * @return single instance of {@link ServiceFactory}
     */
    public static ServiceFactory getInstance() {
        return instance;
    }

    public UserService getUserService() {
        return userService;
    }

    public GambleService getGambleService() {
        return gambleService;
    }

    public AdminService getAdminService() {
        return adminService;
    }

}
