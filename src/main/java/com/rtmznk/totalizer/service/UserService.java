package com.rtmznk.totalizer.service;


import com.rtmznk.totalizer.dao.AdminDAO;
import com.rtmznk.totalizer.dao.DAOFactory;
import com.rtmznk.totalizer.dao.UserDAO;

import com.rtmznk.totalizer.entity.Event;
import com.rtmznk.totalizer.entity.Remittance;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.exception.DAOException;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.utility.PasswordUtility;
import com.rtmznk.totalizer.utility.RequestDataValidator;
import com.rtmznk.totalizer.utility.UserBuilder;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.rtmznk.totalizer.constant.RequestParameterName.MONEY_SUM;

/**
 * Represents service to execute user command
 *
 * @author Artem Zinkevich
 */
public class UserService {
    private DAOFactory factory = DAOFactory.getDAOFactory(DAOFactory.MYSQL);

    UserService() {
    }

    /**
     * Update user info with data from database and return updated user back
     *
     * @param user user object to be updated
     * @return updated {@code User}
     */
    public User updateUserInfo(User user) throws ServiceException {
        User result;
        try (UserDAO userDAO = factory.getUserDAO()) {
            result = userDAO.findUserById(user.getId());
            if (result == null) {
                throw new ServiceException("Can not find user");
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not find user", e);
        }
        return result;
    }

    /**
     * Create new user entry in database
     *
     * @param requestParams {@code Map} object with user parameters
     * @return {@code Map} with errors or {@link Collections.EmptyMap} if no errors
     */
    public Map<String, String> registerUser(Map<String, String> requestParams) throws ServiceException {
        User user;
        Map<String, String> resultMap = Collections.emptyMap();
        Map<String, String> requestErrors = RequestDataValidator.getInstance().receiveRegistrationErrors(requestParams);
        if (requestErrors.isEmpty()) {
            user = UserBuilder.getInstance().buildUser(requestParams);
            user.setPassword(PasswordUtility.hashPassword(user.getPassword()));
            try (UserDAO userDAO = factory.getUserDAO()) {
                userDAO.create(user);
            } catch (DAOException e) {
                throw new ServiceException("Can not register new user", e);
            }
        } else {
            resultMap = requestErrors;
        }
        return resultMap;
    }

    /**
     * Check whether user with such mail exist in database
     *
     * @param mail mail parameter
     * @return {@code true}if exist and {@code false} otherwise.
     */
    public boolean checkMailExistence(String mail) throws ServiceException {
        boolean isExist = false;
        try (UserDAO userDAO = factory.getUserDAO()) {
            if (userDAO.findUserByMail(mail) != null) {
                isExist = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not check e-mail existence", e);
        }
        return isExist;
    }

    /**
     * Check whether mail and password is correct.If correct return
     * {@link User} object corresponding mail.
     *
     * @param mail     mail parameter
     * @param password password parameter to be compared
     * @return {@code User} object
     */
    public User loginAction(String mail, String password) throws ServiceException {
        User user;
        try (UserDAO userDAO = factory.getUserDAO()) {
            user = userDAO.findUserByMail(mail);
            if (user != null && !(user.getPassword().equals(PasswordUtility.hashPassword(password)))) {
                user = null;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not log in", e);
        }
        return user;
    }

    /**
     * Check deposit parameters. If it's valid update user balance, else return Map with errors
     *
     * @param user       user object to be updated
     * @param parameters deposit parameters
     * @return {@code Map} with errors or {@link Collections.EmptyMap} if no errors
     */
    public Map<String, String> makeDeposit(User user, Map<String, String> parameters) throws ServiceException {
        Map<String, String> errors = RequestDataValidator.getInstance().receiveDepositErrors(parameters, user.getMoney());
        if (errors.isEmpty()) {
            User copy;
            try (UserDAO userDAO = factory.getUserDAO()) {
                userDAO.startTransaction();
                copy = user.clone();
                BigDecimal sum = new BigDecimal(parameters.get(MONEY_SUM));
                copy.setMoney(copy.getMoney().add(sum));
                boolean res = userDAO.update(copy);
                if (res) {
                    Remittance remittance = new Remittance(copy.getMoney(), Remittance.Type.DEPOSIT);
                    res = userDAO.createRemittance(remittance, copy.getId());
                }
                if (res) {
                    userDAO.commit();
                } else {
                    userDAO.rollback();
                    throw new ServiceException("Deposit failed. Wrong parameters");
                }
            } catch (DAOException | CloneNotSupportedException e) {
                throw new ServiceException("Deposit failed", e);
            }
        }
        return errors;
    }

    /**
     * Updates user language
     *
     * @param user user to be updated
     * @return {@code true}if succeeded and {@code false} otherwise.
     */
    public boolean updateUserLanguageInDB(User user) throws ServiceException {
        boolean actionResult = false;
        try (UserDAO userDAO = factory.getUserDAO()) {
            if (userDAO.update(user)) {
                actionResult = true;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can not updateTotalizerBetSum user language", e);
        }
        return actionResult;
    }

    /**
     * Updates user password in database if {@param oldPassword} is correct.
     *
     * @param user        user to be updated
     * @param oldPassword old password
     * @param newPassword new password
     * @return @return {@code true}if succeeded and {@code false} otherwise.
     */
    public boolean changeUserPassword(User user, String oldPassword, String newPassword) throws ServiceException {
        boolean result = false;
        try (UserDAO userDAO = factory.getUserDAO()) {
            if (user.getPassword().equals(PasswordUtility.hashPassword(oldPassword))) {
                String newHashedPassword = PasswordUtility.hashPassword(newPassword);
                User userCopy = user.clone();
                userCopy.setPassword(newHashedPassword);
                result = userDAO.update(userCopy);
            }
        } catch (DAOException | CloneNotSupportedException e) {
            throw new ServiceException("Can change user password", e);
        }
        return result;
    }

    /**
     * Find all user remittances
     *
     * @param user user object
     * @return {@code List} of remittances if succeeded.
     */
    public List<Remittance> findAllRemittances(User user) throws ServiceException {
        List<Remittance> remittances;
        try (UserDAO userDAO = factory.getUserDAO()) {
            remittances = userDAO.findUserRemittances(user.getId());
        } catch (DAOException e) {
            throw new ServiceException("Can find user remittances", e);
        }
        return remittances;
    }

    /**
     * Find all finished events.
     *
     * @return list of finished events
     */
    public List<Event> findAllFinishedEvents() throws ServiceException {
        List<Event> events;
        try (AdminDAO adminDAO = factory.getAdminDAO()) {
            events = adminDAO.findFinishedEvents();
            if (events.isEmpty()) {
                events = null;
            }
        } catch (DAOException e) {
            throw new ServiceException("Can find finished events", e);
        }
        return events;
    }
}
