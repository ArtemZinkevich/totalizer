package com.rtmznk.totalizer.utility;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Class for receiving Calendar with UTC timezone.
 * Used in DAO manipulation with TimeStamp objects to prevent data invalidating.
 *
 * @author Artem Zinkevich
 */
public class CalendarUTC {
    private static CalendarUTC instance = new CalendarUTC();
    private final Calendar calendarUTC;

    private CalendarUTC() {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        calendarUTC = Calendar.getInstance(timeZone);
    }

    /**
     * @return instance of Calendar with UTC timeZone
     */
    public Calendar getCalendarUTC() {
        return calendarUTC;
    }

    /**
     * @return single instance of {@link CalendarUTC}
     */
    public static CalendarUTC getInstance() {
        return instance;
    }
}
