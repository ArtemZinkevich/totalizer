package com.rtmznk.totalizer.utility;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static com.rtmznk.totalizer.constant.RequestParameterName.*;

/**
 * Class used for receiving huge amount of data from {@link HttpServletRequest} object
 */
public class RequestDataReceiver {
    private final static RequestDataReceiver instance = new RequestDataReceiver();

    /**
     * @return single instance of {@link RequestDataReceiver}
     */
    public static RequestDataReceiver getInstance() {
        return instance;
    }

    private RequestDataReceiver() {
    }

    public Map<String, String> receiveRegistrationData(HttpServletRequest request) {
        Map<String, String> result = new HashMap<>();
        result.put(NAME, request.getParameter(NAME));
        result.put(LAST_NAME, request.getParameter(LAST_NAME));
        result.put(MAIL, request.getParameter(MAIL));
        result.put(PASSWORD, request.getParameter(PASSWORD));
        result.put(CONFIRM_PASSWORD, request.getParameter(CONFIRM_PASSWORD));
        return result;
    }

    public Map<String, String> receiveDepositData(HttpServletRequest request) {
        Map<String, String> result = new HashMap<>();
        result.put(MONEY_SUM, request.getParameter(MONEY_SUM));
        result.put(CARD_NUMBER, request.getParameter(CARD_NUMBER));
        result.put(MONTH, request.getParameter(MONTH));
        result.put(YEAR, request.getParameter(YEAR));
        result.put(CVV, request.getParameter(CVV));
        result.put(PERSON, request.getParameter(PERSON));
        return result;
    }
}
