package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.Totalizer;
import jdk.net.SocketFlow;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * Class used for inspecting whether totalizer is in expected state
 *
 * @author Artem Zinkevich
 */
public class TotalizerStateInspector {
    private static final String STATE_ERROR = "stateError";
    private static final String CREATION_ERROR = "creationError";
    private static final String EDITION_ERROR = "editionError";
    private static final String FINISHING_ERROR = "finishingError";
    private static final String DELETION_ERROR = "deletionError";
    private static final String TIME_IS_OVER = "timeIsOver";

    private TotalizerStateInspector() {
    }

    /**
     * Checks whether {@param checkedState} is contained in {@param expectedStates} array.
     *
     * @param checkedState   actual totalizer state
     * @param expectedStates states expected
     * @return map of errors if checkedState not contained in expectedStates array.
     */
    public static Map<String, String> checkTotalizerState(Totalizer.State checkedState, Totalizer.State... expectedStates) {
        Map<String, String> errors = new HashMap<>();
        if (expectedStates.length == 1) {
            switch (expectedStates[0]) {
                case FINISHED: {
                    if (checkedState != Totalizer.State.FINISHED) {
                        errors.put(STATE_ERROR, CREATION_ERROR);
                    }
                    break;
                }
                case EDITABLE: {
                    if (checkedState != Totalizer.State.EDITABLE) {
                        errors.put(STATE_ERROR, EDITION_ERROR);
                    }
                    break;
                }
                case PLAYABLE: {
                    if (checkedState != Totalizer.State.PLAYABLE) {
                        errors.put(STATE_ERROR, FINISHING_ERROR);
                    }
                }
            }
        } else {
            boolean flag = false;
            for (Totalizer.State state : expectedStates) {
                if (checkedState == state) {
                    flag = true;
                }
            }
            if (!flag) {
                errors.put(STATE_ERROR, DELETION_ERROR);
            }
        }
        return errors;
    }

    /**
     * Checks whether one of totalizer events already started.
     */
    public static Map<String, String> checkTotalizerTime(Totalizer totalizer) {
        Map<String, String> errors = new HashMap<>();
        if (totalizer != null && totalizer.getEvents() != null) {
            if (totalizer.getEvents().stream().anyMatch(event -> event.getDate().
                    compareTo(LocalDateTime.now(Clock.systemUTC())) <= 0)) {
                errors.put(TIME_IS_OVER, TIME_IS_OVER);
            }
        }
        return errors;
    }
}
