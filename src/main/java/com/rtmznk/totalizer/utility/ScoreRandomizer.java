package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.Event;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Class used for randomizing events score
 *
 * @author Artem Zinkevich
 */
public class ScoreRandomizer {
    private static int MAX_POSSIBLE_SCORE = 5;

    private ScoreRandomizer() {
    }

    /**
     * Set random scores for {@param events}
     */
    public static void defineEventsScore(List<Event> events) {
        for (Event event : events) {
            int score1 = ThreadLocalRandom.current().nextInt(MAX_POSSIBLE_SCORE);
            int score2 = ThreadLocalRandom.current().nextInt(MAX_POSSIBLE_SCORE);
            event.setScore1(score1);
            event.setScore2(score2);
            event.setFinished(true);
        }
    }
}
