package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.*;
import com.rtmznk.totalizer.exception.ServiceException;
import com.rtmznk.totalizer.service.ServiceFactory;
import com.rtmznk.totalizer.service.UserService;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


import static com.rtmznk.totalizer.constant.RequestParameterName.*;
import static com.rtmznk.totalizer.entity.Event.Type.TOTAL;

/**
 * Class used for validation data received from request
 *
 * @author Artem Zinkevich
 */
public class RequestDataValidator {
    private final static RequestDataValidator instance = new RequestDataValidator();
    private static String PASSWORD_PATTERN = "^((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})$";
    private static String SUM_PATTERN = "^\\d{1,4}(\\.\\d{1,2})?$";
    private static String CARD_PATTERN = "^\\d{16}$";
    private static String MONTH_PATTERN = "^([1-9]|1[0-2])$";
    private static String YEAR_PATTERN = "^\\d{2}$";
    private static String CVV_PATTERN = "^\\d{3}$";
    private static String BET_SUM_PATTERN = "^\\d{1,6}(\\.\\d{1,2})?$";
    private static String TOTAL_PATTERN = "^\\d{1,3}\\.5$";
    private static BigDecimal MAX_ACCOUNT_MONEY = new BigDecimal("10000000000");
    private static BigDecimal MAX_JACKPOT_MONEY = new BigDecimal("1000000000");
    private static int MIN_HOURS_BEFORE_EVENT = 24;
    private static int MAX_HOURS_BEFORE_EVENT = 168;
    private static final String EMPTY_FIELD = "emptyField";
    private static final String INCORRECT_MAIL = "incorrectMail";
    private static final String MAIL_IN_USE = "mailInUse";
    private static final String PASSWORD_INCORRECT = "incorrectPass";
    private static final String PASSWORD_CONFIRM_INCORRECT = "inconfirmed";
    private static final String ILLEGAL_SUM = "illegalSum";
    private static final String ILLEGAL_CARD = "illegalCard";
    private static final String MONEY_OVERFLOW = "overflow";
    private static final String ILLEGAL_TOTALIZER_NAME = "nameError";
    private static final String ILLEGAL_JACKPOT = "jackpotError";
    private static final String WRONG_EVENT_TYPE = "typeError";
    private static final String WRONG_SPORT = "sportError";
    private static final String WRONG_TEAM = "teamError";
    private static final String WRONG_EVENT_DATE = "dateError";
    private static final String NO_USER = "noUserError";
    private static final String LOW_BALANCE = "lowBalanceError";
    private static final String WRONG_TOTAL = "totalScoreError";
    private static final String BETS_ERROR = "betsError";
    private static final String TOTALIZER_NOT_AVAILABLE = "totalizerNotAvailable";

    /**
     * @return single instance of {@link RequestDataValidator}
     */
    public static RequestDataValidator getInstance() {
        return instance;
    }

    private RequestDataValidator() {
    }

    /**
     * Validate registration data
     */
    public Map<String, String> receiveRegistrationErrors(Map<String, String> registrationData) throws ServiceException {
        UserService service = ServiceFactory.getInstance().getUserService();
        Map<String, String> errors = new HashMap<>();
        String name = registrationData.get(NAME);
        String lastName = registrationData.get(LAST_NAME);
        String mail = registrationData.get(MAIL);
        String password = registrationData.get(PASSWORD);
        String password2 = registrationData.get(CONFIRM_PASSWORD);
        if (name == null || name.isEmpty()) {
            errors.put(NAME, EMPTY_FIELD);
        }
        if (lastName == null || lastName.isEmpty()) {
            errors.put(LAST_NAME, EMPTY_FIELD);
        }
        if (mail == null || mail.isEmpty()) {
            errors.put(MAIL, EMPTY_FIELD);
        } else if (!(mail.contains("@") && mail.contains("."))) {
            errors.put(MAIL, INCORRECT_MAIL);
        } else if (service.checkMailExistence(mail)) {
            errors.put(MAIL, MAIL_IN_USE);
        }
        if (password == null || password.isEmpty()) {
            errors.put(PASSWORD, EMPTY_FIELD);
        } else if (!Pattern.matches(PASSWORD_PATTERN, password)) {
            errors.put(PASSWORD, PASSWORD_INCORRECT);
        }
        if (password != null && password2 != null && (!password.equals(password2))) {
            errors.put(CONFIRM_PASSWORD, PASSWORD_CONFIRM_INCORRECT);
        }
        return errors;
    }

    /**
     * Validate deposit data
     */
    public Map<String, String> receiveDepositErrors(Map<String, String> depositData, BigDecimal userMoney) throws ServiceException {
        Map<String, String> errors = new HashMap<>();
        boolean wrongCard = false;
        String sum = depositData.get(MONEY_SUM);
        String cardNum = depositData.get(CARD_NUMBER);
        String month = depositData.get(MONTH);
        String year = depositData.get(YEAR);
        String cvv = depositData.get(CVV);
        String person = depositData.get(PERSON);
        if (sum == null || !sum.matches(SUM_PATTERN)) {
            errors.put(ILLEGAL_SUM, ILLEGAL_SUM);
        } else {
            BigDecimal depositSum = new BigDecimal(sum);
            BigDecimal total = userMoney.add(depositSum);
            if (total.compareTo(MAX_ACCOUNT_MONEY) == 1) {
                errors.put(MONEY_OVERFLOW, MONEY_OVERFLOW);
            }
        }
        if (cardNum == null || !cardNum.matches(CARD_PATTERN)) {
            wrongCard = true;
        }
        if (month == null || !month.matches(MONTH_PATTERN)) {
            wrongCard = true;
        }
        if (year == null || !year.matches(YEAR_PATTERN)) {
            wrongCard = true;
        }
        if (cvv == null || !cvv.matches(CVV_PATTERN)) {
            wrongCard = true;
        }
        if (person == null || person.isEmpty()) {
            wrongCard = true;
        }
        if (wrongCard) {
            errors.put(ILLEGAL_CARD, ILLEGAL_CARD);
        }
        return errors;
    }

    /**
     * Validate id string
     */
    public boolean validateId(String id) {
        boolean res = true;
        try {
            long d = Long.parseLong(id);
        } catch (NumberFormatException nfe) {
            res = false;
        }
        return res;
    }

    /**
     * Validate totalizer data
     */
    public Map<String, String> validateTotolizerParameters(Totalizer newTotalizer) {
        Map<String, String> errorMap = new HashMap<>();
        String name = newTotalizer.getName();
        BigDecimal newJackpot = newTotalizer.getJackpot();
        LocalDateTime now = LocalDateTime.now(Clock.systemUTC());
        if (name == null || name.isEmpty()) {
            errorMap.put(ILLEGAL_TOTALIZER_NAME, ILLEGAL_TOTALIZER_NAME);
        }
        if (newJackpot == null || newJackpot.compareTo(MAX_JACKPOT_MONEY) > 0) {
            errorMap.put(ILLEGAL_JACKPOT, ILLEGAL_JACKPOT);
        }
        for (Event event : newTotalizer.getEvents()) {
            Event.Type evType = event.getType();
            String sport = event.getSport();
            String team1 = event.getTeam1();
            String team2 = event.getTeam2();
            LocalDateTime date = event.getDate();
            Double total = event.getTotal();
            if (evType == null) {
                errorMap.put(WRONG_EVENT_TYPE, WRONG_EVENT_TYPE);
            } else {
                if (evType == TOTAL && (total == null || !total.toString().matches(TOTAL_PATTERN))) {
                    errorMap.put(WRONG_TOTAL, WRONG_TOTAL);
                }
            }
            if (sport == null || sport.isEmpty()) {
                errorMap.put(WRONG_SPORT, WRONG_SPORT);
            }
            if (team1 == null || team2 == null || team1.isEmpty() || team2.isEmpty()) {
                errorMap.put(WRONG_TEAM, WRONG_TEAM);
            }
            if ((date.compareTo(now.plusHours(MIN_HOURS_BEFORE_EVENT)) < 0) ||
                    (date.compareTo(now.plusHours(MAX_HOURS_BEFORE_EVENT)) > 0)) {
                errorMap.put(WRONG_EVENT_DATE, WRONG_EVENT_DATE);
            }
        }
        return errorMap;
    }

    /**
     * Validate coupon data
     */
    public Map<String, String> validateCoupon(Coupon coupon, Totalizer totalizer, User updatedUser) {
        Map<String, String> errors = new HashMap<>();
        if (updatedUser == null) {
            errors.put(NO_USER, NO_USER);
        } else {
            if (updatedUser.getMoney().compareTo(coupon.getAmount()) < 0) {
                errors.put(LOW_BALANCE, LOW_BALANCE);
            }
        }
        if (totalizer != null) {
            Map<Long, Event> eventsMap = new HashMap<>();
            totalizer.getEvents().forEach(e -> eventsMap.put(e.getId(), e));
            coupon.getBets().forEach(bet -> {
                Event fitEvent = eventsMap.get(bet.getEventId());
                if (fitEvent == null) {
                    errors.put(BETS_ERROR, BETS_ERROR);
                } else if (fitEvent.getType() != bet.getEventType()) {
                    errors.put(BETS_ERROR, BETS_ERROR);
                } else {
                    if (!isBetConsistent(bet)) {
                        errors.put(BETS_ERROR, BETS_ERROR);
                    }
                }
            });
            BigDecimal betSum = coupon.getAmount();
            BigDecimal credit = coupon.getCredit();
            if (betSum == null || !betSum.toString().matches(BET_SUM_PATTERN) ||
                    (credit != null && betSum.divide(credit, BigDecimal.ROUND_UP).doubleValue() < 2)) {
                errors.put(ILLEGAL_SUM, ILLEGAL_SUM);
            }
        } else {
            errors.put(TOTALIZER_NOT_AVAILABLE, TOTALIZER_NOT_AVAILABLE);
        }
        errors.putAll(TotalizerStateInspector.checkTotalizerTime(totalizer));
        return errors;
    }

    /**
     * Validate if bet data consistent with corresponding event
     */
    private boolean isBetConsistent(Bet bet) {
        boolean result = true;
        Bet.Result betResult = bet.getResult();
        Boolean moreThanTotal = bet.getMoreThanTotal();
        Integer score1 = bet.getScore1();
        Integer score2 = bet.getScore2();
        Event.Type betEventType = bet.getEventType();
        switch (betEventType) {
            case RESULT: {
                if (!isNumberOfFirstNotNullOtherNull(1, betResult, moreThanTotal, score1, score2)) {
                    result = false;
                }
                break;
            }
            case SCORE: {
                if (!isNumberOfFirstNotNullOtherNull(2, score1, score2, betResult, moreThanTotal)) {
                    result = false;
                }
                break;
            }
            case TOTAL: {
                if (!isNumberOfFirstNotNullOtherNull(1, moreThanTotal, score1, score2, betResult)) {
                    result = false;
                }
                break;
            }
        }
        return result;
    }

    /**
     * Helper method. Returns {@code true} if first N={@param firstAmount} of {@param objects} are not null
     * or {@code} false otherwise
     */
    private boolean isNumberOfFirstNotNullOtherNull(int firstAmount, Object... objects) {
        boolean result = true;
        for (int i = 0; i < objects.length && i < firstAmount; i++) {
            result = result && objects[i] != null;
        }
        for (int i = firstAmount; i < objects.length; i++) {
            result = result && objects[i] == null;
        }
        return result;
    }
}
