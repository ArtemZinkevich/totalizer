package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.Coupon;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.rtmznk.totalizer.constant.CreditMultiplierValue.CREDIT_MULTIPLIER_EXPIRED;

/**
 * Represents class for distributing prizes among winners
 *
 * @author Artem Zinkevich
 */
public class Bookmaker {
    private static Bookmaker instance = new Bookmaker();

    /**
     * @return single instance of {@link Bookmaker}
     */
    public static Bookmaker getInstance() {
        return instance;
    }

    /**
     * Distribute prizes among coupons taking part in distribution
     *
     * @param coupons coupons taking part in distribution
     * @param share   amount of money to be divided
     * @return map with users id as keys and amount of money as value
     */
    public Map<Long, BigDecimal> definePrizesForUsers(List<Coupon> coupons, BigDecimal share) {
        Map<Long, BigDecimal> prizes = new HashMap<>();
        BigDecimal zero = BigDecimal.valueOf(0);
        BigDecimal maxBet = zero;
        BigDecimal commonDenominator = zero;
        for (Coupon coupon : coupons) {
            BigDecimal amount = Optional.ofNullable(coupon.getAmount()).orElse(zero);
            BigDecimal credit = Optional.ofNullable(coupon.getCredit()).orElse(zero);
            BigDecimal couponMoney = amount.subtract(credit.multiply(BigDecimal.valueOf(CREDIT_MULTIPLIER_EXPIRED)));
            coupon.setAmount(couponMoney);
            coupon.setCredit(zero);
            coupon.setEvaluated(true);
            coupon.setWon(true);
            if (couponMoney.compareTo(maxBet) > 0) {
                maxBet = couponMoney;
            }
            commonDenominator = commonDenominator.add(couponMoney);
        }
        for (Coupon coupon : coupons) {
            BigDecimal rate = coupon.getAmount().divide(commonDenominator, (int) Math.log10(maxBet.doubleValue()) + 1,
                    BigDecimal.ROUND_DOWN);
            BigDecimal prize = rate.multiply(share).setScale(2, BigDecimal.ROUND_HALF_UP);
            prizes.put(coupon.getUserId(), prize);
        }
        return prizes;
    }
}
