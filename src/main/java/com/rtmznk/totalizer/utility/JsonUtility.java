package com.rtmznk.totalizer.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rtmznk.totalizer.entity.Coupon;
import com.rtmznk.totalizer.entity.Totalizer;
import com.rtmznk.totalizer.exception.ServiceException;

import java.io.IOException;

/**
 * Class for parsing JSON string into java objects.
 *
 * @author Artem Zinkevich
 */
public class JsonUtility {
    private final static JsonUtility instance = new JsonUtility();

    private JsonUtility() {
    }

    /**
     * @return single instance of {@link JsonUtility}
     */
    public static JsonUtility getInstance() {
        return instance;
    }

    /**
     * Parse JSON string into {@link Totalizer} object
     *
     * @param json json string for parsing
     * @return totalizer object
     * @throws ServiceException if it's impossible to parse {@param json} into {@link Totalizer} object
     */
    public Totalizer parseTotalizerJson(String json) throws ServiceException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        Totalizer totalizer;
        try {
            totalizer = objectMapper.readValue(json, Totalizer.class);
        } catch (IOException e) {
            throw new ServiceException("Can not parse incoming totolizer JSON string", e);
        }
        return totalizer;
    }

    /**
     * Parse JSON string into {@link Coupon} object
     *
     * @param json json string for parsing
     * @return coupon object
     * @throws ServiceException if it's impossible to parse {@param json} into {@link Coupon} object
     */
    public Coupon parseCouponJson(String json) throws ServiceException {
        ObjectMapper objectMapper = new ObjectMapper();
        Coupon coupon;
        try {
            coupon = objectMapper.readValue(json, Coupon.class);
        } catch (IOException e) {
            throw new ServiceException("Can not parse incoming totolizer JSON string", e);
        }
        return coupon;
    }
}
