package com.rtmznk.totalizer.utility;

import com.rtmznk.totalizer.entity.User;

import java.util.Map;

import static com.rtmznk.totalizer.constant.RequestParameterName.*;

/**
 * Class used for building user from String parameters
 *
 * @author Artem Zinkevich
 */
public class UserBuilder {
    private static UserBuilder instance = new UserBuilder();

    /**
     * @return single instance of {@link UserBuilder}
     */
    public static UserBuilder getInstance() {
        return instance;
    }

    private UserBuilder() {
    }

    public User buildUser(Map<String, String> requestParams) {
        User user = new User();
        user.setName(requestParams.get(NAME));
        user.setLastName(requestParams.get(LAST_NAME));
        user.setEmail(requestParams.get(MAIL));
        user.setPassword(requestParams.get(PASSWORD));
        return user;
    }
}
