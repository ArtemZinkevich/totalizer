package com.rtmznk.totalizer.utility;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * Class for parsing receiving possible {@link Locale} objects for application.
 *
 * @author Artem Zinkevich
 */
public class LocaleUtility {
    private static Logger logger = LoggerFactory.getLogger(LocaleUtility.class);
    private static final LocaleUtility instance = new LocaleUtility();

    /**
     * Supported locales
     */
    private enum SupportedLocale {
        EN(Locale.ENGLISH), RU(new Locale("ru"));
        private Locale locale;

        SupportedLocale(Locale locale) {
            this.locale = locale;
        }

        public Locale getLocale() {
            return locale;
        }
    }


    private LocaleUtility() {
    }

    /**
     * @return single instance of {@link LocaleUtility}
     */
    public static LocaleUtility getInstance() {
        return instance;
    }

    /**
     * Return locale object according to locale string/ If not support - return a default locale
     *
     * @param localeStr incoming string with language
     * @return Locale.class
     */
    public Locale getLocaleByString(String localeStr) {
        SupportedLocale supportedLocale;
        try {
            supportedLocale = SupportedLocale.valueOf(localeStr.toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.warn("Illegal locale arg : " + localeStr + ". Default locale will be used");
            supportedLocale = SupportedLocale.RU;
        }
        return supportedLocale.getLocale();
    }

    /**
     * return a default locale
     *
     * @return Locale class object
     */
    public Locale getDefaultLocale() {
        SupportedLocale localeEnum = SupportedLocale.RU;
        return localeEnum.getLocale();
    }
}
