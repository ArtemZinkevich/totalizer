package com.rtmznk.totalizer.servlet;

import com.rtmznk.totalizer.command.CommandManager;
import com.rtmznk.totalizer.command.CommandResult;
import com.rtmznk.totalizer.constant.ErrorPageMessage;
import com.rtmznk.totalizer.constant.RequestAttributeName;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class represents a controller for web application. Overrides doGet and
 * doPost methods for HTTP request.
 *
 * @author Artem Zinkevich
 */
@WebServlet(name = "controller", urlPatterns = {"/command/*"})
public class Controller extends HttpServlet {
    private CommandManager manager = CommandManager.getInstance();

    /**
     * Called by the server to allow a servlet(Controller) to handle a GET
     * request.
     *
     * @see javax.servlet.http.HttpServlet
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommandResult result = manager.executeCommand(request, response);
        handleCommandResult(request, response, result);
    }

    /**
     * Called by the server to allow a servlet(Controller) to handle a POST
     * request.
     *
     * @see javax.servlet.http.HttpServlet
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommandResult result = manager.executeCommand(request, response);
        handleCommandResult(request, response, result);
    }

    /**
     * Helper method for handling command execution result
     *
     * @param request       an {@link HttpServletRequest} object that
     *                      contains the request the client has made
     *                      of the servlet
     * @param response      an {@link HttpServletResponse} object that
     *                      contains the response the servlet sends
     *                      to the client
     * @param commandResult result of command manager request
     * @throws ServletException if the request
     *                          could not be handled
     * @throws IOException      if an input or output error is
     *                          detected when the servlet handles
     *                          the request
     */
    private void handleCommandResult(HttpServletRequest request, HttpServletResponse response,
                                     CommandResult commandResult) throws ServletException, IOException {
        switch (commandResult.getResultAction()) {
            case FORWARD: {
                request.getRequestDispatcher(commandResult.getResultRoute()).forward(request, response);
                break;
            }
            case REDIRECT: {
                response.sendRedirect(commandResult.getResultRoute());
                break;
            }
            case AJAX: {
                response.getWriter().write(commandResult.getResultMessage());
                break;
            }
            case ERROR: {
                ErrorPageMessage message = (ErrorPageMessage) request.getAttribute(RequestAttributeName.ERROR_PAGE_MESSAGE);
                response.sendError(message.getErrorCode());
                break;
            }
        }
    }
}