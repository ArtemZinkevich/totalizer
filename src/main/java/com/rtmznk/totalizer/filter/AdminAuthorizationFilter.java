package com.rtmznk.totalizer.filter;

import com.rtmznk.totalizer.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * Represents a filter to prevent access of users with role="user"
 * to resources which can be accessible only by authorized users with role="admin"
 * <p>
 * In case of unauthorized sendError(403) to the ERROR page.
 * </p>
 *
 * @author Artem Zinkevich
 */
@WebFilter(
        filterName = "AdminAuthorizationFilter",
        urlPatterns = {"/profile/admin", "/profile/admin/*"},
        dispatcherTypes = {DispatcherType.REQUEST},
        initParams = {
                @WebInitParam(
                        name = "adminRole",
                        value = "admin",
                        description = "Admin role")
        }
)
public class AdminAuthorizationFilter implements Filter {
    private String adminRole;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        User user = (User) req.getSession().getAttribute(USER);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (user != null && user.getRole().equals(adminRole)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        adminRole = filterConfig.getInitParameter("adminRole");
    }

    @Override
    public void destroy() {
    }
}