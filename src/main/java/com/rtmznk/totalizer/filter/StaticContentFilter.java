package com.rtmznk.totalizer.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Represents a filter to manage requests flow.
 * It routes requests of static content(as .html,.css,.js,images) to default container servlet.
 * It routes requests of non-static content to {@link com.rtmznk.totalizer.servlet.Controller} servlet.
 *
 * @author Artem Zinkevich
 */
@WebFilter(
        filterName = "StaticContentFilter",
        urlPatterns = "/*",
        dispatcherTypes = {DispatcherType.REQUEST}
)
public class StaticContentFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(StaticContentFilter.class);
    private static String PATH_TO_STATIC = "/static";
    private static String PATH_TO_WELCOME_FILE = "/";
    private static String BAD_FAVICON_REQUEST = "favicon.ico";
    private static String PATH_TO_CONTROLLER = "/command";

    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("StaticContentFilter is initialized");
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String path = req.getRequestURI().substring(req.getContextPath().length());
        if (path.startsWith(PATH_TO_STATIC) || path.equals(PATH_TO_WELCOME_FILE)
                || path.contains(BAD_FAVICON_REQUEST)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            path = PATH_TO_CONTROLLER + path;
            servletRequest.getRequestDispatcher(path).forward(servletRequest, servletResponse);
        }
    }

    public void destroy() {
        logger.info("StaticContentFilter is destroyed");
    }
}
