package com.rtmznk.totalizer.filter;

import com.rtmznk.totalizer.constant.SessionAttributeName;
import com.rtmznk.totalizer.entity.User;
import com.rtmznk.totalizer.utility.LocaleUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

/**
 * Represents a filter to check and set {@link Locale} to session before it is
 * passed to the servlet.
 *
 * @author Artem Zinkevich
 */
@WebFilter(
        filterName = "LocaleFilter",
        urlPatterns = "/*"

)
public class LocaleFilter implements Filter {
    private static Logger logger = LoggerFactory.getLogger(LocaleFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("LocaleFilter is initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpSession session = req.getSession();
        LocaleUtility localeUtility = LocaleUtility.getInstance();
        User user = (User) session.getAttribute(SessionAttributeName.USER);
        Locale locale;
        if (user != null) {
            locale = user.getUserLocale();
            if (locale == null) {
                locale = localeUtility.getDefaultLocale();
            }
        } else {
            Locale sessionLocale = (Locale) session.getAttribute(SessionAttributeName.LANGUAGE);
            locale = (sessionLocale == null) ?
                    localeUtility.getDefaultLocale() : sessionLocale;
        }
        session.setAttribute(SessionAttributeName.LANGUAGE, locale);
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        logger.info("LocaleFilter is destroyed");
    }
}
