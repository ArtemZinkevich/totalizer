package com.rtmznk.totalizer.filter;


import com.rtmznk.totalizer.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * Represents a filter to prevent access of unauthenticated users
 * to resources which can be accessible only by authenticated users
 * <p>
 * In case of unauthenticated sendError(401) to the ERROR page.
 * </p>
 *
 * @author Artem Zinkevich
 */
@WebFilter(
        filterName = "AuthenticationFilter",
        urlPatterns = {"/profile", "/profile/*", "/go-profile"},
        dispatcherTypes = {DispatcherType.REQUEST}

)
public class AuthenticationFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        User user = (User) req.getSession().getAttribute(USER);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (user == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }
}