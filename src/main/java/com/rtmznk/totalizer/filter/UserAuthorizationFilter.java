package com.rtmznk.totalizer.filter;

import com.rtmznk.totalizer.entity.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.rtmznk.totalizer.constant.SessionAttributeName.USER;

/**
 * Represents a filter to prevent access of users with role="admin"
 * to resources which can be accessible only by authorized users with role="user"
 * <p>
 * In case of unauthorized sendError(403) to the ERROR page.
 * </p>
 *
 * @author Artem Zinkevich
 */
@WebFilter(
        filterName = "UserAuthorizationFilter",
        urlPatterns = {"/profile/user", "/profile/user/*"},
        dispatcherTypes = {DispatcherType.REQUEST},
        initParams = {
                @WebInitParam(
                        name = "userRole",
                        value = "user",
                        description = "User role")
        }

)
public class UserAuthorizationFilter implements Filter {
    private String userRole;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        User user = (User) req.getSession().getAttribute(USER);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (user != null && user.getRole().equals(userRole)) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        userRole = filterConfig.getInitParameter("userRole");
    }

    @Override
    public void destroy() {
    }
}